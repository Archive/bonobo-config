/**
 * bonobo-config-xmldb.c: xml configuration database implementation.
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#include <config.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <bonobo/bonobo-arg.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo-config/bonobo-config-utils.h>
#include <libxml/xmlmemory.h>

#include "bonobo-config-xmldb.h"

static GObjectClass *parent_class = NULL;

#define CLASS(o) BONOBO_CONFIG_XMLDB_CLASS (G_OBJECT_GET_CLASS (o))

#define PARENT_TYPE BONOBO_TYPE_CONFIG_DATABASE
#define FLUSH_INTERVAL 30 /* 30 seconds */

static DirEntry *
dir_lookup_entry (DirData  *dir,
		  char     *name,
		  gboolean  create)
{
	GSList *l;
	DirEntry *de;
	
	l = dir->entries;

	while (l) {
		de = (DirEntry *)l->data;

		if (!strcmp (de->name, name))
			return de;
		
		l = l->next;
	}

	if (create) {

		de = g_new0 (DirEntry, 1);
		
		de->dir = dir;

		de->name = g_strdup (name);

		dir->entries = g_slist_prepend (dir->entries, de);

		return de;
	}

	return NULL;
}

static DirData *
dir_lookup_subdir (DirData  *dir,
		   char     *name,
		   gboolean  create)
{
	GSList *l;
	DirData *dd;
	
	l = dir->subdirs;

	while (l) {
		dd = (DirData *)l->data;

		if (!strcmp (dd->name, name))
			return dd;
		
		l = l->next;
	}

	if (create) {

		dd = g_new0 (DirData, 1);

		dd->dir = dir;

		dd->name = g_strdup (name);

		dir->subdirs = g_slist_prepend (dir->subdirs, dd);

		return dd;
	}

	return NULL;
}

static DirData *
lookup_dir (DirData    *dir,
	    const char *path,
	    gboolean    create)
{
	const char *s, *e;
	char *name;
	DirData  *dd = dir;
	
	s = path;
	while (*s == '/') s++;
	
	if (*s == '\0')
		return dir;

	if ((e = strchr (s, '/')))
		name = g_strndup (s, e - s);
	else
		name = g_strdup (s);
	
	if ((dd = dir_lookup_subdir (dd, name, create))) {
		if (e)
			dd = lookup_dir (dd, e, create);

		g_free (name);
		return dd;
		
	}

	return NULL;

}

static DirEntry *
lookup_dir_entry (BonoboConfigXMLDB *xmldb,
		  const char        *key, 
		  gboolean           create)
{
	char     *dir_name;
	char     *leaf_name;
	DirEntry *de;
	DirData  *dd;
	xmlNode  *root;

	root = xmlDocGetRootElement (xmldb->doc);
	
	if ((dir_name = bonobo_config_dir_name (key))) {
		dd = lookup_dir (xmldb->dir, dir_name, create);
		
		if (dd && !dd->node) {

			dd->node = xmlNewChild (root, NULL, "section", NULL);
		
			xmlSetProp (dd->node, "path", dir_name);
		}	

		g_free (dir_name);

	} else {
		dd = xmldb->dir;

		if (!dd->node) 
			dd->node = xmlNewChild (root, NULL, "section", NULL);
	}

	if (!dd)
		return NULL;

	if (!(leaf_name = bonobo_config_leaf_name (key)))
		return NULL;

	de = dir_lookup_entry (dd, leaf_name, create);

	g_free (leaf_name);

	return de;
}

static CORBA_any *
real_get_value (BonoboConfigDatabase *db,
		const CORBA_char     *key, 
		CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	DirEntry          *de;
	CORBA_any         *value = NULL;
	char              *locale = NULL; 
				
	/* fixme: how to handle locale correctly ? */

	de = lookup_dir_entry (xmldb, key, FALSE);
	if (!de) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	if (de->value)
		return bonobo_arg_copy (de->value);
	
	/* localized value */
	if (de->node && de->node->children && de->node->children->next)	
		value = bonobo_config_xml_decode_any (de->node, locale, ev);

	if (!value)
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
	
	return value;
}

static void
real_sync (BonoboConfigDatabase *db, 
	   CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	char *tmp_name;

	if (!db->writeable)
		return;

	xmlIndentTreeOutput = 1;

	tmp_name = g_strdup_printf ("%s.tmp.%d\n", xmldb->filename, getpid ());

	if (xmlSaveFile (tmp_name, xmldb->doc) < 0) {
		g_free (tmp_name);
		db->writeable = FALSE;
		return;
	}

	// chmod (tmp_name, 0664);

	if (rename (tmp_name, xmldb->filename) < 0) {
		g_free (tmp_name);
		db->writeable = FALSE;
		return;
	}

	g_free (tmp_name);
	return;
}

static gboolean
timeout_cb (gpointer data)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (data);
	CORBA_Environment ev;

	CORBA_exception_init(&ev);

	real_sync (BONOBO_CONFIG_DATABASE (data), &ev);
	
	CORBA_exception_free (&ev);

	xmldb->time_id = 0;

	/* remove the timeout */
	return FALSE;
}


static void
real_set_value (BonoboConfigDatabase *db,
		const CORBA_char     *key, 
		const CORBA_any      *value,
		CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	DirEntry *de;
	char *name;

	de = lookup_dir_entry (xmldb, key, TRUE);

	if (de->value)
		CORBA_free (de->value);

	de->value = bonobo_arg_copy (value);

	if (de->node) {
		xmlUnlinkNode (de->node);
		xmlFreeNode (de->node);
	}
		
	name =  bonobo_config_leaf_name (key);

	de->node = (xmlNodePtr) bonobo_config_xml_encode_any (value, name, ev);
	
	g_free (name);
       
	xmlAddChild (de->dir->node, de->node);

	if (!xmldb->time_id)
		xmldb->time_id = g_timeout_add (FLUSH_INTERVAL * 1000, 
						timeout_cb, xmldb);
}

static Bonobo_KeyList *
real_get_dirs (BonoboConfigDatabase *db,
	       const CORBA_char     *dir,
	       CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	Bonobo_KeyList *key_list;
	DirData *dd, *sub;
	GSList *l;
	int len;
	
	key_list = Bonobo_KeyList__alloc ();
	key_list->_length = 0;

	if (!(dd = lookup_dir (xmldb->dir, dir, FALSE)))
		return key_list;

	if (!(len = g_slist_length (dd->subdirs)))
		return key_list;

	key_list->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	CORBA_sequence_set_release (key_list, TRUE); 
	
	for (l = dd->subdirs; l != NULL; l = l->next) {
		sub = (DirData *)l->data;
	       
		key_list->_buffer [key_list->_length] = 
			CORBA_string_dup (sub->name);
		key_list->_length++;
	}
	
	return key_list;
}

static Bonobo_KeyList *
real_get_keys (BonoboConfigDatabase *db,
	       const CORBA_char     *dir,
	       CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	Bonobo_KeyList *key_list;
	DirData *dd;
	DirEntry *de;
	GSList *l;
	int i, len;
	
	key_list = Bonobo_KeyList__alloc ();
	key_list->_length = 0;

	if (!(dd = lookup_dir (xmldb->dir, dir, FALSE)))
		return key_list;

	if (!(len = g_slist_length (dd->entries)))
		return key_list;

	key_list->_length = len;
	key_list->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	CORBA_sequence_set_release (key_list, TRUE); 
	
	i = 0;
	for (l = dd->entries; l != NULL; l = l->next) {
		de = (DirEntry *)l->data;
	       
		key_list->_buffer [i] = CORBA_string_dup (de->name);
		i++;
	}
	
	return key_list;
}

static CORBA_boolean
real_has_dir (BonoboConfigDatabase *db,
	      const CORBA_char     *dir,
	      CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);

	if (lookup_dir (xmldb->dir, dir, FALSE))
		return TRUE;

	return FALSE;
}

static void
delete_dir_entry (DirEntry *de)
{
	if (de->value)
		CORBA_free (de->value);

	if (de->node) {
		xmlUnlinkNode (de->node);
		xmlFreeNode (de->node);
	}
	
	if (de->name)
		g_free (de->name);
	
	g_free (de);
}

static void
real_remove_value (BonoboConfigDatabase *db,
		   const CORBA_char     *key, 
		   CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	DirEntry *de;

	if (!(de = lookup_dir_entry (xmldb, key, FALSE)))
		return;

	de->dir->entries = g_slist_remove (de->dir->entries, de);

	delete_dir_entry (de);
}

static void
delete_dir_data (DirData *dd, gboolean is_root)
{
	GSList *l;

	l = dd->subdirs;
	
	while (l) {
		
		delete_dir_data ((DirData *)l->data, FALSE);

		l = l->next;
	}

	g_slist_free (dd->subdirs);

	dd->subdirs = NULL;

	l = dd->entries;

	while (l) {

		delete_dir_entry ((DirEntry *)l->data);

		l = l->next;
	}

	g_slist_free (dd->entries);

	dd->entries = NULL;

	if (!is_root) {
	
		if (dd->name)
			g_free (dd->name);

		if (dd->node) {
			xmlUnlinkNode (dd->node);
			xmlFreeNode (dd->node);
		}
	
		g_free (dd);
	}
}

static void
real_remove_dir (BonoboConfigDatabase *db,
		 const CORBA_char     *dir, 
		 CORBA_Environment    *ev)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (db);
	DirData *dd;

	if (!(dd = lookup_dir (xmldb->dir, dir, FALSE)))
		return;

	if (dd != xmldb->dir && dd->dir)
		dd->dir->subdirs = g_slist_remove (dd->dir->subdirs, dd);

	delete_dir_data (dd, dd == xmldb->dir);
}

static void
bonobo_config_xmldb_finalize (GObject *object)
{
	BonoboConfigXMLDB *xmldb = BONOBO_CONFIG_XMLDB (object);
	CORBA_Environment  ev;

	CORBA_exception_init (&ev);

	bonobo_url_unregister ("BONOBO_CONF:XLMDB", xmldb->filename, &ev);
      
	CORBA_exception_free (&ev);

	xmlFreeDoc (xmldb->doc);
	xmldb->doc = NULL;

	g_free (xmldb->filename);
	xmldb->filename = NULL;

	parent_class->finalize (object);
}


static void
bonobo_config_xmldb_class_init (BonoboConfigDatabaseClass *class)
{
	GObjectClass *object_class = (GObjectClass *) class;
	BonoboConfigDatabaseClass *cd_class;

	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = bonobo_config_xmldb_finalize;

	cd_class = BONOBO_CONFIG_DATABASE_CLASS (class);
		
	cd_class->get_value    = real_get_value;
	cd_class->set_value    = real_set_value;
	cd_class->get_dirs     = real_get_dirs;
	cd_class->get_keys     = real_get_keys;
	cd_class->has_dir      = real_has_dir;
	cd_class->remove_value = real_remove_value;
	cd_class->remove_dir   = real_remove_dir;
	cd_class->sync         = real_sync;
}

static void
bonobo_config_xmldb_init (BonoboConfigXMLDB *xmldb)
{
	xmldb->dir = g_new0 (DirData, 1);	
}

BONOBO_TYPE_FUNC (BonoboConfigXMLDB, PARENT_TYPE, bonobo_config_xmldb);

static void
read_section (DirData *dd)
{
	CORBA_Environment  ev;
	DirEntry          *de;
	xmlNodePtr         s;
	gchar             *name;

	CORBA_exception_init (&ev);

	s = dd->node->children;

	while (s) {
		if (s->type == XML_ELEMENT_NODE && 
		    !strcmp (s->name, "entry") &&
		    (name = xmlGetProp(s, "name"))) {
			
			de = dir_lookup_entry (dd, name, TRUE);
			
			de->node = s;
			
			/* we only decode it if it is a single value, 
			 * multiple (localized) values are decoded in the
			 * get_value function */
			if (!(s->children && s->children->next))
				de->value = bonobo_config_xml_decode_any 
					(s, NULL, &ev);
			
			xmlFree (name);
		}
		s = s->next;
	}

	CORBA_exception_free (&ev);
}

static void
fill_cache (BonoboConfigXMLDB *xmldb)
{
	xmlNode    *n;
	gchar      *path;
	DirData    *dd;
	xmlNode    *root;

	root = xmlDocGetRootElement (xmldb->doc);

	n = root->children;

	while (n) {
		if (n->type == XML_ELEMENT_NODE && 
		    !strcmp (n->name, "section")) {

			path = xmlGetProp(n, "path");

			if (!path || !strcmp (path, "")) {
				dd = xmldb->dir;
			} else 
				dd = lookup_dir (xmldb->dir, path, TRUE);
				
			if (!dd->node)
				dd->node = n;

			read_section (dd);
			
			xmlFree (path);
		}
		n = n->next;
	}
}

Bonobo_ConfigDatabase
bonobo_config_xmldb_new (const char *filename)
{
	BonoboConfigXMLDB     *xmldb;
	Bonobo_ConfigDatabase  db;
	CORBA_Environment      ev;
	char                  *real_name;
	xmlNode               *root;

	g_return_val_if_fail (filename != NULL, NULL);

	CORBA_exception_init (&ev);

	if (filename [0] == '~' && filename [1] == '/')
		real_name = g_strconcat (g_get_home_dir (), &filename [1], 
					 NULL);
	else
		real_name = g_strdup (filename);

	db = bonobo_url_lookup ("BONOBO_CONF:XLMDB", real_name, &ev);

	if (BONOBO_EX (&ev)) {
		CORBA_exception_init (&ev);
		db = CORBA_OBJECT_NIL;
	}

	CORBA_exception_free (&ev);

	if (db) {
		g_free (real_name);
		return bonobo_object_dup_ref (db, NULL);
	}

	if (!(xmldb = g_object_new (BONOBO_TYPE_CONFIG_XMLDB, NULL))) {
		g_free (real_name);
		return CORBA_OBJECT_NIL;
	}

	bonobo_config_database_construct (BONOBO_CONFIG_DATABASE (xmldb), 
					  NULL);
	xmldb->filename = real_name;

	BONOBO_CONFIG_DATABASE (xmldb)->writeable = TRUE;
		       
	xmldb->doc = xmlParseFile (xmldb->filename);

	if (!xmldb->doc)
		xmldb->doc = xmlNewDoc("1.0");

	if (!(root = xmlDocGetRootElement (xmldb->doc))) {
		root = xmlNewDocNode (xmldb->doc, NULL, "bonobo-config", NULL);
		xmlDocSetRootElement (xmldb->doc, root);
	}	

	if (strcmp (root->name, "bonobo-config")) {
		xmlFreeDoc (xmldb->doc);
		xmldb->doc = xmlNewDoc("1.0");
		root = xmlNewDocNode (xmldb->doc, NULL, "bonobo-config", NULL);
		xmlDocSetRootElement (xmldb->doc, root);
	}

	fill_cache (xmldb);

	db = CORBA_Object_duplicate (BONOBO_OBJREF (xmldb), NULL);

	bonobo_url_register ("BONOBO_CONF:XLMDB", real_name, NULL, db, &ev);

	return db;
}
