/**
 * bonobo-config-xmldb.h: xml configuration database implementation.
 *
 * Author:
 *   Dietmar Maurer  (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifndef __BONOBO_CONFIG_XMLDB_H__
#define __BONOBO_CONFIG_XMLDB_H__

#include <stdio.h>
#include <string.h>
#include <bonobo-config/bonobo-config-database.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

G_BEGIN_DECLS

#define BONOBO_TYPE_CONFIG_XMLDB        (bonobo_config_xmldb_get_type ())
#define BONOBO_CONFIG_XMLDB_TYPE        BONOBO_TYPE_CONFIG_XMLDB // deprecated, you should use BONOBO_TYPE_CONFIG_XMLDB
#define BONOBO_CONFIG_XMLDB(o)	        (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_CONFIG_XMLDB, BonoboConfigXMLDB))
#define BONOBO_CONFIG_XMLDB_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_CONFIG_XMLDB, BonoboConfigXMLDBClass))
#define BONOBO_IS_CONFIG_XMLDB(o)       (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_CONFIG_XMLDB))
#define BONOBO_IS_CONFIG_XMLDB_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_CONFIG_XMLDB))

typedef struct _DirData DirData;

struct _DirData {
	char       *name;
	GSList     *entries;
	GSList     *subdirs;
	xmlNodePtr  node;
	DirData    *dir;
};

typedef struct {
	char       *name;
	CORBA_any  *value;
	xmlNodePtr  node;
	DirData    *dir;
} DirEntry;

typedef struct _BonoboConfigXMLDB        BonoboConfigXMLDB;

struct _BonoboConfigXMLDB {
	BonoboConfigDatabase  base;
	
	char                 *filename;
	xmlDocPtr             doc;
	DirData              *dir;
	guint                 time_id;
};

typedef struct {
	BonoboConfigDatabaseClass parent_class;
} BonoboConfigXMLDBClass;


GType		      
bonobo_config_xmldb_get_type  (void);

Bonobo_ConfigDatabase
bonobo_config_xmldb_new (const char *filename);

G_END_DECLS

#endif /* ! __BONOBO_CONFIG_XMLDB_H__ */
