/**
 * bonobo-config-gconfdb.c: GConf based configuration backend
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#include <config.h>
#include <string.h>
#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo-config/bonobo-config-utils.h>

#include "bonobo-config-gconfdb.h"

static GObjectClass *parent_class = NULL;

#define CLASS(o) BONOBO_CONFIG_GCONFDB_CLASS (G_OBJECT_GET_CLASS (o))

#define PARENT_TYPE BONOBO_TYPE_CONFIG_DATABASE

#define ANY_PREFIX "%CORBA:ANY%"

#define SET_BACKEND_FAILED(ev) CORBA_exception_set (ev, CORBA_USER_EXCEPTION, ex_Bonobo_PropertyBag_BackendFailed, NULL)

static CORBA_any *
gconf_to_corba_any (GConfValue *gv)
{ 
	BonoboArg         *value = NULL;
	gchar             *str;
	xmlNode           *node;
	xmlDoc            *doc;
        CORBA_Environment  ev;

	if (!gv)
		return bonobo_arg_new (BONOBO_ARG_NULL);

        CORBA_exception_init (&ev);

	switch (gv->type) {
	case GCONF_VALUE_INVALID:
		return NULL;
	case GCONF_VALUE_INT:
		value = bonobo_arg_new (BONOBO_ARG_INT);
		BONOBO_ARG_SET_LONG (value, gconf_value_get_int (gv));
		return value;
	case GCONF_VALUE_FLOAT:
		value = bonobo_arg_new (BONOBO_ARG_DOUBLE);
		BONOBO_ARG_SET_DOUBLE (value, gconf_value_get_float (gv));
		return value;
	case GCONF_VALUE_BOOL:
		value = bonobo_arg_new (BONOBO_ARG_BOOLEAN);
		BONOBO_ARG_SET_BOOLEAN (value, gconf_value_get_bool (gv));
		return value;
	case GCONF_VALUE_STRING:
		str = (char *)gconf_value_get_string (gv);
		
		if (strncmp (str, ANY_PREFIX, strlen (ANY_PREFIX))) {
			value = bonobo_arg_new (TC_CORBA_string);
			BONOBO_ARG_SET_STRING (value, str);

			return value;
		}

		doc = xmlParseDoc (&str[strlen (ANY_PREFIX)]);
		if (!doc)
			return NULL;

		node = doc->xmlRootNode;
		
		if (!node) {
			xmlFreeDoc (doc);
			return NULL;
		}

		value = bonobo_property_bag_xml_decode_any (node, &ev);
		xmlFreeDoc (doc);

		return value;
	default: 
		return NULL;
	}

	return NULL;
} 

static GConfValue *
corba_any_to_gconf (const CORBA_any *any)
{
	xmlNode           *node;
	xmlDoc            *doc;
	xmlChar           *mem = NULL;
	gchar             *str;
        CORBA_Environment  ev;
	GConfValue        *gv;
	int                size;

        CORBA_exception_init (&ev);

	g_return_val_if_fail (any != NULL, NULL);

	if (bonobo_arg_type_is_equal (any->_type, BONOBO_ARG_STRING, NULL)) {
		gv = gconf_value_new (GCONF_VALUE_STRING);
		gconf_value_set_string (gv, BONOBO_ARG_GET_STRING (any));
		return gv;
	}

	if (bonobo_arg_type_is_equal (any->_type, BONOBO_ARG_INT, NULL)) {
		gv = gconf_value_new (GCONF_VALUE_INT);
		gconf_value_set_int (gv, BONOBO_ARG_GET_INT (any));
		return gv;
	}

	if (bonobo_arg_type_is_equal (any->_type, BONOBO_ARG_DOUBLE, NULL)) {
		gv = gconf_value_new (GCONF_VALUE_FLOAT);
		gconf_value_set_float (gv, BONOBO_ARG_GET_DOUBLE (any));
		return gv;
	}

	if (bonobo_arg_type_is_equal (any->_type, BONOBO_ARG_BOOLEAN, NULL)) {
		gv = gconf_value_new (GCONF_VALUE_BOOL);
		gconf_value_set_bool (gv, BONOBO_ARG_GET_BOOLEAN (any));
		return gv;
	}

	if (!(node = bonobo_property_bag_xml_encode_any (NULL, any, &ev)))
		return NULL;

	doc = xmlNewDoc ("1.0");
	doc->xmlRootNode = xmlCopyNode (node, TRUE);
	xmlDocDumpMemory (doc, &mem, &size);
	xmlFreeDoc (doc);

	str = g_strconcat (ANY_PREFIX, mem, NULL);
	xmlFree (mem);
	xmlFreeNode (node);

	gv = gconf_value_new (GCONF_VALUE_STRING);
	gconf_value_set_string (gv, str);
	g_free (str);
      
	return gv;
}

static gchar *
fixup_key (const char *key)
{
	if (key [0] == '/')
		return g_strdup (key);
	else
		return g_strconcat ("/", key, NULL);
}

static CORBA_any *
real_get_value (BonoboConfigDatabase *db,
		const CORBA_char     *key, 
		CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);
	GConfEntry          *ge;
	CORBA_any           *value = NULL;
	char                *real_key;

	real_key = fixup_key (key);
	ge = gconf_client_get_entry (gconfdb->client, real_key, NULL, 0, NULL);
	g_free (real_key);
	
	if (ge) { 
		value = gconf_to_corba_any (ge->value);
		gconf_entry_free (ge);
	}

	if (!value)
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		
	return value;
}

static void
real_sync (BonoboConfigDatabase *db, 
	   CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);

	gconf_client_suggest_sync (gconfdb->client, NULL);
}


static void
real_set_value (BonoboConfigDatabase *db,
		const CORBA_char     *key, 
		const CORBA_any      *value,
		CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);
	GConfValue          *gv;
	GError              *err = NULL;
	char                *real_key;

	if (!(gv = corba_any_to_gconf (value))) 
		return;

	real_key = fixup_key (key);
	gconf_client_set (gconfdb->client, real_key, gv, &err);
	g_free (real_key);
	
	gconf_value_free (gv);

	if (err) {
		SET_BACKEND_FAILED (ev);
		g_error_free (err);
	}
}

static Bonobo_KeyList *
real_get_dirs (BonoboConfigDatabase *db,
	       const CORBA_char     *dir,
	       CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);
	GSList              *l, *gcl;
	GError              *err = NULL;
	Bonobo_KeyList      *key_list;
	int                  len;

	gcl = gconf_client_all_dirs (gconfdb->client, dir, &err); 

	if (err) {
		SET_BACKEND_FAILED (ev);
		g_error_free (err);
		return NULL;
	}

	key_list = Bonobo_KeyList__alloc ();
	key_list->_length = len = g_slist_length (gcl);

	if (!key_list->_length)
		return key_list;
	
	key_list->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	CORBA_sequence_set_release (key_list, TRUE); 

	for (l = gcl; l != NULL; l = l->next) {
		key_list->_buffer [key_list->_length] = 
			CORBA_string_dup ((char *)l->data);
		g_free (l->data);
		key_list->_length++;
	}
	
	g_slist_free (gcl);

	return key_list;
}

static Bonobo_KeyList *
real_get_keys (BonoboConfigDatabase *db,
	       const CORBA_char     *dir,
	       CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);
	GSList              *l, *gcl;
	GError              *err = NULL;
	Bonobo_KeyList      *key_list;
	GConfEntry          *ge;
	int                  len;

	gcl = gconf_client_all_entries (gconfdb->client, dir, &err);

	if (err) {
		SET_BACKEND_FAILED (ev);
		g_error_free (err);
		return NULL;
	}

	key_list = Bonobo_KeyList__alloc ();
	key_list->_length = len = g_slist_length (gcl);

	if (!key_list->_length)
		return key_list;
	
	key_list->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	CORBA_sequence_set_release (key_list, TRUE); 

	for (l = gcl; l != NULL; l = l->next) {
		ge = (GConfEntry *)l->data;
		key_list->_buffer [key_list->_length] = 
			CORBA_string_dup (ge->key);
		gconf_entry_free (ge);
		key_list->_length++;
	}
	
	g_slist_free (gcl);

	return key_list;
}

static CORBA_boolean
real_has_dir (BonoboConfigDatabase *db,
	      const CORBA_char     *dir,
	      CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);
	GError              *err = NULL;
	gboolean             retval;

	retval = gconf_client_dir_exists (gconfdb->client, dir, &err);

	if (err) {
		SET_BACKEND_FAILED (ev);
		g_error_free (err);
		return FALSE;
	}

	return retval;
}


static void
real_remove_value (BonoboConfigDatabase *db,
		   const CORBA_char     *key, 
		   CORBA_Environment    *ev)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (db);
	GError              *err = NULL;
	char                *real_key;

	real_key = fixup_key (key);
	gconf_client_unset (gconfdb->client, real_key, &err);
	g_free (real_key);

	if (err) {
		SET_BACKEND_FAILED (ev);
		g_error_free (err);
	}
}


static void
real_remove_dir (BonoboConfigDatabase *db,
		 const CORBA_char     *dir, 
		 CORBA_Environment    *ev)
{
	/* fixme: can we simply ignore this? */
}

static void
bonobo_config_gconfdb_finalize (GObject *object)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (object);

	if (gconfdb->es)
		bonobo_object_unref (BONOBO_OBJECT (gconfdb->es));

	if (gconfdb->client) {

		gconf_client_notify_remove (gconfdb->client, gconfdb->nid);

		g_object_unref (G_OBJECT (gconfdb->client));
	}

	parent_class->finalize (object);
}

static void
bonobo_config_gconfdb_class_init (BonoboConfigDatabaseClass *class)
{
	GObjectClass *object_class = (GObjectClass *) class;
	BonoboConfigDatabaseClass *cd_class;

	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = bonobo_config_gconfdb_finalize;

	cd_class = BONOBO_CONFIG_DATABASE_CLASS (class);
		
	cd_class->get_value    = real_get_value;
	cd_class->set_value    = real_set_value;
	cd_class->get_dirs     = real_get_dirs;
	cd_class->get_keys     = real_get_keys;
	cd_class->has_dir      = real_has_dir;
	cd_class->remove_value = real_remove_value;
	cd_class->remove_dir   = real_remove_dir;
	cd_class->sync         = real_sync;
}

static void
bonobo_config_gconfdb_init (BonoboConfigGConfDB *xmldb)
{
	/* nothing to do */
}

BONOBO_TYPE_FUNC (BonoboConfigGConfDB, PARENT_TYPE, bonobo_config_gconfdb);

static void
bonobo_config_gconfdb_notify_listeners (GConfClient* client,
					guint cnxn_id,
					GConfEntry *entry,
					gpointer user_data)
{
	BonoboConfigGConfDB *gconfdb = BONOBO_CONFIG_GCONFDB (user_data);
	char                *ename, *dir_name, *leaf_name;
	CORBA_any           *value;
	CORBA_Environment    ev;

	CORBA_exception_init(&ev);

	value = gconf_to_corba_any (entry->value);

	ename = g_strconcat ("Bonobo/Property:change:", entry->key, NULL);

	bonobo_event_source_notify_listeners(gconfdb->es, ename, value, &ev);

	g_free (ename);

	if (!(dir_name = bonobo_config_dir_name (entry->key)))
		dir_name = g_strdup ("");

	if (!(leaf_name = bonobo_config_leaf_name (entry->key)))
		leaf_name = g_strdup ("");
	
	ename = g_strconcat ("Bonobo/ConfigDatabase:change", dir_name, ":", 
			     leaf_name, NULL);

	bonobo_event_source_notify_listeners (gconfdb->es, ename, value, &ev);
	
	g_free (ename);
	g_free (dir_name);
	g_free (leaf_name);

	CORBA_exception_free (&ev);

	bonobo_arg_release (value);

}

void
bonobo_config_init_gconf_listener (GConfClient *client)
{
	static gboolean bonobo_config_gconf_init = TRUE;

	if (bonobo_config_gconf_init) {
		gconf_client_add_dir (client, "/", GCONF_CLIENT_PRELOAD_NONE,
				      NULL);
		bonobo_config_gconf_init = FALSE;
	}
}

Bonobo_ConfigDatabase
bonobo_config_gconfdb_new ()
{
	BonoboConfigGConfDB   *gconfdb;
	GConfClient           *client;

	if (!gconf_is_initialized())
		gconf_init (0, NULL, NULL);

	if (!(client = gconf_client_get_default ()))
		return NULL;

	if (!(gconfdb = g_object_new (BONOBO_TYPE_CONFIG_GCONFDB, NULL)))
		return CORBA_OBJECT_NIL;

	gconfdb->client = client;

	gconfdb->es = bonobo_event_source_new ();

	BONOBO_CONFIG_DATABASE (gconfdb)->writeable = TRUE;

	bonobo_object_add_interface (BONOBO_OBJECT (gconfdb), 
				     BONOBO_OBJECT (gconfdb->es));

	bonobo_config_init_gconf_listener (client);

	gconfdb->nid = gconf_client_notify_add 
		(client, "/", bonobo_config_gconfdb_notify_listeners, gconfdb, 
		 NULL, NULL);

	return CORBA_Object_duplicate (BONOBO_OBJREF (gconfdb), NULL);
}
