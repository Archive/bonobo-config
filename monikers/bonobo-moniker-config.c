/*
 * bonobo-moniker-config.c: Configuration moniker implementation
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#include <config.h>
#include <string.h>
#include <bonobo/bonobo-moniker.h>
#include <bonobo/bonobo-moniker-util.h>
#include <bonobo/bonobo-moniker-simple.h>
#include <bonobo/bonobo-shlib-factory.h>
#include <bonobo/bonobo-exception.h>

#include <bonobo-config/bonobo-config-bag.h>

#define DEFAULTDB "gconf:"

static Bonobo_Unknown
config_resolve (BonoboMoniker               *moniker,
		const Bonobo_ResolveOptions *options,
		const CORBA_char            *requested_interface,
		CORBA_Environment           *ev)
{
	Bonobo_Moniker         parent;
	Bonobo_ConfigDatabase  db;
	const gchar           *name;

	parent = bonobo_moniker_get_parent (moniker, ev);
	if (BONOBO_EX (ev))
		return CORBA_OBJECT_NIL;

	if (parent != CORBA_OBJECT_NIL) {

		db = Bonobo_Moniker_resolve (parent, options, 
		        "IDL:Bonobo/ConfigDatabase:1.0", ev);

		bonobo_object_release_unref (parent, NULL);
	
	} else 
		db = bonobo_get_object (DEFAULTDB, "Bonobo/ConfigDatabase", 
					ev);

	
	if (BONOBO_EX (ev) || db == CORBA_OBJECT_NIL)
		return CORBA_OBJECT_NIL;
       
	name = bonobo_moniker_get_name (moniker);

	if (!strcmp (requested_interface, "IDL:Bonobo/ConfigDatabase:1.0"))
		return db;

	if (!strcmp (requested_interface, "IDL:Bonobo/PropertyBag:1.0")) {
		BonoboConfigBag *bag;
	
		bag = bonobo_config_bag_new (db, name);

		bonobo_object_release_unref (db, NULL);
	
		if (bag)
			return (Bonobo_Unknown) CORBA_Object_duplicate (
			      BONOBO_OBJREF (bag), ev);
		
	       
		bonobo_exception_set (ev, ex_Bonobo_Moniker_InterfaceNotFound);

		return CORBA_OBJECT_NIL;
	}
	
	return CORBA_OBJECT_NIL; /* try moniker extenders */
}


static BonoboObject *
bonobo_moniker_config_factory (BonoboGenericFactory *this, 
			       const char           *object_id,
			       void                 *closure)
{
	static gboolean initialized = FALSE;

	if (!initialized) {
		initialized = TRUE;
	}

	if (!strcmp (object_id, "OAFIID:Bonobo_Moniker_config")) {

		return BONOBO_OBJECT (bonobo_moniker_simple_new (
		        "config:", config_resolve));
	} else
		g_warning ("Failing to manufacture a '%s'", object_id);
	
	return NULL;
}

BONOBO_OAF_SHLIB_FACTORY ("OAFIID:Bonobo_Moniker_config_Factory",
			  "bonobo configuration moniker",
			  bonobo_moniker_config_factory,
			  NULL);
