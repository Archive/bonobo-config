/**
 * bonobo-config-gconfdb.h: GConf based configuration backend
 *
 * Author:
 *   Dietmar Maurer  (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef __BONOBO_CONFIG_GCONFDB_H__
#define __BONOBO_CONFIG_GCONFDB_H__

#include <bonobo-config/bonobo-config-database.h>
#include <bonobo/bonobo-event-source.h>
#include <gconf/gconf-client.h>

G_BEGIN_DECLS

#define BONOBO_TYPE_CONFIG_GCONFDB        (bonobo_config_gconfdb_get_type ())
#define BONOBO_CONFIG_GCONFDB_TYPE        BONOBO_TYPE_CONFIG_GCONFDB // deprecated, you should use BONOBO_TYPE_CONFIG_GCONFDB
#define BONOBO_CONFIG_GCONFDB(o)	  (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_CONFIG_GCONFDB, BonoboConfigGConfDB))
#define BONOBO_CONFIG_GCONFDB_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_CONFIG_GCONFDB, BonoboConfigGConfDBClass))
#define BONOBO_IS_CONFIG_GCONFDB(o)       (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_CONFIG_GCONFDB))
#define BONOBO_IS_CONFIG_GCONFDB_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_CONFIG_GCONFDB))

typedef struct {
	BonoboConfigDatabase  base;
	
	GConfClient          *client;
	BonoboEventSource    *es;
	guint                 nid;

} BonoboConfigGConfDB;

typedef struct {
	BonoboConfigDatabaseClass parent_class;
} BonoboConfigGConfDBClass;


GType		      
bonobo_config_gconfdb_get_type  (void);

Bonobo_ConfigDatabase
bonobo_config_gconfdb_new ();

G_END_DECLS

#endif /* ! __BONOBO_CONFIG_GCONFDB_H__ */
