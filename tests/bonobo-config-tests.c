/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
#include <bonobo-config-tests.h>

#include "Foo.h"

static gulong test_count = 0;
static gulong failed_tests = 0;

#define CHECK_EV(what) G_STMT_START{					\
	++test_count;							\
	if (BONOBO_EX (&ev)) {						\
		g_warning (G_STRLOC ": Failed to %s (%s): %s",		\
			   (what), path,				\
			   bonobo_exception_get_text (&ev));		\
		CORBA_exception_free (&ev);				\
		++failed_tests;						\
		return FALSE;						\
	}								\
}G_STMT_END

#define CHECK(condition, what, error) G_STMT_START{			\
	++test_count;							\
	if (!(condition)) {						\
		g_warning (G_STRLOC ": Failed to %s (%s): %s",		\
			   (what), path, error);			\
		++failed_tests;						\
		return FALSE;						\
	}								\
}G_STMT_END

Bonobo_ConfigDatabase
bonobo_config_tests_get_db (const gchar *moniker)
{
	Bonobo_ConfigDatabase db = NULL;
        CORBA_Environment ev;

        CORBA_exception_init (&ev);
	db = bonobo_get_object (moniker, "Bonobo/ConfigDatabase", &ev);
	if (BONOBO_EX (&ev))
	    g_error (G_STRLOC ": Can't get moniker `%s': %s", moniker,
		     bonobo_exception_get_text (&ev));

	return db;
}

void
bonobo_config_write_test_summary (void)
{
	if (!failed_tests)
		g_print ("SUCCESS: All %ld tests passed!\n", test_count);
	else
		g_print ("FAILURE: %ld of %ld tests failed\n", failed_tests, test_count);

	exit (failed_tests);
}

gboolean
bonobo_config_read_simple (Bonobo_ConfigDatabase db, const gchar *path,
			   CORBA_TypeCode opt_tc, const CORBA_any *expected_value)
{
        CORBA_Environment ev;
	CORBA_any *any;

        CORBA_exception_init (&ev);

	any = Bonobo_PropertyBag_getValue (db, path, &ev);
	CHECK_EV ("read simple value");
	CHECK ((any != NULL) && (any->_type->kind != CORBA_tk_null),
	       "read simple value", "retval is NULL");

	if (opt_tc != CORBA_OBJECT_NIL)
		CHECK (CORBA_TypeCode_equivalent (opt_tc, any->_type, NULL),
		       "read simple value", "wrong type");

	if (expected_value) {
		CORBA_TypeCode real_tc;

		CHECK (CORBA_TypeCode_equivalent (any->_type, expected_value->_type, NULL),
		       "read simple value", "expected different type");

		real_tc = any->_type;
		while (real_tc->kind == CORBA_tk_alias)
			real_tc = real_tc->subtypes [0];

		switch (real_tc->kind) {

#define MAKE_CHECK_BASIC(k,t)									\
case CORBA_tk_ ## k:										\
	CHECK (BONOBO_ARG_GET_GENERAL (any, TC_CORBA_ ## t, CORBA_ ## t, NULL) ==		\
	       BONOBO_ARG_GET_GENERAL (expected_value, TC_CORBA_ ## t, CORBA_ ## t, NULL),	\
	       "read simple value", "expected different value");					\
	break;

			MAKE_CHECK_BASIC (short,     short);
			MAKE_CHECK_BASIC (long,      long);
			MAKE_CHECK_BASIC (ushort,    unsigned_short);
			MAKE_CHECK_BASIC (ulong,     unsigned_long);
			MAKE_CHECK_BASIC (longlong,  long_long);
			MAKE_CHECK_BASIC (float,     float);
			MAKE_CHECK_BASIC (double,    double);
			MAKE_CHECK_BASIC (ulonglong, unsigned_long_long);
			MAKE_CHECK_BASIC (char,      char);
			MAKE_CHECK_BASIC (octet,     octet);

#undef MAKE_CHECK_BASIC

		default:
			CHECK (FALSE, "read simple value", "not a simple value");
		}
	}

	CORBA_free (any);

	return TRUE;
}

gboolean
bonobo_config_write_simple (Bonobo_ConfigDatabase db, const gchar *path, const CORBA_any *value)
{
        CORBA_Environment ev;
	CORBA_any *any;

        CORBA_exception_init (&ev);

	any = Bonobo_PropertyBag_getValue (db, path, &ev);
	CHECK_EV ("read simple value");

	if (any->_type->kind == CORBA_tk_null) {
		CORBA_free (any);
		any = NULL;
	}

	if (any)
		CHECK (CORBA_TypeCode_equivalent (any->_type, value->_type, NULL),
		       "read simple value", "expected different type");

	Bonobo_PropertyBag_setValue (db, path, value, &ev);
	CHECK_EV ("write simple value");

	if (!bonobo_config_read_simple (db, path, value->_type, value))
		return FALSE;

	if (any) {
		Bonobo_PropertyBag_setValue (db, path, any, &ev);
		CHECK_EV ("write simple value");

		if (!bonobo_config_read_simple (db, path, any->_type, any))
			return FALSE;

		CORBA_free (any);
	}

	return TRUE;
}
