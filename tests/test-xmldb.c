/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
#include <bonobo-config-tests.h>
#include <bonobo-config-xmldb.h>

#include <config.h>
#include <locale.h>
#include <stdlib.h>

int
main (int argc, char **argv)
{
	Bonobo_ConfigDatabase db = NULL;

	setlocale (LC_ALL, "");
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);

	if (!bonobo_init (&argc, argv))
		g_error ("Can not bonobo_init");

	bonobo_activate ();

	db = bonobo_config_xmldb_new ("test-config.xmldb");
	g_assert (db != CORBA_OBJECT_NIL);

	bonobo_config_test_suite_read_simple_values (db);

	bonobo_config_test_suite_write_simple_values (db);

	bonobo_config_write_test_summary ();

	exit (0);
}

