/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
#include <bonobo-config-tests.h>
#include <bonobo-config-ditem.h>
#include <bonobo-config-xmldb.h>

#include <config.h>
#include <locale.h>
#include <stdlib.h>

#include "Foo.h"

static void
boot_ditem (Bonobo_ConfigDatabase db)
{
	BonoboArg *arg;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	arg = bonobo_arg_new (TC_Bonobo_PropertySet);
	bonobo_pbclient_set_value (db, "/bonobo-config-tests", arg, &ev);
	g_assert (!BONOBO_EX (&ev));
	bonobo_arg_release (arg);

	CORBA_exception_init (&ev);
	arg = bonobo_arg_new (TC_Foo_Test_Simple);
	bonobo_pbclient_set_value (db, "/Simple", arg, &ev);
	g_assert (!BONOBO_EX (&ev));
	bonobo_arg_release (arg);

	arg = bonobo_arg_new (TC_Foo_Desktop_Entry);
	bonobo_pbclient_set_value (db, "/Desktop_Entry", arg, &ev);
	g_assert (!BONOBO_EX (&ev));
	bonobo_arg_release (arg);

	Bonobo_ConfigDatabase_sync (db, &ev);
	g_assert (!BONOBO_EX (&ev));
}

int
main (int argc, char **argv)
{
	Bonobo_ConfigDatabase parent_db = NULL, db = NULL;

	setlocale (LC_ALL, "");
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);

	if (!bonobo_init (&argc, argv))
		g_error ("Can not bonobo_init");

	bonobo_activate ();

	parent_db = bonobo_config_xmldb_new ("test-ditem.xmldb");
	g_assert (parent_db != CORBA_OBJECT_NIL);

	boot_ditem (parent_db);

	db = bonobo_config_ditem_new ("test-ditem.desktop",
				      parent_db, NULL);
	g_assert (db != CORBA_OBJECT_NIL);

	bonobo_config_test_suite_read_simple_values (db);

	bonobo_config_test_suite_write_simple_values (db);

	bonobo_config_write_test_summary ();

	exit (0);
}

