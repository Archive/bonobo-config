/*
 * bonobo-config-tets.h: Test functions
 *
 * Author:
 *   Martin Baulig (baulig@suse.de)
 *
 * Copyright 2001 SuSE Linux AG
 */

#ifndef __BONOBO_CONFIG_TESTS_H__
#define __BONOBO_CONFIG_TESTS_H__

#include <libbonobo.h>
#include <bonobo-config/bonobo-config-utils.h>

G_BEGIN_DECLS

Bonobo_ConfigDatabase
bonobo_config_tests_get_db (const gchar *moniker);

void
bonobo_config_write_test_summary (void);

gboolean
bonobo_config_read_simple (Bonobo_ConfigDatabase db, const gchar *path,
			   CORBA_TypeCode opt_tc, const CORBA_any *expected_value);

gboolean
bonobo_config_write_simple (Bonobo_ConfigDatabase db, const gchar *path, const CORBA_any *value);

void
bonobo_config_test_suite_read_simple_values (Bonobo_ConfigDatabase db);

void
bonobo_config_test_suite_write_simple_values (Bonobo_ConfigDatabase db);

G_END_DECLS

#endif /* ! __BONOBO_CONFIG_TESTS_H__ */


