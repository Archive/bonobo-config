/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
#include <bonobo-config-tests.h>
#include <bonobo-config-gconfdb.h>

#include <config.h>
#include <locale.h>
#include <stdlib.h>

int
main (int argc, char **argv)
{
	Bonobo_ConfigDatabase db = NULL;

	if (!bonobo_init (&argc, argv))
		g_error ("Can not bonobo_init");

	bonobo_activate ();

	db = bonobo_config_gconfdb_new ();
	g_assert (db != CORBA_OBJECT_NIL);

	bonobo_config_test_suite_write_simple_values (db);

	bonobo_config_write_test_summary ();

	exit (0);
}

