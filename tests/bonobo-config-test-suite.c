/* -*- Mode: C; c-set-style: gnu indent-tabs-mode: t; c-basic-offset: 8; tab-width: 8 -*- */
#include <bonobo-config-tests.h>

void
bonobo_config_test_suite_read_simple_values (Bonobo_ConfigDatabase db)
{
	BonoboArg *arg;

	arg = bonobo_arg_new (BONOBO_ARG_SHORT);
	BONOBO_ARG_SET_SHORT (arg, 5);
	bonobo_config_read_simple (db, "bonobo-config-tests/simple-short", TC_CORBA_short, arg);
	bonobo_arg_release (arg);

	arg = bonobo_arg_new (BONOBO_ARG_LONG);
	BONOBO_ARG_SET_LONG (arg, 4923435);
	bonobo_config_read_simple (db, "bonobo-config-tests/simple-long", TC_CORBA_long, arg);
	bonobo_arg_release (arg);
}

void
bonobo_config_test_suite_write_simple_values (Bonobo_ConfigDatabase db)
{
	BonoboArg *arg;

	arg = bonobo_arg_new (BONOBO_ARG_SHORT);
	BONOBO_ARG_SET_SHORT (arg, 9);
	bonobo_config_write_simple (db, "bonobo-config-tests/simple-short", arg);
	bonobo_arg_release (arg);

	arg = bonobo_arg_new (BONOBO_ARG_LONG);
	BONOBO_ARG_SET_LONG (arg, 897345);
	bonobo_config_write_simple (db, "bonobo-config-tests/simple-long", arg);
	bonobo_arg_release (arg);
}
