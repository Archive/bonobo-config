/*
 * bonobo-property-bag-editor.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-ui-container.h>
#include <bonobo/bonobo-widget.h>

#include "bonobo-property-editor.h"
#include "gtkwtree.h"
#include "gtkwtreeitem.h"

static GtkWidget *
create_subtree (Bonobo_PropertyEditor  editor,
		Bonobo_UIContainer     uic,
		CORBA_Environment     *ev)
{
	Bonobo_PropertyEditor_PropertyEditorSet *pl;
	GtkWidget *tree, *e, *item, *sub;
	Bonobo_Control control;
	int i;

	pl = Bonobo_PropertyEditor_getSubEditors (editor, ev);
	if (BONOBO_EX (ev) || !pl) 
		return NULL;

	tree = gtk_wtree_new();

	for (i = 0; i < pl->_length; i++) {

		control = pl->_buffer [i].editor;
		bonobo_object_dup_ref (control, NULL);
		e = bonobo_widget_new_control_from_objref (control, uic);
		gtk_widget_show (e);
	
		item = gtk_wtree_item_new_with_widget (pl->_buffer[i].name, e);
		gtk_widget_show (item);

		if ((sub = create_subtree (pl->_buffer [i].editor, uic, ev))) {
			gtk_wtree_item_set_subwtree (GTK_WTREE_ITEM (item),
						     sub);
		}

		gtk_wtree_append (GTK_WTREE (tree), item);
	}

	CORBA_free (pl);

	return tree;
}

BonoboControl *
bonobo_property_bag_editor_new (Bonobo_PropertyBag  bag,
				Bonobo_UIContainer  uic,
				CORBA_Environment  *ev)
{
	GtkWidget           *tree, *sub, *item;
	Bonobo_PropertyList *plist;
	int                  i;

	tree = gtk_wtree_new();
	
	plist = Bonobo_PropertyBag_getProperties (bag, ev);
      	
	if (BONOBO_EX (ev))
		return CORBA_OBJECT_NIL;
	
	for (i = 0; i < plist->_length; i++) {
		CORBA_TypeCode tc;
		GtkWidget *e;
		char      *pn;
		CORBA_Object editor;

		CORBA_exception_init (ev);
		
		pn = Bonobo_Property_getName (plist->_buffer [i], ev);
		if (BONOBO_EX (ev))
			continue;

		tc = Bonobo_Property_getType (plist->_buffer [i], ev);
		if (BONOBO_EX (ev)) {
			CORBA_free (pn);
			continue;
		}

		editor = bonobo_property_editor_resolve (tc, ev);

		CORBA_Object_release ((CORBA_Object)tc, ev);

		if (BONOBO_EX (ev) || editor == CORBA_OBJECT_NIL) {
			CORBA_free (pn);
			continue;
		}	
	
		e = bonobo_widget_new_control_from_objref (editor, uic);
		gtk_widget_show (e);

		Bonobo_PropertyEditor_setProperty (editor, plist->_buffer [i],
						   ev);

		if (BONOBO_EX (ev)) {
			CORBA_free (pn);
			continue;
		}

		item = gtk_wtree_item_new_with_widget (pn, e);
		gtk_wtree_append (GTK_WTREE (tree), item);

		CORBA_free (pn);

		if ((sub = create_subtree (editor, uic, ev))) {
			gtk_wtree_item_set_subwtree (GTK_WTREE_ITEM (item),
						     sub);
		}

	}

	CORBA_free (plist);

	gtk_widget_show_all (tree);

	return bonobo_control_new (tree);
}

