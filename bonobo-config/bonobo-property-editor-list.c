/**
 * bonobo-property-editor-list.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#include <config.h>
#include <gtk/gtk.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-widget.h>

#include "bonobo-property-editor-list.h"
#include "bonobo-subproperty.h"

#define PARENT_TYPE (bonobo_property_editor_get_type ())

/* Parent object class in GTK hierarchy */
static BonoboPropertyEditorClass *bonobo_pe_list_parent_class;

struct _BonoboPEListPrivate {
	GtkAdjustment *ad;
	CORBA_any *value;
	GtkWidget *widget;
	GtkWidget *subwidget;
	GtkWidget *sb;
	BonoboEventSource *es;
	Bonobo_PropertyEditor subed;
	BonoboSubProperty *subprop;
	gint cpos;
};

static void
bonobo_pe_list_destroy (GtkObject *object)
{
	BonoboPEList *editor = BONOBO_PE_LIST (object);

	/* fixme: cleanup everything */

	if (editor->priv->es)
		bonobo_object_unref (BONOBO_OBJECT (editor->priv->es));

	if (editor->priv->value)
		CORBA_free (editor->priv->value);

	g_free (editor->priv);

	GTK_OBJECT_CLASS (bonobo_pe_list_parent_class)->destroy (object);
}

static void 
subproperty_change_cb (BonoboPropertyEditor *editor,
		       const CORBA_any      *value,
		       int                   index)
{
	BonoboPEListPrivate *priv = BONOBO_PE_LIST (editor)->priv;
	DynamicAny_DynAny dyn;
	DynamicAny_DynAny_AnySeq *seq;
	CORBA_Environment ev;
	CORBA_any *nv;
	gpointer src, dest;
	CORBA_TypeCode tc;

	CORBA_exception_init (&ev);

	dyn = CORBA_ORB_create_dyn_any (bonobo_orb (), priv->value, &ev);
	if (BONOBO_EX (&ev) || dyn == CORBA_OBJECT_NIL)
		return;

	seq = DynamicAny_DynSequence_get_elements (dyn, &ev);
	if (BONOBO_EX (&ev) || seq == NULL)
		return;

	/* fixme: ugly hack */
	src = value;
	dest = &seq->_buffer [priv->cpos];
	tc = (CORBA_TypeCode) CORBA_Object_duplicate((CORBA_Object) TC_any, 
						     NULL);
	ORBit_free_via_TypeCode (dest, &tc, TRUE);
	_ORBit_copy_value (&src, &dest, tc);

	DynamicAny_DynSequence_set_elements (dyn, seq, &ev);
	if (BONOBO_EX (&ev))
		return;

	nv = DynamicAny_DynAny_to_any (dyn, &ev);
	if (BONOBO_EX (&ev) || nv == NULL)
		return;

	bonobo_property_editor_set_value (editor, nv, NULL);

	CORBA_Object_release ((CORBA_Object) dyn, &ev);

	CORBA_free (nv);

	CORBA_free (seq);

	CORBA_exception_free (&ev);
}

static CORBA_any *
get_subval (CORBA_any *value, gint pos, CORBA_Environment *ev)
{
	DynamicAny_DynAny dyn, cur_dyn;
	CORBA_any *subval = NULL;


	dyn = CORBA_ORB_create_dyn_any (bonobo_orb (), value, ev);
	if (BONOBO_EX (ev) || dyn == CORBA_OBJECT_NIL)
		return NULL;

	if (!DynamicAny_DynAny_seek (dyn, pos, ev))
		return NULL;

	cur_dyn = DynamicAny_DynAny_current_component (dyn, ev);
	if (BONOBO_EX (ev) || cur_dyn == CORBA_OBJECT_NIL) 
		return NULL;

	subval = DynamicAny_DynAny_to_any (cur_dyn, ev);
	if (BONOBO_EX (ev) || subval == NULL) 
		return NULL;
	
	CORBA_Object_release ((CORBA_Object) cur_dyn, ev);

	CORBA_Object_release ((CORBA_Object) dyn, ev);

	return subval;
}

static void 
real_pe_set_value (BonoboPropertyEditor *editor,
		   BonoboArg            *value,
		   CORBA_Environment    *ev)
{
	BonoboPEListPrivate *priv = BONOBO_PE_LIST (editor)->priv;
	CORBA_any *subval;
	int length;

	if (value->_type->kind != CORBA_tk_sequence) {
		bonobo_exception_set (ev,ex_Bonobo_PropertyEditor_InvalidType);
		return;
	}

	if (!priv->value) { /* first time */
		Bonobo_PropertyEditor subed;
		Bonobo_UIContainer uic;
		CORBA_TypeCode tc;
		GtkWidget *e;
	
		tc = value->_type->subtypes [0];
		subed = bonobo_property_editor_resolve (tc, ev);

		if (BONOBO_EX (ev) || subed == CORBA_OBJECT_NIL)
			return;

		uic = bonobo_control_get_remote_ui_container 
			(BONOBO_CONTROL (editor));

		if (uic == CORBA_OBJECT_NIL)
			return;

		e = bonobo_widget_new_control_from_objref (subed, uic);
		if (!e)
			return;

		gtk_widget_show (e);
		
		if (priv->subwidget) {
			gtk_container_remove (GTK_CONTAINER (priv->widget),
					      priv->subwidget);
			priv->subwidget = NULL;
		}

		priv->subwidget = e;

		gtk_box_pack_start (GTK_BOX (priv->widget), 
				    priv->subwidget,
				    TRUE, TRUE, 0);

		priv->subed = subed;

	} else {
		if (bonobo_arg_is_equal (priv->value, value, NULL))
			return;

		CORBA_free (priv->value);
	}
	
	priv->value = bonobo_arg_copy (value);

	if (priv->subed == CORBA_OBJECT_NIL)
		return;

	length = ((CORBA_sequence_octet *)priv->value->_value)->_length;

	if (priv->cpos >= length)
		priv->cpos = 0;

	priv->ad->upper = length - 1;
	priv->ad->value = priv->cpos;

	gtk_adjustment_changed (priv->ad);

	if (!length)
		return;

	subval = get_subval (priv->value, priv->cpos, ev);
	if (BONOBO_EX (ev) || subval == NULL) 
		return;

	if (!priv->subprop) {
		priv->subprop = bonobo_sub_property_new 
			(editor, "listmember", subval, priv->cpos,
			 priv->es, subproperty_change_cb);

		Bonobo_PropertyEditor_setProperty 
			(priv->subed, BONOBO_OBJREF (priv->subprop), ev);
	} else 
		bonobo_sub_property_set_value (priv->subprop, subval);	
	
	CORBA_free (subval);
}

static void
changed_cpos_cb (GtkAdjustment *ad,
		 gpointer     user_data) 
{
	BonoboPEListPrivate *priv = BONOBO_PE_LIST (user_data)->priv;
	CORBA_Environment ev;
	CORBA_any *subval;
	int length;

	if (!priv->value) 
		return;
	
	length = ((CORBA_sequence_octet *)priv->value->_value)->_length;

	if (!length)
		return;
	
	CORBA_exception_init (&ev);

	if (((gint) ad->value) == priv->cpos) 
		return;
	
	priv->cpos = ad->value;

	subval = get_subval (priv->value, priv->cpos, &ev);
	if (BONOBO_EX (&ev) || subval == NULL) 
		return;

	bonobo_sub_property_set_value (priv->subprop, subval);	

	CORBA_free (subval);

	CORBA_exception_free (&ev);
}

static Bonobo_PropertyEditor_PropertyEditorSet *
real_pe_get_sub_editors (BonoboPropertyEditor *editor,
			 CORBA_Environment    *ev)
{
	BonoboPEListPrivate *priv = BONOBO_PE_LIST (editor)->priv;

	if (priv->subed == CORBA_OBJECT_NIL)
	  return NULL;

	return Bonobo_PropertyEditor_getSubEditors (priv->subed, ev);
}

static void
bonobo_pe_list_class_init (BonoboPEListClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	BonoboPropertyEditorClass *pec = (BonoboPropertyEditorClass *)klass;

	bonobo_pe_list_parent_class = gtk_type_class (PARENT_TYPE);

	pec->set_value = real_pe_set_value;
	pec->get_sub_editors = real_pe_get_sub_editors;

	object_class->destroy = bonobo_pe_list_destroy;
}

static void
bonobo_pe_list_init (BonoboPEList *editor)
{
	editor->priv = g_new0 (BonoboPEListPrivate, 1);
}


BONOBO_X_TYPE_FUNC (BonoboPEList, PARENT_TYPE, bonobo_pe_list);

BonoboObject *
bonobo_property_editor_sequence_new ()
{
	BonoboPEList *ed;

	ed = gtk_type_new (bonobo_pe_list_get_type ());

	ed->priv->ad = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, 0.0, 0.0, 1.0,
							   1.0, 1.0));

	ed->priv->sb = gtk_spin_button_new (GTK_ADJUSTMENT (ed->priv->ad), 
					    1.0, 0);

	ed->priv->widget = gtk_hbox_new (FALSE, 0);

	gtk_box_pack_end (GTK_BOX (ed->priv->widget), 
			  ed->priv->sb,
			  FALSE, FALSE, 0);

	ed->priv->subwidget = gtk_label_new ("no value set");

	gtk_box_pack_start (GTK_BOX (ed->priv->widget), 
			    ed->priv->subwidget,
			    TRUE, TRUE, 0);


	gtk_widget_show_all (ed->priv->widget);

	bonobo_control_construct (BONOBO_CONTROL (ed), ed->priv->widget);
 
	ed->priv->es = bonobo_event_source_new ();

	gtk_signal_connect (GTK_OBJECT (ed->priv->ad), "value-changed",
			    (GtkSignalFunc) changed_cpos_cb, ed);
	
	return BONOBO_OBJECT (ed);
}
