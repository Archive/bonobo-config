/**
 * bonobo-property-editor.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-widget.h>

#include "bonobo-property-editor.h"

#define CLASS(o) BONOBO_PROPERTY_EDITOR_CLASS (GTK_OBJECT(o)->klass)

#define TC_OBJECT_FROM_SERVANT(servant) \
BONOBO_PROPERTY_EDITOR (bonobo_object_from_servant (servant));

/* Parent object class in GTK hierarchy */
static GtkObjectClass *bonobo_property_editor_parent_class;

struct _BonoboPropertyEditorPrivate {
	Bonobo_Property               property;
	BonoboPropertyEditorSetFn     set_cb;
	Bonobo_EventSource_ListenerId id;
};

#define  TYPE_PRINT(tckind)			                        \
	case tckind:							\
                printf (#tckind "\n");           			\
                break;

void
print_typecode (CORBA_TypeCode tc, const gchar *name, int level)
{
        int i, j;

	for (i=0;i<level;i++) printf(" ");          
	if (name) printf ("(%s)", name);
	printf ("(%d)(%p)", ORBIT_ROOT_OBJECT(tc)->refs, tc);

	switch (tc->kind) {

		TYPE_PRINT(CORBA_tk_null);
		TYPE_PRINT(CORBA_tk_void);
		TYPE_PRINT(CORBA_tk_short);
		TYPE_PRINT(CORBA_tk_long);
		TYPE_PRINT(CORBA_tk_ushort);
		TYPE_PRINT(CORBA_tk_ulong);
		TYPE_PRINT(CORBA_tk_float);
		TYPE_PRINT(CORBA_tk_double);
		TYPE_PRINT(CORBA_tk_boolean);
		TYPE_PRINT(CORBA_tk_char);
		TYPE_PRINT(CORBA_tk_octet);
		TYPE_PRINT(CORBA_tk_string);
		TYPE_PRINT(CORBA_tk_longlong);
		TYPE_PRINT(CORBA_tk_ulonglong);
		TYPE_PRINT(CORBA_tk_longdouble);
		TYPE_PRINT(CORBA_tk_wchar);
		TYPE_PRINT(CORBA_tk_wstring);
		TYPE_PRINT(CORBA_tk_objref);
		TYPE_PRINT(CORBA_tk_any);
		TYPE_PRINT(CORBA_tk_TypeCode);
		
	case CORBA_tk_struct:
		for (i=0;i<level;i++) printf(" "); 
		printf ("CORBA_tk_struct %s\n", tc->repo_id);
		for (i = 0; i < tc->sub_parts; i++) {
			print_typecode (tc->subtypes [i], tc->subnames [i],
					level+2);
		}
		break;

	case CORBA_tk_sequence:
		for (i=0;i<level;i++) printf(" "); 
		printf ("CORBA_tk_sequence\n");
		print_typecode (tc->subtypes [0], NULL, level+2);
		break;

	case CORBA_tk_alias:
		for (i=0;i<level;i++) printf(" "); 
		printf ("CORBA_tk_alias %p %p %s\n", tc, tc->repo_id,
			tc->repo_id);
		print_typecode (tc->subtypes [0], NULL, level+2);
		break;

	case CORBA_tk_enum:
		for (i=0;i<level;i++) printf(" "); 
		printf ("CORBA_tk_enum %p %p %s\n", tc, tc->repo_id,
			tc->repo_id);
		for (j = 0; j < tc->sub_parts; j++) {
			for (i=0;i<level+2;i++) printf(" "); 
			printf ("%s\n", tc->subnames [j]);
		}
		break;

	default:
		for (i=0;i<level;i++) printf(" "); 
		printf ("Unknown Type\n");
	
	}

}

static void
int_set_value (BonoboPropertyEditor *editor, CORBA_any *any,
	       CORBA_Environment *ev)
{
	CORBA_any nv;

	/* fixme: support nested aliases */

	if (any->_type->kind == CORBA_tk_alias) {

		nv._type =  any->_type->subtypes [0];
		nv._value = any->_value;

		if (editor->priv->set_cb)
			editor->priv->set_cb (editor, &nv, ev);
		else if (CLASS (editor)->set_value)
			CLASS (editor)->set_value (editor, &nv, ev);

	} else {
		if (editor->priv->set_cb)
			editor->priv->set_cb (editor, any, ev);
		else if (CLASS (editor)->set_value)
			CLASS (editor)->set_value (editor, any, ev);
	}
}

static void 
value_changed_cb (BonoboListener    *listener,
		  char              *event_name, 
		  CORBA_any         *any,
		  CORBA_Environment *ev,
		  gpointer           user_data)
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (user_data);
	
	if (!bonobo_arg_type_is_equal (any->_type, editor->tc, ev)) {
		bonobo_exception_set 
			(ev, ex_Bonobo_PropertyEditor_InvalidType);
		g_warning ("property type change (changed cb) %d %d", 
			   any->_type->kind, editor->tc->kind);
		return;
	}

	int_set_value (editor, any, ev);
}

static void
impl_Bonobo_PropertyEditor_setProperty (PortableServer_Servant  servant,
					const Bonobo_Property   property,
					CORBA_Environment      *ev)
{
	BonoboArg *arg;
	BonoboPropertyEditor *editor = TC_OBJECT_FROM_SERVANT (servant);

	arg = Bonobo_Property_getValue (property, ev);

	if (BONOBO_EX (ev) || arg == NULL)
		return;

	/* fixme: use bonobo_object_release_unref instead */
	if (editor->priv->property) 
		CORBA_Object_release (property, NULL);
	   
	editor->priv->property = CORBA_Object_duplicate (property, NULL);

	if (editor->priv->id && editor->priv->property != CORBA_OBJECT_NIL)
		bonobo_event_source_client_remove_listener 
			(editor->priv->property, editor->priv->id, NULL);

	editor->priv->id = bonobo_event_source_client_add_listener 
		(property, value_changed_cb, NULL, ev, editor);

	if (bonobo_arg_type_is_equal (arg->_type, TC_null, NULL)) {
		bonobo_arg_release (arg);
		return;
	}

	if (editor->tc == CORBA_OBJECT_NIL)
		editor->tc = (CORBA_TypeCode)CORBA_Object_duplicate 
			((CORBA_Object) arg->_type, NULL);

	int_set_value (editor, arg, ev);
		
	bonobo_arg_release (arg);
}

static Bonobo_PropertyEditor_PropertyEditorSet *
impl_Bonobo_PropertyEditor_getSubEditors (PortableServer_Servant  servant,
					  CORBA_Environment      *ev)
{
	BonoboPropertyEditor *editor = TC_OBJECT_FROM_SERVANT (servant);

	if (CLASS (editor)->get_sub_editors) 
		return CLASS (editor)->get_sub_editors (editor, ev);
      
	return CORBA_OBJECT_NIL;
}

static void
bonobo_property_editor_destroy (GtkObject *object)
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (object);
	CORBA_Environment ev;
	
	CORBA_exception_init (&ev);

	if (editor->priv->id && editor->priv->property != CORBA_OBJECT_NIL)
		bonobo_event_source_client_remove_listener (
	                editor->priv->property, editor->priv->id, NULL);
		
	/* fixme: use bonobo_object_release_unref instead */
	if (editor->priv->property)
		CORBA_Object_release ((CORBA_Object) editor->priv->property, 
				      &ev);

	if (editor->tc)
		CORBA_Object_release ((CORBA_Object) editor->tc, &ev);


	CORBA_exception_free (&ev);	

	bonobo_property_editor_parent_class->destroy (object);
}

static void 
real_property_editor_set_value (BonoboPropertyEditor *editor,
				BonoboArg            *value,
				CORBA_Environment    *ev)
{
	g_warning ("calling default set_value implementation");
}

static void
bonobo_property_editor_class_init (BonoboPropertyEditorClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	POA_Bonobo_PropertyEditor__epv *epv = &klass->epv;

	bonobo_property_editor_parent_class = 
		gtk_type_class (bonobo_control_get_type ());

	klass->set_value = real_property_editor_set_value;
	klass->get_sub_editors = NULL;

	object_class->destroy = bonobo_property_editor_destroy;

	epv->setProperty   = impl_Bonobo_PropertyEditor_setProperty;
	epv->getSubEditors = impl_Bonobo_PropertyEditor_getSubEditors;
}

static void
bonobo_property_editor_init (BonoboPropertyEditor *editor)
{
	editor->priv = g_new0 (BonoboPropertyEditorPrivate, 1);
}


BONOBO_X_TYPE_FUNC_FULL (BonoboPropertyEditor, 
			 Bonobo_PropertyEditor, 
			 bonobo_control_get_type (), 
			 bonobo_property_editor);


BonoboPropertyEditor *
bonobo_property_editor_new (GtkWidget *widget, 
			    BonoboPropertyEditorSetFn set_cb,
			    CORBA_TypeCode tc)
{
	BonoboPropertyEditor *editor;
	
	g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);
	g_return_val_if_fail (set_cb != NULL, NULL);

	editor = gtk_type_new (bonobo_property_editor_get_type ());

	if (tc != CORBA_OBJECT_NIL)
		editor->tc = (CORBA_TypeCode)CORBA_Object_duplicate 
			((CORBA_Object) tc, NULL);

	editor->priv->set_cb = set_cb;

	bonobo_control_construct (BONOBO_CONTROL (editor), widget);
 
	return editor;
}

void                
bonobo_property_editor_set_value (BonoboPropertyEditor *editor,
				  const BonoboArg      *value,
				  CORBA_Environment    *opt_ev)
{
	CORBA_Environment  ev, *my_ev;
	CORBA_any nv;

	bonobo_return_if_fail (editor != NULL, opt_ev);
	bonobo_return_if_fail (BONOBO_IS_PROPERTY_EDITOR(editor), opt_ev);
	bonobo_return_if_fail (value != NULL, opt_ev);

	if (!editor->priv->property)
		return;

	if (!opt_ev) {
		CORBA_exception_init (&ev);
		my_ev = &ev;
	} else
		my_ev = opt_ev;

	if (editor->tc->kind == CORBA_tk_alias && 
	    bonobo_arg_type_is_equal (value->_type, editor->tc->subtypes[0], 
				      my_ev)) {
		
		nv._type =  editor->tc;
		nv._value = value->_value;

		Bonobo_Property_setValue (editor->priv->property, &nv, my_ev);
		
	} else {
		if (!bonobo_arg_type_is_equal (value->_type, editor->tc, 
					       my_ev)) {
			bonobo_exception_set 
				(opt_ev, ex_Bonobo_PropertyEditor_InvalidType);
			g_warning ("property type change %d %d", 
				   value->_type->kind, editor->tc->kind);
			return;
		}
		Bonobo_Property_setValue (editor->priv->property, value, 
					  my_ev);
	}

	if (!opt_ev)
		CORBA_exception_free (&ev);
}

static char *
tckind_to_string (CORBA_TCKind kind)
{
	switch (kind) {

	case CORBA_tk_short:    return "short";
	case CORBA_tk_long:     return "long";
	case CORBA_tk_ushort:   return "ushort";
	case CORBA_tk_ulong:    return "ulong";
	case CORBA_tk_float:    return "float";
	case CORBA_tk_double:   return "double";
	case CORBA_tk_boolean:  return "boolean";
	case CORBA_tk_char:     return "char";
	case CORBA_tk_struct:   return "struct";
	case CORBA_tk_enum:     return "enum";
	case CORBA_tk_string:   return "string";
	case CORBA_tk_sequence: return "sequence";
	case CORBA_tk_array:    return "array";
	default:                return "default";

	}
}

Bonobo_PropertyEditor
bonobo_property_editor_resolve (CORBA_TypeCode tc,  
				CORBA_Environment *ev)
{
	Bonobo_PropertyEditor editor;
	char *oafiid;
	int kind;

	bonobo_return_val_if_fail (tc != NULL, CORBA_OBJECT_NIL, ev);

	if (tc->repo_id && tc->repo_id[0]) {

		oafiid = g_strconcat (BONOBO_PROPERTY_EDITOR_PREFIX, 
				      tc->repo_id, NULL);

		editor = bonobo_activaiton_activate_from_id (oafiid, 0, NULL, ev);

		g_free (oafiid);

		if (!BONOBO_EX (ev) && editor != CORBA_OBJECT_NIL)
			return editor;

		CORBA_exception_init (ev); /* clear all errors */

		if (tc->kind == CORBA_tk_alias) {
			oafiid = g_strconcat (BONOBO_PROPERTY_EDITOR_PREFIX, 
					      tc->subtypes [0]->repo_id, 
					      NULL);
			
			editor = bonobo_activation_activate_from_id (oafiid, 0, NULL, ev);

			g_free (oafiid);

			if (!BONOBO_EX (ev) && editor != CORBA_OBJECT_NIL)
				return editor;

			CORBA_exception_init (ev); /* clear all errors */
		}
	}

	
	if (tc->kind == CORBA_tk_alias)
		kind = tc->subtypes [0]->kind;
	else
		kind = tc->kind;

	oafiid = g_strconcat (BONOBO_PROPERTY_EDITOR_PREFIX, 
			      tckind_to_string (kind), NULL);

	editor = bonobo_activation_activate_from_id (oafiid, 0, NULL, ev);
	
	g_free (oafiid);

	return editor;
}

GtkWidget *
bonobo_config_widget_new (CORBA_TypeCode      tc, 
			  Bonobo_PropertyBag  pb,
			  const char         *name,
			  CORBA_any          *defval,
			  CORBA_Environment  *opt_ev)
{
	Bonobo_PropertyEditor pe;
	Bonobo_Property property;
	CORBA_Environment ev, *my_ev;
	CORBA_any *value;
	GtkWidget *w;

	bonobo_return_val_if_fail (tc != CORBA_OBJECT_NIL, NULL, opt_ev);
	bonobo_return_val_if_fail (pb != CORBA_OBJECT_NIL, NULL, opt_ev);
	bonobo_return_val_if_fail (name != NULL, NULL, opt_ev);

	if (!opt_ev) {
		CORBA_exception_init (&ev);
		my_ev = &ev;
	} else
		my_ev = opt_ev;

	property = Bonobo_PropertyBag_getPropertyByName (pb, name, my_ev);
	if (BONOBO_EX (my_ev) || property == CORBA_OBJECT_NIL) {
		if (!opt_ev)
			CORBA_exception_free (&ev);
		return NULL;
	}

	value = Bonobo_Property_getValue (property, my_ev);
	if (BONOBO_EX (my_ev) || value == NULL) {
		bonobo_object_release_unref (property, NULL);
		if (!opt_ev)
			CORBA_exception_free (&ev);
		return NULL;
	}

	if (bonobo_arg_type_is_equal (value->_type, TC_null, NULL)) {
		CORBA_free (value);
		
		if (defval) {
			Bonobo_Property_setValue (property, defval, my_ev);
		} else {
			value = bonobo_arg_new (tc);
			Bonobo_Property_setValue (property, value, my_ev);
			CORBA_free (value);
		}
	} else	
		CORBA_free (value);

	pe = bonobo_property_editor_resolve (tc, my_ev);
	if (BONOBO_EX (my_ev) || pe == CORBA_OBJECT_NIL) {
		bonobo_object_release_unref (property, NULL);
		if (!opt_ev)
			CORBA_exception_free (&ev);
		return NULL;
	}

	Bonobo_PropertyEditor_setProperty (pe, property, my_ev);
	
	bonobo_object_release_unref (property, NULL);

	if (BONOBO_EX (my_ev)) {
		bonobo_object_release_unref (pe, NULL);
		if (!opt_ev)
			CORBA_exception_free (&ev);
		return NULL;
	}

	w = bonobo_widget_new_control_from_objref (pe, NULL);

	if (!opt_ev)
		CORBA_exception_free (&ev);

	return w;
}
