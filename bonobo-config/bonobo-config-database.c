/**
 * bonobo-config-database.c: config database object implementation.
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#include <config.h>
#include <string.h>

#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-arg.h>

#include "bonobo-config-database.h"

static GObjectClass *parent_class = NULL;

#define CLASS(o) BONOBO_CONFIG_DATABASE_CLASS (G_OBJECT_GET_CLASS (o))

#define PARENT_TYPE (BONOBO_TYPE_X_OBJECT)

#define DATABASE_FROM_SERVANT(servant) \
(BONOBO_CONFIG_DATABASE (bonobo_object (servant)))

typedef struct {
	Bonobo_ConfigDatabase db;
	char *path;
} DataBaseInfo;

struct _BonoboConfigDatabasePrivate {
	GList             *db_list;
	BonoboEventSource *es;
};

/**
 * bonobo_config_dir_name:
 * @key: a configuration key 
 *
 * Gets the directory components of a configuration key. If @key does not 
 * contain a directory component it returns %NULL.
 *
 * Returns: the directory components of a configuration key.
 */
char *
bonobo_config_dir_name (const char *key)
{
	char *s;

	g_return_val_if_fail (key != NULL, NULL);
	
	if (!(s = strrchr (key, '/')))
		return NULL;
       
	while (s > key && *(s - 1) == '/')
		s--;

	if (s == key)
		return NULL;

	return g_strndup (key, s - key);
}

/**
 * bonobo_config_leaf_name:
 * @key: a configuration key 
 *
 * Gets the name of a configuration key without the leading directory 
 * component.
 *
 * Returns: the name of a configuration key without the leading directory 
 * component.
 */
char *
bonobo_config_leaf_name (const char *key)
{
	char *s;

	g_return_val_if_fail (key != NULL, NULL);
	
	if (!(s = strrchr (key, '/'))) {
		if (key [0])
			return g_strdup (key);
		return NULL;
	}

	if (s[1])
		return g_strdup (s + 1);
	
	return NULL;
}

static void
insert_key_name (gpointer	key,
		 gpointer	value,
		 gpointer	user_data)
{
	Bonobo_KeyList *key_list = (Bonobo_KeyList *)user_data;

	key_list->_buffer [key_list->_length++] = CORBA_string_dup (key);
}

Bonobo_KeyList *
merge_keylists (Bonobo_KeyList *cur_list, 
		Bonobo_KeyList *def_list)
{
	Bonobo_KeyList *key_list;
	GHashTable     *ht;
	int             i, len;

	ht =  g_hash_table_new (g_str_hash, g_str_equal);

	for (i = 0; i < cur_list->_length; i++) 
		g_hash_table_insert (ht, cur_list->_buffer [i], NULL);

	for (i = 0; i < def_list->_length; i++)
		g_hash_table_insert (ht, def_list->_buffer [i], NULL);

	len =  g_hash_table_size (ht);

	key_list = Bonobo_KeyList__alloc ();
	key_list->_length = 0;
	key_list->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	CORBA_sequence_set_release (key_list, TRUE); 

	g_hash_table_foreach (ht, insert_key_name, key_list);

	g_hash_table_destroy (ht);

	return key_list;
}

static CORBA_any *
get_default (BonoboConfigDatabase   *cd,
	     const CORBA_char       *key, 
	     CORBA_Environment      *ev)
{
	CORBA_any *value = NULL;
	DataBaseInfo *info;
	GList *l;

	for (l = cd->priv->db_list; l != NULL; l = l->next) {
		info = (DataBaseInfo *)l->data;

		value = Bonobo_ConfigDatabase_getValue (info->db, key, ev);
		if (BONOBO_EX (ev))
			return NULL;

		if (value)
			return value;

	}

	bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
	return NULL;
}

static CORBA_any *
get_value_with_default (BonoboConfigDatabase   *cd,
			const CORBA_char       *key, 
			CORBA_Environment      *ev)
{
	CORBA_any *value = NULL;

	if (CLASS (cd)->get_value)
		value = CLASS (cd)->get_value (cd, key, ev);

	if (!BONOBO_EX (ev) && value)
		return value;

	CORBA_exception_init (ev);

	return get_default (cd, key, ev);
}

static void
notify_listeners (BonoboConfigDatabase *cd, 
		  const char           *key, 
		  const CORBA_any      *value)
{
	CORBA_Environment ev;
	char *dir_name;
	char *leaf_name;
	char *ename;

	if (!key || !cd->priv->es)
		return;

	CORBA_exception_init(&ev);

	ename = g_strconcat ("Bonobo/Property:change:", key, NULL);

	bonobo_event_source_notify_listeners(cd->priv->es, ename, value, &ev);

	g_free (ename);
	
	if (!(dir_name = bonobo_config_dir_name (key)))
		dir_name = g_strdup ("");
	
	leaf_name = bonobo_config_leaf_name (key);
	
	ename = g_strconcat ("Bonobo/ConfigDatabase:change", dir_name, ":", 
			     leaf_name, NULL);

	bonobo_event_source_notify_listeners (cd->priv->es, ename, value, &ev);
						   
	CORBA_exception_free (&ev);

	g_free (ename);
	g_free (dir_name);
	g_free (leaf_name);
}

static Bonobo_KeyList *
impl_Bonobo_PropertyBag_getKeys (PortableServer_Servant  servant,
				 const CORBA_char       *filter,
				 CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);
	CORBA_Environment nev;
	DataBaseInfo *info;
	GList *l;
	Bonobo_KeyList *cur_list, *def_list, *tmp_list;

	cur_list = NULL;

	if (CLASS (cd)->get_keys)
		cur_list = CLASS (cd)->get_keys (cd, filter, ev);
	
	if (BONOBO_EX (ev))
		return NULL;

	CORBA_exception_init (&nev);

	for (l = cd->priv->db_list; l != NULL; l = l->next) {
		info = (DataBaseInfo *)l->data;

		CORBA_exception_init (&nev);

		def_list = Bonobo_ConfigDatabase_getKeys (info->db, filter, 
							  &nev);
		if (!BONOBO_EX (&nev) && def_list) {

			if (!def_list->_length) {
				CORBA_free (def_list);
				continue;
			}

			if (!cur_list || !cur_list->_length) {
				if (cur_list)
					CORBA_free (cur_list);
				cur_list = def_list;
				continue;
			}

			tmp_list = merge_keylists (cur_list, def_list);
			
			CORBA_free (cur_list);
			CORBA_free (def_list);

			cur_list = tmp_list;
		}
	}

	CORBA_exception_free (&nev);

	return cur_list;
}

static CORBA_TypeCode
impl_Bonobo_PropertyBag_getType (PortableServer_Servant  servant,
				 const CORBA_char       *key,
				 CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);
	CORBA_any *value = NULL;
	CORBA_TypeCode tc = CORBA_OBJECT_NIL;

	if (CLASS (cd)->get_value)
		value = CLASS (cd)->get_value (cd, key, ev);

	if (!BONOBO_EX (ev) && value) {
		tc = (CORBA_TypeCode)CORBA_Object_duplicate 
			((CORBA_Object)value->_type, NULL);
		CORBA_free (value);
		return tc;
	}

	CORBA_exception_init (ev);

	if (!(value = get_default (cd, key, ev)))
		return TC_null;

	tc = (CORBA_TypeCode)CORBA_Object_duplicate 
		((CORBA_Object)value->_type, NULL);
	CORBA_free (value);
	return tc;
}

static CORBA_any *
impl_Bonobo_PropertyBag_getValue (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);

	return get_value_with_default (cd, key, ev);
}

static void 
impl_Bonobo_PropertyBag_setValue (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  const CORBA_any        *value,
				  CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);

	if (cd->writeable && CLASS (cd)->set_value) {
		CLASS (cd)->set_value (cd, key, value, ev);
		
		if (!BONOBO_EX (ev))
			notify_listeners (cd, key, value);

		return;
	}

	bonobo_exception_set (ev, ex_Bonobo_PropertyBag_ReadOnly);
}

static Bonobo_PropertySet *
impl_Bonobo_PropertyBag_getValues (PortableServer_Servant  servant,
				   const CORBA_char       *filter,
				   CORBA_Environment      *ev)
{
	/* fixme: */

	bonobo_exception_set (ev, ex_Bonobo_NotImplemented);
	return NULL;
}

static void 
impl_Bonobo_PropertyBag_setValues (PortableServer_Servant    servant,
				   const Bonobo_PropertySet *set,
				   CORBA_Environment        *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);

	/* fixme: */

	if (cd->writeable)
		bonobo_exception_set (ev, ex_Bonobo_NotImplemented);

	bonobo_exception_set (ev, ex_Bonobo_PropertyBag_ReadOnly);
}

static CORBA_any *
impl_Bonobo_PropertyBag_getDefault (PortableServer_Servant  servant,
				    const CORBA_char       *key,
				    CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);

	return get_default (cd, key, ev);
}

static CORBA_char *
impl_Bonobo_PropertyBag_getDocTitle (PortableServer_Servant  servant,
				     const CORBA_char       *key,
				     CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);
	CORBA_any *value;
	char *dockey, *s;

	if (key [0] == '/')
		dockey = g_strconcat ("/doc/short", key, NULL);
	else
		dockey = g_strconcat ("/doc/short/", "/", key, NULL);

	value = get_value_with_default (cd, dockey, ev);

	g_free (dockey);

	if (BONOBO_EX (ev) || !value)
		return NULL;

	if (!bonobo_arg_type_is_equal (value->_type, TC_CORBA_string, NULL)) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_InvalidType);
		return NULL;
	}

	s = CORBA_string_dup (BONOBO_ARG_GET_STRING (value));

	CORBA_free (value);

	return s;
}

static CORBA_char *
impl_Bonobo_PropertyBag_getDoc (PortableServer_Servant  servant,
				const CORBA_char       *key,
				CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT(servant);
	CORBA_any *value;
	char *dockey, *s;

	if (key [0] == '/')
		dockey = g_strconcat ("/doc/long", key, NULL);
	else
		dockey = g_strconcat ("/doc/long", "/", key, NULL);

	value = get_value_with_default (cd, dockey, ev);

	g_free (dockey);

	if (BONOBO_EX (ev) || !value)
		return NULL;

	if (!bonobo_arg_type_is_equal (value->_type, TC_CORBA_string, NULL)) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_InvalidType);
		return NULL;
	}

	s = CORBA_string_dup (BONOBO_ARG_GET_STRING (value));

	CORBA_free (value);

	return s;
}

static Bonobo_PropertyFlags
impl_Bonobo_PropertyBag_getFlags (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);

	if (cd->writeable)
		return Bonobo_PROPERTY_WRITEABLE | Bonobo_PROPERTY_READABLE;

	return Bonobo_PROPERTY_READABLE;
}

static Bonobo_KeyList *
impl_Bonobo_ConfigDatabase_getDirs (PortableServer_Servant  servant,
				     const CORBA_char       *dir,
				     CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);
	CORBA_Environment nev;
	DataBaseInfo *info;
	GList *l;
	Bonobo_KeyList *cur_list, *def_list, *tmp_list;

	cur_list = NULL;

	if (CLASS (cd)->get_dirs)
		cur_list = CLASS (cd)->get_dirs (cd, dir, ev);
	
	if (BONOBO_EX (ev))
		return NULL;
	
	CORBA_exception_init (&nev);

	for (l = cd->priv->db_list; l != NULL; l = l->next) {
		info = (DataBaseInfo *)l->data;

		CORBA_exception_init (&nev);

		def_list = Bonobo_ConfigDatabase_getDirs (info->db, dir,&nev);
		if (!BONOBO_EX (&nev) && def_list) {

			if (!def_list->_length) {
				CORBA_free (def_list);
				continue;
			}

			if (!cur_list || !cur_list->_length) {
				CORBA_free (cur_list);
				cur_list = def_list;
				continue;
			}

			tmp_list = merge_keylists (cur_list, def_list);
			
			CORBA_free (cur_list);
			CORBA_free (def_list);

			cur_list = tmp_list;
		}
	}

	CORBA_exception_free (&nev);

	return cur_list;
}


static CORBA_boolean
impl_Bonobo_ConfigDatabase_hasDir (PortableServer_Servant  servant,
				   const CORBA_char       *dir,
				   CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);
	CORBA_Environment nev;
	DataBaseInfo *info;
	CORBA_boolean res;
	GList *l;

	if (CLASS (cd)->has_dir &&
	    CLASS (cd)->has_dir (cd, dir, ev))
		return TRUE;

	CORBA_exception_init (&nev);

	for (l = cd->priv->db_list; l != NULL; l = l->next) {
		info = (DataBaseInfo *)l->data;

		CORBA_exception_init (&nev);
		
		res = Bonobo_ConfigDatabase_hasDir (info->db, dir, &nev);
		
		CORBA_exception_free (&nev);
		
		if (!BONOBO_EX (&nev) && res)
			return TRUE;
	}

	CORBA_exception_free (&nev);

	return CORBA_FALSE;
}

static void 
impl_Bonobo_ConfigDatabase_removeValue (PortableServer_Servant  servant,
					const CORBA_char       *key,
					CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);

	if (cd->writeable && CLASS (cd)->remove_value) {
		CLASS (cd)->remove_value (cd, key, ev);
		return;
	}

	bonobo_exception_set (ev, ex_Bonobo_PropertyBag_ReadOnly);
}

static void 
impl_Bonobo_ConfigDatabase_removeDir (PortableServer_Servant  servant,
				      const CORBA_char       *dir,
				      CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);

	if (cd->writeable && CLASS (cd)->remove_dir) {
		CLASS (cd)->remove_dir (cd, dir, ev);
		return;
	}

	bonobo_exception_set (ev, ex_Bonobo_PropertyBag_ReadOnly);
}

static void 
impl_Bonobo_ConfigDatabase_addDatabase (PortableServer_Servant servant,
					const Bonobo_ConfigDatabase ddb,
					const CORBA_char *path,
					const Bonobo_ConfigDatabase_DBFlags fl,
					CORBA_Environment *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);
	DataBaseInfo *info;
	GList *l;

	/* we cant add ourselves */
	if (CORBA_Object_is_equivalent (BONOBO_OBJREF (cd), ddb, NULL))
		return;

	/* check if the database was already added */
	for (l = cd->priv->db_list; l != NULL; l = l->next) {
		info = (DataBaseInfo *)l->data;
		if (CORBA_Object_is_equivalent (info->db, ddb, NULL))
			return;
	}

	info = g_new0 (DataBaseInfo , 1);

	info->db = bonobo_object_dup_ref (ddb, ev);

	if (BONOBO_EX (ev)) {
		g_free (info);
		return;
	}
	
	info->path = g_strdup (path);

	cd->priv->db_list = g_list_append (cd->priv->db_list, info);
}

static void 
impl_Bonobo_ConfigDatabase_sync (PortableServer_Servant  servant, 
				 CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);

	if (CLASS (cd)->sync)
		CLASS (cd)->sync (cd, ev);
}

static CORBA_boolean
impl_Bonobo_ConfigDatabase__get_isWriteable (PortableServer_Servant  servant,
					     CORBA_Environment      *ev)
{
	BonoboConfigDatabase *cd = DATABASE_FROM_SERVANT (servant);

	return cd->writeable;
}

static void
bonobo_config_database_finalize (GObject *object)
{
	BonoboConfigDatabase *cd = BONOBO_CONFIG_DATABASE (object);
	DataBaseInfo *info;
	GList *l;

	if (!cd->priv)
		return;

	for (l = cd->priv->db_list; l != NULL; l = l->next) {
		info = (DataBaseInfo *)l->data;

		bonobo_object_release_unref (info->db, NULL);

		g_free (info->path);

		g_free (info);
	}
	
	if (cd->priv->db_list)
		g_list_free (cd->priv->db_list);	

	g_free (cd->priv);

	cd->priv = NULL;

	parent_class->finalize (object);
}

static void
bonobo_property_bag_server_class_init (BonoboPropertyBagServerClass *klass)
{
	POA_Bonobo_PropertyBag__epv *epv = &klass->epv;

	epv->getKeys       = impl_Bonobo_PropertyBag_getKeys;
	epv->getType       = impl_Bonobo_PropertyBag_getType;
	epv->getValue      = impl_Bonobo_PropertyBag_getValue;
	epv->setValue      = impl_Bonobo_PropertyBag_setValue;
	epv->getValues     = impl_Bonobo_PropertyBag_getValues;
	epv->setValues     = impl_Bonobo_PropertyBag_setValues;
	epv->getDefault    = impl_Bonobo_PropertyBag_getDefault;
	epv->getDocTitle   = impl_Bonobo_PropertyBag_getDocTitle;
	epv->getDoc        = impl_Bonobo_PropertyBag_getDoc;
	epv->getFlags      = impl_Bonobo_PropertyBag_getFlags;
}

static void
bonobo_property_bag_server_init (BonoboPropertyBagServer *pb)
{
	/* nothing to do */
}

BONOBO_TYPE_FUNC_FULL (BonoboPropertyBagServer, 
		       Bonobo_PropertyBag,
		       BONOBO_TYPE_OBJECT,
		       bonobo_property_bag_server);

static void
bonobo_config_database_class_init (BonoboConfigDatabaseClass *class)
{
	GObjectClass *object_class = (GObjectClass *) class;
	POA_Bonobo_ConfigDatabase__epv *epv;
		
	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = bonobo_config_database_finalize;

	epv = &class->epv;

	epv->getDirs        = impl_Bonobo_ConfigDatabase_getDirs;
	epv->hasDir         = impl_Bonobo_ConfigDatabase_hasDir;
	epv->removeValue    = impl_Bonobo_ConfigDatabase_removeValue;
	epv->removeDir      = impl_Bonobo_ConfigDatabase_removeDir;
	epv->addDatabase    = impl_Bonobo_ConfigDatabase_addDatabase;
	epv->sync           = impl_Bonobo_ConfigDatabase_sync;

	epv->_get_isWriteable = impl_Bonobo_ConfigDatabase__get_isWriteable;
}

static void
bonobo_config_database_init (BonoboConfigDatabase *cd)
{
	cd->priv = g_new0 (BonoboConfigDatabasePrivate, 1);
}

BonoboConfigDatabase *
bonobo_config_database_construct (BonoboConfigDatabase *cd,
				  BonoboEventSource    *opt_es)
{
	g_return_val_if_fail (cd != NULL, NULL);

	if (opt_es) {

		bonobo_object_ref (BONOBO_OBJECT(opt_es));
		cd->priv->es = opt_es;

	} else {

		if (!(cd->priv->es = bonobo_event_source_new ())) {
			bonobo_object_unref (BONOBO_OBJECT (cd));
			return NULL;
		}
	}

	bonobo_object_add_interface (BONOBO_OBJECT (cd), 
				     BONOBO_OBJECT (cd->priv->es));

	return cd;
}

BONOBO_TYPE_FUNC_FULL (BonoboConfigDatabase, 
		       Bonobo_ConfigDatabase,
		       bonobo_property_bag_server_get_type (),
		       bonobo_config_database);
