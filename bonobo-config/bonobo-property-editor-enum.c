/*
 * bonobo-property-editor-enum.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <ctype.h>
#include <bonobo.h>

#include "bonobo-property-editor.h"

typedef struct {
	GtkWidget *combo;
	GList     *values;
} Bonobo_PropertyEditor_Data_enum;

static Bonobo_PropertyEditor_Data_enum *
get_data (BonoboPropertyEditor *editor)
{
	return gtk_object_get_data (GTK_OBJECT (editor), 
				    "PROPERTY_EDITOR_DATA");
}

static void
destroy_cb (BonoboPropertyEditor *editor,
	    gpointer              user_data) 
{
	Bonobo_PropertyEditor_Data_enum *pd;
	GList *l;

	pd = get_data (editor);

	if (!pd || !pd->values)
		return;

	l = pd->values;

	while (l) {
		g_free (l->data);
		l = l->next;
	}

	if (pd->values)
		g_list_free (pd->values);

	g_free (pd);
}

static void
changed_cb (GtkEditable *editable,
	    gpointer     user_data) 
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (user_data);
	CORBA_Environment ev;
	DynamicAny_DynAny dyn;
	BonoboArg *arg;
	char *new_str;

	CORBA_exception_init (&ev);

	new_str = gtk_entry_get_text (GTK_ENTRY (editable));

	dyn = CORBA_ORB_create_dyn_enum (bonobo_orb (), editor->tc, &ev);

	DynamicAny_DynEnum_set_as_string (dyn, new_str, &ev);

	arg = DynamicAny_DynAny_to_any (dyn, &ev);

	CORBA_Object_release ((CORBA_Object) dyn, &ev);

	bonobo_property_editor_set_value (editor, arg, &ev);

	bonobo_arg_release (arg);

	CORBA_exception_free (&ev);
}

static void
set_value_cb (BonoboPropertyEditor *editor,
	      BonoboArg            *value,
	      CORBA_Environment    *ev)
{
	Bonobo_PropertyEditor_Data_enum *pd;
	DynamicAny_DynAny dyn;
	GtkCombo *combo;
	GtkEntry *entry;
	char *str, *old_str;
	int i;

	pd = get_data (editor);

	if (!pd || value->_type->kind != CORBA_tk_enum)
		return;

	combo = GTK_COMBO (pd->combo);
	entry = GTK_ENTRY (combo->entry);

	dyn = CORBA_ORB_create_dyn_any (bonobo_orb (), value, ev);

	if (!pd->values) {
		for (i = 0; i < value->_type->sub_parts; i++) {
			pd->values = g_list_append (pd->values, 
			        g_strdup (value->_type->subnames [i]));
		}
	
		gtk_combo_set_popdown_strings (combo, pd->values);
	}

	old_str = gtk_entry_get_text (entry);

	str = DynamicAny_DynEnum_get_as_string (dyn, ev);
	
	gtk_entry_set_editable (entry, TRUE);

	if (str && strcmp (old_str, str))
		gtk_entry_set_text (entry, str);

	CORBA_free (str);
		
	CORBA_Object_release ((CORBA_Object) dyn, ev);
}

BonoboObject *
bonobo_property_editor_enum_new ()
{
	GtkWidget *combo;
	BonoboPropertyEditor *ed;
	Bonobo_PropertyEditor_Data_enum *data;

	combo = gtk_combo_new ();
	gtk_widget_show (combo);
	gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);

	data = g_new0 (Bonobo_PropertyEditor_Data_enum, 1);
	data->combo = combo;

	ed = bonobo_property_editor_new (combo, set_value_cb, NULL);

	gtk_object_set_data (GTK_OBJECT (ed), "PROPERTY_EDITOR_DATA", data);

	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), "changed",
			    (GtkSignalFunc) changed_cb, ed);

	gtk_signal_connect (GTK_OBJECT (ed), "destroy",
			    (GtkSignalFunc) destroy_cb, NULL);

	return BONOBO_OBJECT (ed);
}
