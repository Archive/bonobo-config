/**
 * bonobo-property-bag-proxy.c: a proxy for property bags
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#include <config.h>
#include <glib-object.h>
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-arg.h>
#include <string.h>

#include "bonobo-property-bag-proxy.h"

static GObjectClass *parent_class = NULL;

#define BONOBO_PBPROXY_FROM_SERVANT(servant) \
(BONOBO_PBPROXY (bonobo_object (servant)))

struct _BonoboPBProxyPriv {
	GList          *pl;
	Bonobo_Listener listener;
};

typedef struct {
	char            *name;
	CORBA_any       *value;
	CORBA_any       *new_value;
} PropertyData;

enum {
	MODIFIED_SIGNAL,
	LAST_SIGNAL
};

static guint proxy_signals [LAST_SIGNAL];

static PropertyData *
lookup_property_data (BonoboPBProxy     *proxy,
		      const char        *name)
{
	PropertyData *pd;
	GList *l;

	for (l = proxy->priv->pl; l; l = l->next) {
		pd = (PropertyData *)l->data;
		
		if (!strcmp (pd->name, name))
			return pd;
	}

	return NULL;
}

static void
bonobo_pbproxy_free_plist (BonoboPBProxy *proxy)
{
	PropertyData  *pd;
	GList         *l;

	for (l = proxy->priv->pl; l; l = l->next) {
		pd = (PropertyData *)l->data;
			
		g_free (pd->name);

		CORBA_free (pd->new_value);

		CORBA_free (pd->value);
		
		g_free (pd);
	}

	g_list_free (proxy->priv->pl);

	proxy->priv->pl = NULL;
}

static void
remove_listener (BonoboPBProxyPriv *priv,
		 Bonobo_PropertyBag pb)
{
	if (pb != CORBA_OBJECT_NIL &&
	    priv->listener != CORBA_OBJECT_NIL) {
		CORBA_Environment ev;

		CORBA_exception_init (&ev);

		bonobo_event_source_client_remove_listener (
		        pb, priv->listener, &ev);

		CORBA_Object_release (priv->listener, &ev);

		CORBA_exception_free (&ev);
	}

	priv->listener = CORBA_OBJECT_NIL;
}

static void
bonobo_pbproxy_finalize (GObject *object)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY (object);

	if (!proxy->priv)
		return;

	remove_listener (proxy->priv, proxy->bag);

	bonobo_pbproxy_free_plist (proxy);

	if (proxy->bag != CORBA_OBJECT_NIL)
		bonobo_object_release_unref (proxy->bag, NULL);

	proxy->bag = CORBA_OBJECT_NIL;

	g_free (proxy->priv);

	proxy->priv = NULL;

	parent_class->finalize (object);
}


/* CORBA implementations */

static Bonobo_KeyList *
impl_Bonobo_PropertyBag_getKeys (PortableServer_Servant  servant,
				 const CORBA_char       *filter,
				 CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);
	Bonobo_KeyList *klist;
	GList *l;
	int len;

	klist = Bonobo_KeyList__alloc ();
	klist->_length = 0;

	if (!(len = g_list_length (proxy->priv->pl)))
		return klist;

	klist->_buffer = CORBA_sequence_CORBA_string_allocbuf (len);
	CORBA_sequence_set_release (klist, TRUE); 

	for (l = proxy->priv->pl; l; l = l->next)
		klist->_buffer [klist->_length++] = 
			CORBA_string_dup (((PropertyData *)l->data)->name);
	

	return klist;
}

static CORBA_TypeCode
impl_Bonobo_PropertyBag_getType (PortableServer_Servant  servant,
				 const CORBA_char       *key,
				 CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);
	PropertyData  *pd;
	CORBA_any     *v;

	if (!(pd = lookup_property_data (proxy, key))) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	if (pd->new_value)
		v = pd->new_value;
	else
		v = pd->value;

	if (!v)
		return TC_null;

	return (CORBA_TypeCode) CORBA_Object_duplicate (
	        (CORBA_Object)v->_type, ev);
}

static CORBA_any *
impl_Bonobo_PropertyBag_getValue (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);
	PropertyData  *pd;

	if (!(pd = lookup_property_data (proxy, key))) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	if (pd->new_value)
		return bonobo_arg_copy (pd->new_value);

	if (pd->value)
		return bonobo_arg_copy (pd->value);

	return bonobo_arg_new (TC_null);
}

static void 
impl_Bonobo_PropertyBag_setValue (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  const CORBA_any        *value,
				  CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);
	PropertyData  *pd;

	if (!(pd = lookup_property_data (proxy, key))) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return;
	}

	if (!pd->new_value && pd->value && 
	    bonobo_arg_is_equal (pd->value, value, NULL))
		return;

	if (pd->new_value && bonobo_arg_is_equal (pd->new_value, value, NULL))
		return;

	CORBA_free (pd->new_value);

	pd->new_value = bonobo_arg_copy (value);

	g_signal_emit (G_OBJECT (proxy), proxy_signals [MODIFIED_SIGNAL], 0);

	bonobo_event_source_notify_listeners_full (proxy->es,
						   "Bonobo/Property",
						   "change", key,
						   value, ev);

	bonobo_event_source_notify_listeners_full (proxy->es,
						   "Bonobo/PropertyProxy",
						   "change", key,
						   value, ev);
}


static Bonobo_PropertySet *
impl_Bonobo_PropertyBag_getValues (PortableServer_Servant servant,
				   const CORBA_char       *filter,
				   CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);
	Bonobo_PropertySet *set;
	GList		   *l;
	int		    len;
	int		    i;

	len = g_list_length (proxy->priv->pl);

	set = Bonobo_PropertySet__alloc ();
	set->_length = len;

	if (len == 0)
		return set;

	set->_buffer = CORBA_sequence_Bonobo_Pair_allocbuf (len);

	i = 0;
	for (l = proxy->priv->pl; l != NULL; l = l->next) {
		PropertyData *pd = (PropertyData *)l->data;
		BonoboArg *arg;

		set->_buffer [i].name = CORBA_string_dup (pd->name);

		if (pd->new_value)
			arg = bonobo_arg_copy (pd->new_value);
		else if (pd->value)
			arg = bonobo_arg_copy (pd->value);
		else 
			arg = bonobo_arg_new (TC_null);

		set->_buffer [i].value = *arg;

		i++;		
	}

	return set;
}

static void                  
impl_Bonobo_PropertyBag_setValues (PortableServer_Servant servant,
				   const Bonobo_PropertySet *set,
				   CORBA_Environment *ev)
{
	int i;

	for (i = 0; i < set->_length; i++) {
		impl_Bonobo_PropertyBag_setValue (servant, 
						  set->_buffer [i].name,
						  &set->_buffer [i].value, 
						  ev);
		if (BONOBO_EX (ev))
			return;
	}
}

static CORBA_any *
impl_Bonobo_PropertyBag_getDefault (PortableServer_Servant  servant,
				    const CORBA_char       *key,
				    CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);

	return Bonobo_PropertyBag_getDefault (proxy->bag, key, ev);
}

static CORBA_char *
impl_Bonobo_PropertyBag_getDocTitle (PortableServer_Servant  servant,
				     const CORBA_char       *key,
				     CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);

	return Bonobo_PropertyBag_getDocTitle (proxy->bag, key, ev);
}

static CORBA_char *
impl_Bonobo_PropertyBag_getDoc (PortableServer_Servant  servant,
				const CORBA_char       *key,
				CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);

	return Bonobo_PropertyBag_getDoc (proxy->bag, key, ev);
}

static Bonobo_PropertyFlags
impl_Bonobo_PropertyBag_getFlags (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  CORBA_Environment      *ev)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY_FROM_SERVANT (servant);

	return Bonobo_PropertyBag_getFlags (proxy->bag, key, ev);
}

static void
value_changed_cb (BonoboListener    *listener,
		  char              *event_name, 
		  CORBA_any         *any,
		  CORBA_Environment *ev,
		  gpointer           user_data)
{
	BonoboPBProxy *proxy = BONOBO_PBPROXY (user_data);
	PropertyData *pd;
	char *name;

	if (!(name = bonobo_event_subtype (event_name)))
		return;

	if (!(pd = lookup_property_data (proxy, name)))
		return;

	CORBA_free (pd->new_value);
	pd->new_value = NULL;
	
	CORBA_free (pd->value);
	pd->value = bonobo_arg_copy (any);

	bonobo_event_source_notify_listeners_full (proxy->es, 
	        "Bonobo/Property", "change", name, any, ev);
	
	g_free (name);
}

void
bonobo_pbproxy_set_bag (BonoboPBProxy     *proxy,
			Bonobo_PropertyBag bag)
{
	Bonobo_KeyList     *klist;
	CORBA_Environment   ev;
	PropertyData       *pd;
	int                 i;

	g_return_if_fail (proxy != NULL);

	remove_listener (proxy->priv, proxy->bag);

	bonobo_pbproxy_free_plist (proxy);
	
	/* fixme: we must unref the bag, but it seems there is a ref counting
	   error somewhere - so I just disabled it */
	/*
	  if (proxy->bag != CORBA_OBJECT_NIL)
	  bonobo_object_release_unref (proxy->bag, NULL);
	*/

	proxy->bag = CORBA_OBJECT_NIL;

	CORBA_exception_init (&ev);

	if (bag != CORBA_OBJECT_NIL) {

		proxy->bag = bonobo_object_dup_ref (bag, NULL);
	
		proxy->priv->listener = bonobo_event_source_client_add_listener_full (
			bag, g_cclosure_new (G_CALLBACK (value_changed_cb), proxy, NULL),
			"Bonobo/Property:change:", NULL);

		klist = Bonobo_PropertyBag_getKeys (bag, "", &ev);
      	
		if (BONOBO_EX (&ev) || !klist) {
			CORBA_exception_free (&ev);
			return;
		}
      	
		for (i = 0; i < klist->_length; i++) {
			CORBA_any *value;

			CORBA_exception_init (&ev);

			value = Bonobo_PropertyBag_getValue (bag, 
				klist->_buffer [i], &ev);

			if (BONOBO_EX (&ev))
				continue;

			pd = g_new0 (PropertyData, 1);

			pd->name = g_strdup (klist->_buffer [i]);
			pd->value = value;
			proxy->priv->pl = g_list_prepend (proxy->priv->pl, pd);
		}

		CORBA_free (klist);
	}

	CORBA_exception_free (&ev);
}

BonoboPBProxy *
bonobo_pbproxy_construct (BonoboPBProxy *proxy)
{
	return proxy;
}

BonoboPBProxy *
bonobo_pbproxy_new ()
{
	BonoboPBProxy      *proxy;

	proxy = g_object_new (BONOBO_TYPE_PBPROXY, NULL);

	if (!(proxy->es = bonobo_event_source_new ())) {
		bonobo_object_unref (BONOBO_OBJECT (proxy));
		return NULL;
	}

	bonobo_object_add_interface (BONOBO_OBJECT (proxy), 
				     BONOBO_OBJECT (proxy->es));
	
	return bonobo_pbproxy_construct (proxy);
}

static void
bonobo_pbproxy_class_init (BonoboPBProxyClass *class)
{
	GObjectClass *object_class = (GObjectClass *) class;
	POA_Bonobo_PropertyBag__epv *epv =  &class->epv;
	
	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = bonobo_pbproxy_finalize;

	proxy_signals [MODIFIED_SIGNAL] = 
		g_signal_new ("modified",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (BonoboPBProxyClass, modified),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);
                       
	epv->getKeys       = impl_Bonobo_PropertyBag_getKeys;
	epv->getType       = impl_Bonobo_PropertyBag_getType;
	epv->getValue      = impl_Bonobo_PropertyBag_getValue;
	epv->setValue      = impl_Bonobo_PropertyBag_setValue;
	epv->getValues     = impl_Bonobo_PropertyBag_getValues;
	epv->setValues     = impl_Bonobo_PropertyBag_setValues;
	epv->getDefault    = impl_Bonobo_PropertyBag_getDefault;
	epv->getDocTitle   = impl_Bonobo_PropertyBag_getDocTitle;
	epv->getDoc        = impl_Bonobo_PropertyBag_getDoc;
	epv->getFlags      = impl_Bonobo_PropertyBag_getFlags;
}

static void
bonobo_pbproxy_init (BonoboPBProxy *proxy)
{
	proxy->priv = g_new0 (BonoboPBProxyPriv, 1);
}

BONOBO_TYPE_FUNC_FULL (BonoboPBProxy, 
		       Bonobo_PropertyBag,
		       BONOBO_TYPE_OBJECT,
		       bonobo_pbproxy);




CORBA_any *
bonobo_pbproxy_get_default (BonoboPBProxy     *proxy,
			    const char        *name,
			    CORBA_Environment *ev)
{
	return Bonobo_PropertyBag_getDefault (proxy->bag, name, ev);
}

char *
bonobo_pbproxy_get_doc (BonoboPBProxy     *proxy,
			       const char        *name,
			       CORBA_Environment *ev)
{
	return Bonobo_PropertyBag_getDoc (proxy->bag, name, ev);
}

CORBA_long        
bonobo_pbproxy_get_flags (BonoboPBProxy     *proxy,
			  const char        *name,
			  CORBA_Environment *ev)
{

	return Bonobo_PropertyBag_getFlags (proxy->bag, name, ev);
}

void
bonobo_pbproxy_update (BonoboPBProxy *proxy)
{
	GList *l;
	PropertyData *pd;
	CORBA_Environment ev;
	
	for (l = proxy->priv->pl; l; l = l->next) {
		pd = (PropertyData *)l->data;

		CORBA_exception_init (&ev);
		
		if (pd->new_value)
			Bonobo_PropertyBag_setValue (proxy->bag, pd->name, 
						     pd->new_value, &ev);
	}

	CORBA_exception_free (&ev);
}

void
bonobo_pbproxy_revert (BonoboPBProxy *proxy)
{
	/* fixme: */
	g_warning ("bonobo_pbproxy_revert not implemented");
}

