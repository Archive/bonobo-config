/**
 * bonobo-property-editor-struct.h:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef _BONOBO_PROPERTY_EDITOR_STRUCT_H_
#define _BONOBO_PROPERTY_EDITOR_STRUCT_H_

#include "bonobo-property-editor.h"

G_BEGIN_DECLS

#define BONOBO_TYPE_PE_STRUCT        (bonobo_pe_struct_get_type ())
#define BONOBO_PE_STRUCT_TYPE        BONOBO_TYPE_PE_STRUCT // deprecated, you should use BONOBO_TYPE_PE_STRUCT
#define BONOBO_PE_STRUCT(o)          (GTK_CHECK_CAST ((o), BONOBO_TYPE_PE_STRUCT, BonoboPEStruct))
#define BONOBO_PE_STRUCT_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_TYPE_PE_STRUCT, BonoboPEStructClass))
#define BONOBO_IS_PE_STRUCT(o)       (GTK_CHECK_TYPE ((o), BONOBO_TYPE_PE_STRUCT))
#define BONOBO_IS_PE_STRUCT_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_TYPE_PE_STRUCT))

typedef struct _BonoboPEStructPrivate BonoboPEStructPrivate;

typedef struct {
	BonoboPropertyEditor   base;
	BonoboPEStructPrivate *priv;
} BonoboPEStruct;


typedef struct {
	BonoboPropertyEditorClass parent_class;
} BonoboPEStructClass;

GtkType            
bonobo_pe_struct_get_type          (void);

BonoboObject *
bonobo_property_editor_struct_new  ();

G_END_DECLS

#endif /* _BONOBO_PROPERTY_EDITOR_STRUCT_H_ */
