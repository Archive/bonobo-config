/**
 * bonobo-property-bag-proxy.h: a proxy for property bags
 *
 * Author:
 *   Dietmar Maurer  (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef __BONOBO_PBPROXY_H__
#define __BONOBO_PBPROXY_H__

#include <bonobo/bonobo-object.h>
#include <bonobo/bonobo-event-source.h>

G_BEGIN_DECLS

#define BONOBO_TYPE_PBPROXY        (bonobo_pbproxy_get_type ())
#define BONOBO_PBPROXY_TYPE        BONOBO_TYPE_PBPROXY // deprecated, you should use BONOBO_TYPE_PBPROXY
#define BONOBO_PBPROXY(o)	   (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_PBPROXY, BonoboPBProxy))
#define BONOBO_PBPROXY_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_PBPROXY, BonoboPBProxyClass))
#define BONOBO_IS_PBPROXY(o)	   (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_PBPROXY))
#define BONOBO_IS_PBPROXY_CLASS(k) (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_PBPROXY))

typedef struct _BonoboPBProxy        BonoboPBProxy;
typedef struct _BonoboPBProxyPriv    BonoboPBProxyPriv;

struct _BonoboPBProxy {
	BonoboObject       base;

	/* read only values */
	BonoboEventSource  *es;
	Bonobo_PropertyBag  bag; 

	BonoboPBProxyPriv  *priv;
};

typedef struct {
	BonoboObjectClass  parent_class;

	POA_Bonobo_PropertyBag__epv epv;

        void (*modified)                       (BonoboPBProxy *proxy);

} BonoboPBProxyClass;


GType		  bonobo_pbproxy_get_type  (void);

BonoboPBProxy    *bonobo_pbproxy_construct (BonoboPBProxy *proxy);

BonoboPBProxy	 *bonobo_pbproxy_new	   ();

void              bonobo_pbproxy_set_bag   (BonoboPBProxy     *proxy,
					    Bonobo_PropertyBag bag);

void              bonobo_pbproxy_update    (BonoboPBProxy *proxy);
void              bonobo_pbproxy_revert    (BonoboPBProxy *proxy);


G_END_DECLS

#endif /* ! __BONOBO_PBPROXY_H__ */
