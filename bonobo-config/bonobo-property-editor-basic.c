/*
 * bonobo-property-editor-basic.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <ctype.h>
#include <bonobo.h>

#include "bonobo-property-editor.h"

enum {
	ARG_HEX
};

typedef struct {
	GtkWidget      *entry;
} Bonobo_PropertyEditor_Data_basic;

static gboolean
check_type (CORBA_TypeCode tc1, CORBA_TypeCode tc2)
{

	return bonobo_arg_type_is_equal (tc1, tc2, NULL);
}

static Bonobo_PropertyEditor_Data_basic *
get_data (BonoboPropertyEditor *editor)
{
	return gtk_object_get_data (GTK_OBJECT (editor), 
				    "PROPERTY_EDITOR_DATA");
}

static void
destroy_cb (BonoboPropertyEditor *editor,
	    gpointer              user_data) 
{
	Bonobo_PropertyEditor_Data_basic *pd;

	pd = get_data (editor);

	if (pd)
		g_free (pd);
}

static void        
changed_cb (GtkEntry *entry,
	    gpointer user_data)
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (user_data);
	Bonobo_PropertyEditor_Data_basic *pd;
	CORBA_Environment ev;
	DynamicAny_DynAny dyn = NULL;
	CORBA_TypeCode tc;
	BonoboArg *arg;
	CORBA_ORB orb;
	char *text;

	if (!(pd = get_data (editor)))
		return;

	if (!editor->tc)
		return;

	CORBA_exception_init (&ev);

	orb = bonobo_orb ();

	text = gtk_entry_get_text (entry);

	if (editor->tc->kind == CORBA_tk_alias)
		tc = editor->tc->subtypes [0];
	else
		tc = editor->tc;

	if (check_type (tc, TC_ushort)) {
		CORBA_unsigned_short v;

		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_ushort, &ev);
		v = strtoul (text, NULL, 0);
		DynamicAny_DynAny_insert_ushort (dyn, v, &ev);

	} else if (check_type (tc, TC_short)) {
		CORBA_short v;
		
		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_short, &ev);
		v = strtol (text, NULL, 0);
		DynamicAny_DynAny_insert_short (dyn, v, &ev);
	
	} else if (check_type (tc, TC_ulong)) {
		CORBA_unsigned_long v;

		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_ulong, &ev);
		v = strtoul (text, NULL, 0);
		DynamicAny_DynAny_insert_ulong (dyn, v, &ev);

	} else if (check_type (tc, TC_long)) {
		CORBA_long v;

		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_long, &ev);
		v = strtol (text, NULL, 0);
		DynamicAny_DynAny_insert_long (dyn, v, &ev);
	
	} else if (check_type (tc, TC_float)) {
		CORBA_float v;
		
		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_float, &ev);
		v = strtod (text, NULL);
		DynamicAny_DynAny_insert_float (dyn, v, &ev);

	} else if (check_type (tc, TC_double)) {
		CORBA_float v;
		
		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_double, &ev);
		v = strtod (text, NULL);
		DynamicAny_DynAny_insert_double (dyn, v, &ev);

	} else if (check_type (tc, TC_CORBA_string)) {

		dyn = CORBA_ORB_create_basic_dyn_any (orb, TC_string, &ev);
		DynamicAny_DynAny_insert_string (dyn, text, &ev);
	}

	if (BONOBO_EX (&ev) || dyn == NULL)
		return;

	arg = DynamicAny_DynAny_to_any (dyn, &ev);

	bonobo_property_editor_set_value (editor, arg, &ev);

	bonobo_arg_release (arg);

	CORBA_Object_release ((CORBA_Object) dyn, &ev);

	CORBA_exception_free (&ev);
}

static void
set_value_cb (BonoboPropertyEditor *editor,
	      BonoboArg            *value,
	      CORBA_Environment    *ev)
{
	Bonobo_PropertyEditor_Data_basic *pd;
	DynamicAny_DynAny dyn;
	char *text;

	pd = get_data (editor);

	if (!pd)
		return;

	dyn = CORBA_ORB_create_dyn_any (bonobo_orb (), value, ev);

	if (BONOBO_EX (ev) || dyn == NULL)
		return;

	if (check_type (value->_type, TC_ushort)) {
		CORBA_unsigned_short v;
		
		v = DynamicAny_DynAny_get_ushort (dyn, ev); 
		text =  g_strdup_printf ("%u", v);
	
	} else if (check_type (value->_type, TC_short)) {
		CORBA_short v;
		
		v = DynamicAny_DynAny_get_short (dyn, ev); 
		text =  g_strdup_printf ("%d", v);

	} else if (check_type (value->_type, TC_ulong)) {
		CORBA_unsigned_long v;
		
		v = DynamicAny_DynAny_get_ulong (dyn, ev); 
		text =  g_strdup_printf ("%u", v);
	
	} else if (check_type (value->_type, TC_long)) {
		CORBA_long v;
		
		v = DynamicAny_DynAny_get_long (dyn, ev); 
		text =  g_strdup_printf ("%d", v);

	} else if (check_type (value->_type, TC_float)) {
		CORBA_float v;
		
		v = DynamicAny_DynAny_get_float (dyn, ev); 
		text =  g_strdup_printf ("%f", v);

	} else if (check_type (value->_type, TC_double)) {
		CORBA_double v;
		
		v = DynamicAny_DynAny_get_double (dyn, ev); 
		text =  g_strdup_printf ("%g", v);

	} else if (check_type (value->_type, TC_string)) {
		CORBA_char *v;

		v = DynamicAny_DynAny_get_string (dyn, ev); 
		text =  g_strdup (v);
		CORBA_free (v);

	} else {
		text = g_strdup ("(unknown type code)");
	}


	CORBA_Object_release ((CORBA_Object) dyn, ev);

	gtk_signal_handler_block_by_func (GTK_OBJECT (pd->entry), changed_cb, 
					  editor);

	if (strcmp (gtk_entry_get_text (GTK_ENTRY (pd->entry)), text)) {
		gtk_entry_set_editable (GTK_ENTRY (pd->entry), TRUE);
		gtk_entry_set_text (GTK_ENTRY (pd->entry), text);
	}

	gtk_signal_handler_unblock_by_func (GTK_OBJECT (pd->entry), 
					    changed_cb, editor);

	g_free (text);
}

static gboolean
is_supported (CORBA_TypeCode tc)
{
	if (check_type (tc, TC_short))
		return TRUE;
	if (check_type (tc, TC_ushort))
		return TRUE;
	if (check_type (tc, TC_long))
		return TRUE;
	if (check_type (tc, TC_ulong))
		return TRUE;
	if (check_type (tc, TC_float))
		return TRUE;
	if (check_type (tc, TC_double))
		return TRUE;
	if (check_type (tc, TC_string))
		return TRUE;

	return FALSE;
}

static void
get_prop (BonoboPropertyBag *bag, 
	  BonoboArg         *arg, 
	  guint              arg_id, 
	  CORBA_Environment *ev, 
	  gpointer           data)
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (data);
	Bonobo_PropertyEditor_Data_basic *pd;
	
	if (!(pd = get_data (editor)))
		return;

	switch (arg_id) {
	case ARG_HEX:
		BONOBO_ARG_SET_BOOLEAN (arg, FALSE);
		break;
	default:
		break;
	}
}

static void
set_prop (BonoboPropertyBag *bag, 
	  const BonoboArg   *arg, 
	  guint              arg_id, 
	  CORBA_Environment *ev, 
	  gpointer           data)
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (data);
	Bonobo_PropertyEditor_Data_basic *pd;

	if (!(pd = get_data (editor)))
		return;

	switch (arg_id) {
	case ARG_HEX:
		break;
	default:
		break;
	}
}

BonoboObject *
bonobo_property_editor_basic_new (CORBA_TypeCode tc)
{
	BonoboPropertyEditor *ed;
	Bonobo_PropertyEditor_Data_basic *data;
	BonoboPropertyBag *pbag;
	
	if (!is_supported (tc))
		return NULL;

	data = g_new0 (Bonobo_PropertyEditor_Data_basic, 1);
	data->entry = gtk_entry_new ();
	gtk_widget_show (data->entry);

	ed = bonobo_property_editor_new (data->entry, set_value_cb, tc);

	gtk_object_set_data (GTK_OBJECT (ed), "PROPERTY_EDITOR_DATA", data);

	gtk_signal_connect (GTK_OBJECT (data->entry), "changed",
			    (GtkSignalFunc) changed_cb, ed);

	gtk_signal_connect (GTK_OBJECT (ed), "destroy",
			    (GtkSignalFunc) destroy_cb, NULL);

	pbag = bonobo_property_bag_new (get_prop, set_prop, ed);
	bonobo_control_set_properties (BONOBO_CONTROL (ed), pbag);

	bonobo_object_add_interface (BONOBO_OBJECT (ed), 
				     BONOBO_OBJECT (pbag));

	return BONOBO_OBJECT (ed);
}
