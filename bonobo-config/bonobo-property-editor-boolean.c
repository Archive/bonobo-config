/*
 * bonobo-property-editor-boolean.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <ctype.h>
#include <bonobo.h>

#include "bonobo-property-editor.h"

static void
changed_cb (GtkEditable *editable,
	    gpointer     user_data) 
{
	BonoboPropertyEditor *editor = BONOBO_PROPERTY_EDITOR (user_data);
	CORBA_Environment ev;
	BonoboArg *arg;
	char *new_str;

	CORBA_exception_init (&ev);
	
	new_str = gtk_entry_get_text (GTK_ENTRY (editable));

	arg = bonobo_arg_new (TC_boolean);

	if (!strcmp ("TRUE", new_str))
		BONOBO_ARG_SET_BOOLEAN (arg, TRUE);
	else 
		BONOBO_ARG_SET_BOOLEAN (arg, FALSE);

	bonobo_property_editor_set_value (editor, arg, &ev);

	bonobo_arg_release (arg);

	CORBA_exception_free (&ev);
}

static void
set_value_cb (BonoboPropertyEditor *editor,
	      BonoboArg            *value,
	      CORBA_Environment    *ev)
{
	GtkWidget *widget;
	GtkCombo *combo;
	GtkEntry *entry;
	CORBA_boolean new_value, old_value;

	if (!bonobo_arg_type_is_equal (value->_type, TC_boolean, NULL))
		return;

	widget = bonobo_control_get_widget (BONOBO_CONTROL (editor));
	combo = GTK_COMBO (widget);
	entry = GTK_ENTRY (combo->entry);

	if (!strcmp (gtk_entry_get_text (entry), "TRUE"))
		old_value = TRUE;
	else
		old_value = FALSE;

	new_value = BONOBO_ARG_GET_BOOLEAN (value);

	gtk_signal_handler_block_by_func (GTK_OBJECT (entry), changed_cb, 
					  editor);

	if (new_value != old_value)
		gtk_entry_set_text (entry, new_value ? "TRUE" : "FALSE");

	gtk_signal_handler_unblock_by_func (GTK_OBJECT (entry), changed_cb, 
					    editor);
	
}

BonoboObject *
bonobo_property_editor_boolean_new ()
{
	GtkWidget *combo;
	BonoboPropertyEditor *ed;
	GList *l;
	BonoboPropertyBag *pbag;

	combo = gtk_combo_new ();

	l = g_list_append (NULL, "TRUE");
	l = g_list_append (l, "FALSE");

	gtk_combo_set_popdown_strings (GTK_COMBO (combo), l);
	g_list_free (l);
	gtk_widget_show (combo);

	ed = bonobo_property_editor_new (combo, set_value_cb, TC_boolean);

	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), "changed",
			    (GtkSignalFunc) changed_cb, ed);

	pbag = bonobo_property_bag_new (NULL, NULL, ed);
	bonobo_control_set_properties (BONOBO_CONTROL (ed), pbag);

	bonobo_object_add_interface (BONOBO_OBJECT (ed), 
				     BONOBO_OBJECT (pbag));

	return BONOBO_OBJECT (ed);
}
