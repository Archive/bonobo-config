/*
 * bonobo-config-utils.c: Utility functions
 *
 * Author:
 *   Michael Meeks  (michael@ximian.com)
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <config.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-arg.h>

#include "bonobo-config-utils.h"

char *
xml_node_to_string (xmlNode  *node)
{
	xmlDoc     *doc;
	xmlNode    *root;
	xmlChar    *mem = NULL;
	int         size;
	char       *retval;

	doc = xmlNewDoc ("1.0");
	root = xmlCopyNode (node, TRUE);
	xmlDocSetRootElement (doc, root);

	xmlDocDumpMemory (doc, &mem, &size);

	xmlFreeDoc (doc);

	retval = g_strdup (mem);

	xmlFree (mem);

	return retval;
}


static void
xml_encode_type (xmlNode           *type_parent,
	     CORBA_TypeCode     tc,
	     CORBA_Environment *ev);

static void
encode_subtypes (xmlNode           *parent,
		 CORBA_TypeCode     tc,
		 int                num_subtypes,
		 CORBA_Environment *ev)
{
	xmlNode *subtypes;
	int      i;

	subtypes = xmlNewChild (parent, NULL, "subtypes", NULL);

	for (i = 0; i < num_subtypes; i++)
		xml_encode_type (subtypes, tc->subtypes [i], ev);
}

static void
xml_encode_type (xmlNode           *parent,
		 CORBA_TypeCode     tc,
		 CORBA_Environment *ev)
{
	xmlNode *node;
	char scratch [128];
	int  i;

	node = xmlNewChild (parent, NULL, "type", NULL);

	if (tc->name)
                xmlSetProp (node, "name", tc->name);
	
	if (tc->repo_id)
                xmlSetProp (node, "repo_id", tc->repo_id);

	snprintf (scratch, 127, "%d", tc->kind);
	xmlSetProp (node, "tckind", scratch);

	if (tc->length) {
		snprintf (scratch, 127, "%u", tc->length);
		xmlSetProp (node, "length", scratch);
	}

	if (tc->sub_parts) {
		snprintf (scratch, 127, "%u", tc->sub_parts);
		xmlSetProp (node, "sub_parts", scratch);
	}

	switch (tc->kind) {
 	case CORBA_tk_struct:
	case CORBA_tk_union:
	case CORBA_tk_enum:
	case CORBA_tk_except: { /* subnames */
		xmlNode *subnames;

		subnames = xmlNewChild (node, NULL, "subnames", NULL);

		for (i = 0; i < tc->sub_parts; i++)
			xmlNewChild (subnames, NULL, "name", tc->subnames [i]);
	}
	if (tc->kind != CORBA_tk_enum)
		encode_subtypes (node, tc, tc->sub_parts, ev);
	break;

	case CORBA_tk_alias:
	case CORBA_tk_array:
	case CORBA_tk_sequence:
		encode_subtypes (node, tc, 1, ev);
		break;

	default:
		break;
	}
}

#define DO_ENCODE(tckind,format,corbatype,value,align)			\
	case tckind:							\
		*value = ALIGN_ADDRESS (*value, align);			\
		snprintf (scratch, 127, format,				\
			  * (corbatype *) *value);			\
		*value = ((guchar *)*value) + sizeof (corbatype);	\
		break;

static void
xml_encode_value (xmlNode           *parent,
		  CORBA_TypeCode     tc,
		  gpointer          *value,
		  CORBA_Environment *ev)
{
	xmlNode *node;
	char     scratch [256] = "";
	int      i;

	node = xmlNewChild (parent, NULL, "value", NULL);

	switch (tc->kind) {
	case CORBA_tk_null:
	case CORBA_tk_void:
		break;


		DO_ENCODE (CORBA_tk_short, "%d", CORBA_short, value, 
			   ORBIT_ALIGNOF_CORBA_SHORT);
		DO_ENCODE (CORBA_tk_ushort, "%u", CORBA_unsigned_short, value,
			   ORBIT_ALIGNOF_CORBA_SHORT);
		DO_ENCODE (CORBA_tk_long, "%d", CORBA_long, value, 
			   ORBIT_ALIGNOF_CORBA_LONG);
		DO_ENCODE (CORBA_tk_enum, "%d", CORBA_long, value, 
			   ORBIT_ALIGNOF_CORBA_LONG);
		DO_ENCODE (CORBA_tk_ulong, "%u", CORBA_unsigned_long, value, 
			   ORBIT_ALIGNOF_CORBA_LONG);
		DO_ENCODE (CORBA_tk_float, "%g", CORBA_float, value, 
			   ORBIT_ALIGNOF_CORBA_FLOAT);
		DO_ENCODE (CORBA_tk_double, "%g", CORBA_double, value, 
			   ORBIT_ALIGNOF_CORBA_DOUBLE);
		DO_ENCODE (CORBA_tk_boolean, "%d", CORBA_boolean, value, 1);
		DO_ENCODE (CORBA_tk_char, "%d", CORBA_char, value,  1);
		DO_ENCODE (CORBA_tk_octet, "%d", CORBA_octet, value, 1);
		DO_ENCODE (CORBA_tk_wchar, "%d", CORBA_wchar, value, 
			   ORBIT_ALIGNOF_CORBA_SHORT);

	case CORBA_tk_string:
	case CORBA_tk_wstring:
		*value = ALIGN_ADDRESS(*value, ORBIT_ALIGNOF_CORBA_POINTER);
		xmlNodeSetContent (node, *(CORBA_char **) *value);
		*value = ((guchar *)*value) + sizeof (CORBA_char *);
		break;

	case CORBA_tk_objref:
		g_warning ("Cannot serialize an objref");
		break;

	case CORBA_tk_TypeCode:
		*value = ALIGN_ADDRESS (*value, ORBIT_ALIGNOF_CORBA_POINTER);
		xml_encode_type (node, * (CORBA_TypeCode *) *value, ev);
		*value = ((guchar *)*value) + sizeof(CORBA_TypeCode);
		break;

	case CORBA_tk_any:
		*value = ALIGN_ADDRESS (*value,
					MAX (ORBIT_ALIGNOF_CORBA_LONG,
					     MAX (ORBIT_ALIGNOF_CORBA_POINTER, 
						  ORBIT_ALIGNOF_CORBA_STRUCT)));
		bonobo_property_bag_xml_encode_any (node, (CORBA_any *) *value,
						    ev);
		*value = ((guchar *)*value) + sizeof (CORBA_any);
		break;

	case CORBA_tk_sequence: {
		CORBA_sequence_CORBA_octet *sval = *value;
		gpointer subval;

		*value = ALIGN_ADDRESS (*value,
					MAX (MAX (ORBIT_ALIGNOF_CORBA_LONG, 
						  ORBIT_ALIGNOF_CORBA_STRUCT), 
					     ORBIT_ALIGNOF_CORBA_POINTER));

		snprintf (scratch, 127, "%d", sval->_length);
		xmlSetProp (node, "length", scratch);

		subval = sval->_buffer;

		for (i = 0; i < sval->_length; i++)
			xml_encode_value (node, tc->subtypes [0], &subval, 
					  ev);
	    
		*value = ((guchar *)*value) + sizeof (CORBA_sequence_CORBA_octet);
		scratch [0] = '\0';
		break;
	}

	case CORBA_tk_array:
		for (i = 0; i < tc->length; i++)
			xml_encode_value (node, tc->subtypes [0], value, ev);
		break;

 	case CORBA_tk_struct:
	case CORBA_tk_except:
		for (i = 0; i < tc->sub_parts; i++)
			xml_encode_value (node, tc->subtypes [i], value, ev);
		break;

	case CORBA_tk_alias:
		xml_encode_value (node, tc->subtypes [0], value, ev);
		break;

	case CORBA_tk_union:
	case CORBA_tk_Principal:
	case CORBA_tk_fixed:
	case CORBA_tk_recursive:
	default:
		g_warning ("Unhandled kind '%d'", tc->kind);
		break;
	}

	if (scratch [0])
		xmlNodeSetContent (node, scratch);
}

/**
 * bonobo_property_bag_xml_encode_any:
 * @opt_parent: optional parent, should be NULL
 * @any: the Any to serialize
 * @ev: a corba exception environment
 * 
 * This routine encodes @any into an XML tree using the
 * #xmlNode XML abstraction. @ev is used for flagging
 * any non-fatal exceptions during the process. On exception
 * NULL will be returned. opt_parent should be NULL, and is
 * used internally for recursive tree construction.
 *
 * Both type and content data are dumped in a non-standard, but
 * trivial format.
 * 
 * Return value: the XML tree representing the Any
 **/
xmlNode *
bonobo_property_bag_xml_encode_any (xmlNode           *opt_parent,
				    const CORBA_any   *any,
				    CORBA_Environment *ev)
{
	xmlNode  *node;
	gpointer  value;

	g_return_val_if_fail (any != NULL, NULL);

	if (opt_parent)
		node = xmlNewChild (opt_parent, NULL, "any", NULL);
	else
		node = xmlNewNode (NULL, "any");

	value = any->_value;

	xml_encode_type  (node, any->_type, ev);
	xml_encode_value (node, any->_type, &value, ev);

	return node;
}

static CORBA_TypeCode
decode_type (xmlNode           *node,
	     CORBA_Environment *ev);

static gboolean
decode_subtypes_into (xmlNode           *parent,
		      CORBA_TypeCode     tc,
		      int                num_subtypes,
		      CORBA_Environment *ev)
{
	xmlNode *l, *subtypes = NULL;
	int      i = 0;

	for (l = parent->children; l; l = l->next) {
		if (!strcmp (l->name, "subtypes"))
			subtypes = l;
	}

	if (!subtypes) {
		g_warning ("Missing subtypes field - leak");
		return FALSE;
	}

	tc->subtypes = g_new (CORBA_TypeCode, num_subtypes);

	for (l = subtypes->children; l; l = l->next) {

		if (i >= num_subtypes)
			g_warning ("Too many sub types should be %d", 
				   num_subtypes);
		else {
			tc->subtypes [i] = decode_type (l, ev);
			g_assert (tc->subtypes [i]);
		}
		i++;
	}

	if (i < num_subtypes) {
		g_warning ("Not enough sub names: %d should be %d", i, 
			   num_subtypes);
		return FALSE;
	}

	return TRUE;
}

static CORBA_TypeCode
decode_type (xmlNode           *node,
	     CORBA_Environment *ev)
{
	CORBA_TypeCode tc;
	xmlNode  *l;
	CORBA_TCKind   kind;
	char *txt;

	if ((txt = xmlGetProp (node, "tckind"))) {
		kind = atoi (txt);
		xmlFree (txt);
	} else {
		g_warning ("Format error no tckind");
		return NULL;
	}
	
	switch (kind) {
#define HANDLE_SIMPLE_TYPE(tc,kind)		\
	case CORBA_tk_##kind:			\
		return tc;

	HANDLE_SIMPLE_TYPE (TC_CORBA_string,             string);
	HANDLE_SIMPLE_TYPE (TC_CORBA_short,              short);
	HANDLE_SIMPLE_TYPE (TC_CORBA_long,               long);
	HANDLE_SIMPLE_TYPE (TC_CORBA_unsigned_short,     ushort);
	HANDLE_SIMPLE_TYPE (TC_CORBA_unsigned_long,      ulong);
	HANDLE_SIMPLE_TYPE (TC_CORBA_float,              float);
	HANDLE_SIMPLE_TYPE (TC_CORBA_double,             double);
	HANDLE_SIMPLE_TYPE (TC_CORBA_long_double,        longdouble);
	HANDLE_SIMPLE_TYPE (TC_CORBA_boolean,            boolean);
	HANDLE_SIMPLE_TYPE (TC_CORBA_char,               char);
	HANDLE_SIMPLE_TYPE (TC_CORBA_wchar,              wchar);
	HANDLE_SIMPLE_TYPE (TC_CORBA_octet,              octet);
	HANDLE_SIMPLE_TYPE (TC_CORBA_any,                any);
	HANDLE_SIMPLE_TYPE (TC_CORBA_wstring,            wstring);
	HANDLE_SIMPLE_TYPE (TC_CORBA_long_long,          longlong); 
	HANDLE_SIMPLE_TYPE (TC_CORBA_unsigned_long_long, ulonglong);

	default:
		break;
	}

	tc = g_new0 (struct CORBA_TypeCode_struct, 1);

	ORBit_RootObject_init(&tc->parent, &ORBit_TypeCode_epv);
	/* set refs to 1 */
	CORBA_Object_duplicate ((CORBA_Object)tc, NULL);

	tc->kind = kind;

	if ((txt = xmlGetProp (node, "name"))) {
		tc->name = g_strdup (txt);
		xmlFree (txt);
	}

	if ((txt = xmlGetProp (node, "repo_id"))) {
		tc->repo_id = g_strdup (txt);
		xmlFree (txt);
	}

	if ((txt = xmlGetProp (node, "length"))) {
		tc->length = atoi (txt);
		xmlFree (txt);
	}

	if ((txt = xmlGetProp (node, "sub_parts"))) {
		tc->sub_parts = atoi (txt);
		xmlFree (txt);
	}

	switch (tc->kind) {
 	case CORBA_tk_struct:
	case CORBA_tk_union:
	case CORBA_tk_enum:
	case CORBA_tk_except: { /* subnames */
		xmlNode *subnames = NULL;
		int      i = 0;

		for (l = node->children; l; l = l->next) {
			if (!strcmp (l->name, "subnames"))
				subnames = l;
		}

		if (!subnames) {
			g_warning ("Missing subnames field - leak");
			goto decode_error;
		}

		tc->subnames = (char **) g_new (char *, tc->sub_parts);

		for (l = subnames->children; l; l = l->next) {
			if (i >= tc->sub_parts)
				g_warning ("Too many sub names should be %d",
					   tc->sub_parts);
			else {
				char *txt = xmlNodeGetContent (l);
				tc->subnames [i++] = g_strdup (txt);
				xmlFree (txt);
			}
		}
		if (i < tc->sub_parts) {
			g_warning ("Not enough sub names: %d should be %d", i,
				   tc->sub_parts);
			goto decode_error;
		}
	}
	if (tc->kind != CORBA_tk_enum)
		if (!decode_subtypes_into (node, tc, tc->sub_parts, ev))
			goto decode_error;
	break;

	case CORBA_tk_alias:
	case CORBA_tk_array:
	case CORBA_tk_sequence:
		if (!decode_subtypes_into (node, tc, 1, ev))
			goto decode_error;
		break;

	default:
		break;
	}

	return tc;

 decode_error:
	CORBA_Object_release ((CORBA_Object) tc, ev);
	return NULL;
}

#define DO_DECODE_XML(tckind,format,corbatype,value,align)		      \
	case tckind:							      \
		*value = ALIGN_ADDRESS (*value, align);			      \
		if (!scratch)						      \
			g_warning ("Null content");			      \
		else if (sscanf (scratch, format, (corbatype *) *value) != 1) \
			g_warning ("Broken scanf on '%s'", scratch);	      \
		*value = ((guchar *)*value) + sizeof (corbatype);	      \
		break;

#define DO_DECODEI_XML(tckind,format,corbatype,value,align)		      \
	case tckind: {							      \
		CORBA_unsigned_long i;					      \
		*value = ALIGN_ADDRESS (*value, align);			      \
		if (!scratch)						      \
			g_warning ("Null content");			      \
		else if (sscanf (scratch, format, &i) != 1)		      \
			g_warning ("Broken scanf on '%s'", scratch);	      \
		*(corbatype *) *value = i;				      \
		*value = ((guchar *)*value) + sizeof (corbatype);	      \
		break;							      \
	}

static void
decode_value (xmlNode           *node,
	      CORBA_TypeCode     tc,
	      gpointer          *value,
	      CORBA_Environment *ev)
{
	xmlNode *l;
	char    *scratch;
	int      i;

	scratch = xmlNodeGetContent (node);

	switch (tc->kind) {
	case CORBA_tk_null:
	case CORBA_tk_void:
		break;

		DO_DECODEI_XML (CORBA_tk_short, "%d", CORBA_short, value, 
				ORBIT_ALIGNOF_CORBA_SHORT);
		DO_DECODEI_XML (CORBA_tk_ushort, "%u", CORBA_unsigned_short, 
				value, ORBIT_ALIGNOF_CORBA_SHORT);
		DO_DECODEI_XML (CORBA_tk_long, "%d", CORBA_long, value, 
				ORBIT_ALIGNOF_CORBA_LONG);
		DO_DECODEI_XML (CORBA_tk_enum, "%d", CORBA_long, value, 
				ORBIT_ALIGNOF_CORBA_LONG);
		DO_DECODEI_XML (CORBA_tk_ulong, "%u", CORBA_unsigned_long, 
				value, ORBIT_ALIGNOF_CORBA_LONG);
		DO_DECODEI_XML (CORBA_tk_boolean, "%d", CORBA_boolean, value, 
				1);
		DO_DECODEI_XML (CORBA_tk_char, "%d", CORBA_char, value,  1);
		DO_DECODEI_XML (CORBA_tk_octet, "%d", CORBA_octet, value, 1);
		DO_DECODEI_XML (CORBA_tk_wchar, "%d", CORBA_wchar, value, 
				ORBIT_ALIGNOF_CORBA_SHORT);
		DO_DECODE_XML (CORBA_tk_float, "%g", CORBA_float, value, 
			       ORBIT_ALIGNOF_CORBA_FLOAT);
		DO_DECODE_XML (CORBA_tk_double, "%lg", CORBA_double, value, 
			       ORBIT_ALIGNOF_CORBA_DOUBLE);

	case CORBA_tk_string:
	case CORBA_tk_wstring:
		*value = ALIGN_ADDRESS (*value, ORBIT_ALIGNOF_CORBA_POINTER);
		if (scratch)
			*(CORBA_char **) *value = CORBA_string_dup (scratch);
		else 
			*(CORBA_char **) *value = CORBA_string_dup ("");
		*value = ((guchar *)*value) + sizeof (CORBA_char *);
		break;

	case CORBA_tk_objref:
		g_warning ("Error objref in stream");
		break;

	case CORBA_tk_TypeCode:
		*value = ALIGN_ADDRESS (*value, ORBIT_ALIGNOF_CORBA_POINTER);
		*(CORBA_TypeCode *) *value = decode_type (node, ev);
		*value = ((guchar *) *value) + sizeof (CORBA_TypeCode);
		break;

	case CORBA_tk_any:
		*value = ALIGN_ADDRESS (*value,
					MAX (ORBIT_ALIGNOF_CORBA_LONG,
					     MAX (ORBIT_ALIGNOF_CORBA_POINTER, 
						  ORBIT_ALIGNOF_CORBA_STRUCT)));
		*(CORBA_any **)*value = 
			bonobo_property_bag_xml_decode_any (node, ev);
		*value = ((guchar *) *value) + sizeof (CORBA_any);
		break;

	case CORBA_tk_sequence: {
		CORBA_sequence_CORBA_octet *sval = *value;
		gpointer subval;
		char *txt = xmlGetProp (node, "length");

		if (!txt) {
			g_warning ("No length on sequence");
			break;
		}

		sval->_length = atoi (txt);
		sval->_maximum = tc->length;
		if (sval->_maximum && sval->_maximum <= sval->_length)
			g_warning ("Sequence too long");

		sval->_buffer = sval->_length ? ORBit_alloc_tcval (
			tc->subtypes [0], sval->_length) : NULL;

		*value = ALIGN_ADDRESS (*value,
					MAX (MAX (ORBIT_ALIGNOF_CORBA_LONG, 
						  ORBIT_ALIGNOF_CORBA_STRUCT), 
					     ORBIT_ALIGNOF_CORBA_POINTER));

		subval = sval->_buffer;
		i = 0;
		for (l = node->children; l; l = l->next) {
			if (i < sval->_length)
				decode_value (l, tc->subtypes [0], &subval, 
					      ev);
			else
				g_warning ("Too many sequence elements %d", i);
			i++;
		}
		if (i < sval->_length)
			g_warning ("Not enough sequence elements:"
				   " %d should be %d", i, tc->length);

		xmlFree (txt);
		*value = ((guchar *)*value) + sizeof (CORBA_sequence_CORBA_octet);
		break;
	}

	case CORBA_tk_array:
		i = 0;
		for (l = node->children; l; l = l->next) {
			if (i < tc->length)
				decode_value (l, tc->subtypes [0], value, ev);
			else
				g_warning ("Too many elements %d", tc->length);
			i++;
		}
		if (i < tc->length)
			g_warning ("Not enough elements: %d should be %d",
				   i, tc->length);
		break;

 	case CORBA_tk_struct:
	case CORBA_tk_except:
		i = 0;
		for (l = node->children; l; l = l->next) {
			if (i < tc->sub_parts)
				decode_value (l, tc->subtypes [i], value, ev);
			else
				g_warning ("Too many structure elements %d",
					   tc->sub_parts);
			i++;
		}
		if (i < tc->sub_parts)
			g_warning ("Not enough structure elements:"
				   " %d should be %d", i, tc->sub_parts);
		break;

	case CORBA_tk_alias:
		l = node->children;
		decode_value (l, tc->subtypes [0], value, ev);
		break;

	case CORBA_tk_union:
	case CORBA_tk_Principal:
	case CORBA_tk_fixed:
	case CORBA_tk_recursive:
	default:
		g_warning ("Unhandled");
		break;
	}

	xmlFree (scratch);
}

/**
 * bonobo_property_bag_xml_decode_any:
 * @node: the parsed XML representation of an any
 * @ev: a corba exception environment
 * 
 * This routine is the converse of bonobo_property_bag_xml_encode_any.
 * It hydrates a serialized CORBA_any.
 * 
 * Return value: the CORBA_any or NULL on error
 **/
CORBA_any *
bonobo_property_bag_xml_decode_any (xmlNode           *node,
				    CORBA_Environment *ev)
{
	CORBA_any      *any;
	CORBA_TypeCode  tc;
	xmlNode        *l, *type = NULL, *value = NULL;
	gpointer        retval;

	g_return_val_if_fail (node != NULL, NULL);

	if (strcmp (node->name, "any")) {
		g_warning ("Not an any");
		return NULL;
	}

	for (l = node->children; l; l = l->next) {

		if (!strcmp (l->name, "type"))
			type = l;
		if (!strcmp (l->name, "value"))
			value = l;
	}
	if (!type || !value) {
		g_warning ("Missing type(%p) or value(%p) node under '%s'",
			   type, value, node->name);
		return NULL;
	}
	
	tc = decode_type (type, ev);
	
	g_return_val_if_fail (tc != NULL, NULL);
	
	retval = ORBit_alloc_tcval (tc, 1);

	any = CORBA_any__alloc ();
	any->_type = tc;
	any->_value = retval;

	decode_value (value, tc, &retval, ev);

	return any;
}

#define ADD_STRING(text)                          G_STMT_START{         \
                l = strlen (text);                                      \
                memcpy (*buffer, text, l + 1);                          \
                *buffer += l; }G_STMT_END


#define ENCODE_TO_STRING(tckind,format,corbatype,value,align)		\
	case tckind:							\
		*value = ALIGN_ADDRESS (*value, align);			\
		snprintf (scratch, 127, format, *(corbatype *)*value);	\
                ADD_STRING(scratch);                                    \
		*value = ((guchar *)*value) + sizeof (corbatype);	\
		break;

#define UNPRINTABLE "XXX"

static void
encode_value (char             **buffer,
	      CORBA_TypeCode     tc,
	      gpointer          *value,
	      int                maxlen)
{
	char scratch [256] = "";
	int  i, l;

	switch (tc->kind) {
	case CORBA_tk_null:
	case CORBA_tk_void:
		break;

		ENCODE_TO_STRING (CORBA_tk_short,  "%d", CORBA_short, value, 
				  ORBIT_ALIGNOF_CORBA_SHORT);
		ENCODE_TO_STRING (CORBA_tk_ushort, "%u", CORBA_unsigned_short,
				  value, ORBIT_ALIGNOF_CORBA_SHORT);
		ENCODE_TO_STRING (CORBA_tk_long, "%d", CORBA_long, value, 
				  ORBIT_ALIGNOF_CORBA_LONG);
		ENCODE_TO_STRING (CORBA_tk_ulong,  "%u", CORBA_unsigned_long, 
				  value, ORBIT_ALIGNOF_CORBA_LONG);
		ENCODE_TO_STRING (CORBA_tk_float, "%g", CORBA_float, value, 
				  ORBIT_ALIGNOF_CORBA_FLOAT);
		ENCODE_TO_STRING (CORBA_tk_double, "%g", CORBA_double, value, 
				  ORBIT_ALIGNOF_CORBA_DOUBLE);
		ENCODE_TO_STRING (CORBA_tk_boolean, "%d", CORBA_boolean, 
				  value, 1);
		ENCODE_TO_STRING (CORBA_tk_char, "%d", CORBA_char, value, 1);

		ENCODE_TO_STRING (CORBA_tk_octet, "%d", CORBA_octet, value, 1);

		ENCODE_TO_STRING (CORBA_tk_wchar, "%d", CORBA_wchar, value, 
				  ORBIT_ALIGNOF_CORBA_SHORT);

	case CORBA_tk_enum:
		*value = ALIGN_ADDRESS (*value,  ORBIT_ALIGNOF_CORBA_LONG);
		ADD_STRING (tc->subnames[*(CORBA_long *)*value]);
		*value = ((guchar *)*value) + sizeof(CORBA_long);
		break;
	case CORBA_tk_string:
	case CORBA_tk_wstring:
		*value = ALIGN_ADDRESS(*value, ORBIT_ALIGNOF_CORBA_POINTER);
		ADD_STRING ("\"");
		l = strlen (*(CORBA_char **) *value); 
		memcpy (*buffer, *(CORBA_char **) *value, l + 1);
		*buffer += l;
		ADD_STRING ("\"");
		*value = ((guchar *)*value) + sizeof (CORBA_char *);
		break;

	case CORBA_tk_sequence: {
		CORBA_sequence_CORBA_octet *sval;
		gpointer subval;

		*value = ALIGN_ADDRESS (*value, MAX(MAX(ORBIT_ALIGNOF_CORBA_LONG, 
							ORBIT_ALIGNOF_CORBA_STRUCT),
						    ORBIT_ALIGNOF_CORBA_POINTER));

		sval = *value;
		subval = sval->_buffer;

		ADD_STRING ("(");
		for (i = 0; i < sval->_length; i++) {
			if (i)
				ADD_STRING (", ");
			encode_value (buffer, tc->subtypes [0], &subval, 0);
		}
		ADD_STRING (")");

		*value = ((guchar *)*value) + sizeof (CORBA_sequence_CORBA_octet);
		break;
	}

	case CORBA_tk_array:
		ADD_STRING ("[");
		for (i = 0; i < tc->length; i++) {
			if (i)
				ADD_STRING (", ");
			encode_value (buffer, tc->subtypes [0], value, 0);
		}
		ADD_STRING ("]");
		break;

 	case CORBA_tk_struct:
	case CORBA_tk_except:
		ADD_STRING ("{");
		for (i = 0; i < tc->sub_parts; i++) {
			if (i)
				ADD_STRING (", ");
			encode_value (buffer, tc->subtypes [i], value, 0);
		}
		ADD_STRING ("}");
		break;

	case CORBA_tk_alias:
		encode_value (buffer, tc->subtypes [0], value, 0);
		break;

	case CORBA_tk_union:
	case CORBA_tk_Principal:
	case CORBA_tk_fixed:
	case CORBA_tk_recursive:
	case CORBA_tk_objref:
	case CORBA_tk_TypeCode:
	case CORBA_tk_any:
	default:
		l = strlen (UNPRINTABLE);                               
                memcpy (*buffer, UNPRINTABLE, l + 1);                   
                *buffer += l; 
		break;
	}
}

/**
 * bonobo_config_any_to_string:
 * @any: the Any to encode
 *
 * Converts the @any into a string representation.
 *
 * Returns: a string representation of the any.
 */
char *
bonobo_config_any_to_string (CORBA_any *any)
{
	char buffer[4096];
	char *p = buffer;
	gpointer v;

	v = any->_value;

	encode_value (&p, any->_type, &v, 4096);

	return g_strdup (buffer);
}

static CORBA_TypeCode
string_to_type_code (const char *k)
{
	if (!strcmp (k, "short"))
		return TC_CORBA_short;

	if (!strcmp (k, "ushort"))
		return TC_CORBA_unsigned_short;

	if (!strcmp (k, "long"))
		return TC_CORBA_long;

	if (!strcmp (k, "ulong"))
		return TC_CORBA_unsigned_long;

	if (!strcmp (k, "float"))
		return TC_CORBA_float;

	if (!strcmp (k, "double"))
		return TC_CORBA_double;

	if (!strcmp (k, "boolean"))
		return TC_CORBA_boolean;

	if (!strcmp (k, "char"))
		return TC_CORBA_char;

	if (!strcmp (k, "string"))
		return TC_CORBA_string;

	return TC_null;
}

static char *
is_simple (CORBA_TCKind my_kind)
{
	switch (my_kind) {

	case CORBA_tk_short:   return "short";
	case CORBA_tk_ushort:  return "ushort";
	case CORBA_tk_long:    return "long";
	case CORBA_tk_ulong:   return "ulong";
	case CORBA_tk_float:   return "float";
	case CORBA_tk_double:  return "double";
	case CORBA_tk_boolean: return "boolean";
	case CORBA_tk_char:    return "char";
	case CORBA_tk_string:  return "string";
				  
	default:               return NULL;
	}
}

#define DO_ENCODE_XML(tckind,format,corbatype,value)			\
	case tckind:							\
		snprintf (scratch, 127, format,				\
			  * (corbatype *) value);			\
		xmlSetProp (node, "value", scratch);                    \
		break;

static void
encode_simple_value (xmlNode      *node,
		     const CORBA_any   *any,
		     CORBA_Environment *ev)
{
	gpointer v = any->_value;
	char scratch [256] = "";

	switch (any->_type->kind) {
	case CORBA_tk_string:
		xmlSetProp (node, "value", * (char **) v);
		break;

		DO_ENCODE_XML (CORBA_tk_short, "%d", CORBA_short, v);
		DO_ENCODE_XML (CORBA_tk_ushort, "%u", CORBA_unsigned_short,v); 
		DO_ENCODE_XML (CORBA_tk_long, "%d", CORBA_long, v);
		DO_ENCODE_XML (CORBA_tk_ulong, "%u", CORBA_unsigned_long, v);
		DO_ENCODE_XML (CORBA_tk_float, "%g", CORBA_float, v);
		DO_ENCODE_XML (CORBA_tk_double, "%g", CORBA_double,  v);
		DO_ENCODE_XML (CORBA_tk_boolean, "%d", CORBA_boolean, v);
		DO_ENCODE_XML (CORBA_tk_char, "%d", CORBA_char, v);
	default:
		g_warning ("unhandled enumeration value");
	}
}

/**
 * bonobo_config_xml_encode_any:
 * @any: the Any to encode
 * @name: the name of the entry
 * @ev: a corba exception environment
 *
 * This routine encodes @any into a XML tree. 
 * 
 * Return value: the XML tree representing the Any
 */
xmlNode *
bonobo_config_xml_encode_any (const CORBA_any   *any,
			      const char        *name,
			      CORBA_Environment *ev)
{
	xmlNode *node;
	char *kind;

	g_return_val_if_fail (any != NULL, NULL);
	g_return_val_if_fail (name != NULL, NULL);
	g_return_val_if_fail (ev != NULL, NULL);

	node = xmlNewNode (NULL, "entry");

	xmlSetProp (node, "name", name);

	if ((kind = is_simple (any->_type->kind))) {
		xmlSetProp (node, "type", kind);
		encode_simple_value (node, any, ev);
		return node;
	}

	bonobo_property_bag_xml_encode_any (node, any, ev);

	return node;
}

static char *
get_value_with_locale (xmlNode *node,
		       const char   *locale)
{
	xmlNode *child;
	char *v = NULL;
	char *l;

	for (child = node->children; child; child = child->next) {
		
		if (!strcmp (child->name, "value")) {
		
			l = xmlGetProp (child, "xml:lang");

			if (!l && !v)
				v = xmlNodeGetContent (child);
			
			if (l && locale && !strcmp (locale, l)) {
				if (v)
					xmlFree (v);
				
				v = xmlNodeGetContent (child);

				break;
			}
		}

	}

	return v;
}

#define DO_DECODE(tckind,format,corbatype,value)			     \
	case tckind: {							     \
		if (sscanf (v, format, (corbatype *) (value->_value)) != 1)  \
			g_warning ("Broken scanf on '%s'", v);	             \
		break;							     \
	}

#define DO_DECODEI(tckind,format,corbatype,value)			     \
	case tckind: {							     \
		 CORBA_unsigned_long i;			                     \
		 if (sscanf (v, format, &i) != 1)                            \
			g_warning ("Broken scanf on '%s'", v);	             \
		  *(corbatype *) value->_value = i;     	                     \
		  break;						     \
	}

static CORBA_any *
decode_simple_value (char *kind, char *v)
{
	CORBA_any *value;

	CORBA_TypeCode tc;

	if (!(tc = string_to_type_code (kind)))
		return NULL;

	value = bonobo_arg_new (tc);
 
	switch (tc->kind) {
	case CORBA_tk_string:
		BONOBO_ARG_SET_STRING (value, v);
		break;
		
		DO_DECODEI (CORBA_tk_short, "%d", CORBA_short, value); 
		DO_DECODEI (CORBA_tk_ushort, "%u",CORBA_unsigned_short, value);
		DO_DECODEI (CORBA_tk_long, "%d", CORBA_long, value); 
		DO_DECODEI (CORBA_tk_ulong, "%u", CORBA_unsigned_long, value); 
		DO_DECODE (CORBA_tk_float, "%g", CORBA_float, value);
		DO_DECODE (CORBA_tk_double, "%lg", CORBA_double, value);
		DO_DECODEI (CORBA_tk_boolean, "%d", CORBA_boolean, value);
		DO_DECODEI (CORBA_tk_char, "%d", CORBA_char, value);
	default:
	}

	return value;
}

/**
 * bonobo_config_xml_decode_any:
 * @node: the parsed XML representation of an any
 * @locale: an optional locale
 * @ev: a corba exception environment
 *
 * This routine is the converse of #bonobo_config_xml_encode_any.
 * It hydrates a serialized CORBA_any.
 * 
 * Return value: the CORBA_any or NULL on error
 */
CORBA_any *
bonobo_config_xml_decode_any (xmlNode           *node,
			      const char        *locale, 
			      CORBA_Environment *ev)
{
	CORBA_any *value = NULL;
	char *kind, *tmp;
	xmlNode *child;

	g_return_val_if_fail (node != NULL, NULL);
	g_return_val_if_fail (ev != NULL, NULL);

	if (strcmp (node->name, "entry"))
		return NULL;

	child = node->children;

	if (child && !strcmp (child->name, "any")) 
		return bonobo_property_bag_xml_decode_any (child, ev);
	
	if (!(kind = xmlGetProp (node, "type")))
		return NULL;

	tmp = xmlGetProp (node, "value");

	if (!tmp)
		tmp = get_value_with_locale (node, locale);

	if (tmp) {
		value = decode_simple_value (kind, tmp);
		xmlFree (tmp);
	}

	xmlFree (kind);
	return value;
}

