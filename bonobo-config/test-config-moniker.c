/*
 * test-config-moniker.c: configuration moniker tests
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#include <libbonobo.h>
#include <bonobo-config-utils.h>

#if 0
static void
create_enum (const char *moniker, CORBA_Environment *ev)
{
	Bonobo_Unknown v;
	CORBA_any *value;

	v = bonobo_get_object (moniker, "IDL:Bonobo/Property:1.0", ev);
	if (BONOBO_EX(ev) || !v) {
		printf ("ERR %s\n", bonobo_exception_get_text (ev));
		g_error ("Couldn't get Bonobo/Property interface");
	}

	value = bonobo_arg_new (TC_Bonobo_StorageType);
	BONOBO_ARG_SET_GENERAL (value, Bonobo_STORAGE_TYPE_REGULAR,
				TC_Bonobo_StorageType, long, NULL); 

	Bonobo_Property_setValue (v, value, ev);
}

static void
create_struct (const char *moniker, CORBA_Environment *ev)
{
	Bonobo_Unknown v;
	CORBA_any *value;

	v = bonobo_get_object (moniker, "IDL:Bonobo/Property:1.0", ev);
	if (BONOBO_EX(ev) || !v) {
		printf ("ERR %s\n", bonobo_exception_get_text (ev));
		g_error ("Couldn't get Bonobo/Property interface");
	}

	value = bonobo_arg_new (TC_Bonobo_StorageInfo);

	Bonobo_Property_setValue (v, value, ev);
}

static void
create_string_list (const char *moniker, CORBA_Environment *ev)
{
	Bonobo_Unknown v;
	CORBA_any *value;
	DynamicAny_DynSequence dyn;

	v = bonobo_get_object (moniker, "IDL:Bonobo/Property:1.0", ev);
	if (BONOBO_EX(ev) || !v) {
		printf ("ERR %s\n", bonobo_exception_get_text (ev));
		g_error ("Couldn't get Bonobo/Property interface");
	}

	dyn = CORBA_ORB_create_dyn_sequence (bonobo_orb (), 
					     TC_CORBA_sequence_CORBA_string,
					     ev);

	DynamicAny_DynSequence_set_length (dyn, 2, ev);
	
	value = DynamicAny_DynAny_to_any (dyn, ev);

	CORBA_Object_release ((CORBA_Object) dyn, ev);

	Bonobo_Property_setValue (v, value, ev);
}

static void
create_complex_list (const char *moniker, CORBA_Environment *ev)
{
	Bonobo_Unknown v;
	CORBA_any *value;
	DynamicAny_DynSequence dyn;
	
	v = bonobo_get_object (moniker, "IDL:Bonobo/Property:1.0", ev);
	if (BONOBO_EX(ev) || !v) {
		printf ("ERR %s\n", bonobo_exception_get_text (ev));
		g_error ("Couldn't get Bonobo/Property interface");
	}

	dyn = CORBA_ORB_create_dyn_sequence (bonobo_orb (), 
	        TC_CORBA_sequence_Bonobo_StorageInfo, ev);

	DynamicAny_DynSequence_set_length (dyn, 3, ev);
	
	value = DynamicAny_DynAny_to_any (dyn, ev);

	CORBA_Object_release ((CORBA_Object) dyn, ev);

	Bonobo_Property_setValue (v, value, ev);
}
#endif

static void 
listener_callback (BonoboListener    *listener,
		   char              *event_name, 
		   CORBA_any         *any,
		   CORBA_Environment *ev,
		   gpointer           user_data)
{
	xmlNode *node;
	gchar   *enc;

	node = bonobo_property_bag_xml_encode_any (NULL, any, ev);
	enc = xml_node_to_string (node);

	printf ("got event %s value %s\n", event_name, enc);

	xmlFreeNode (node);

        g_free (enc);
}

static void
test_moniker (const char *moniker, CORBA_Environment *ev)
{
	Bonobo_Unknown bag;
	CORBA_any *value;
	CORBA_TypeCode tc;
	CORBA_char *doc;
	Bonobo_KeyList *names;
	gint i;

	printf("TEST0\n");

	bag = bonobo_get_object (moniker, "IDL:Bonobo/PropertyBag:1.0", ev);
	if (BONOBO_EX(ev) || !bag) {
		printf ("ERR %s\n", bonobo_exception_get_text (ev));
		g_error ("Couldn't get Bonobo/PropertyBag interface");
	}

	bonobo_event_source_client_add_listener (bag, listener_callback, NULL, 
						 NULL, NULL); 

	printf("TEST01\n");
	names = Bonobo_PropertyBag_getKeys (bag, "", ev);
	if (BONOBO_EX (ev))
		return;

	for (i = 0; i < names->_length; i++)
		printf ("FOUND name: %s\n", names->_buffer [i]);

	CORBA_free (names);

	printf("TEST1\n");

	value = bonobo_arg_new (BONOBO_ARG_INT);
	BONOBO_ARG_SET_LONG (value, 17);

	Bonobo_PropertyBag_setValue (bag, "test1", value, ev);
	if (BONOBO_EX (ev))
		return;

	printf("TEST2\n");

	BONOBO_ARG_SET_LONG (value, 19);

	Bonobo_PropertyBag_setValue (bag, "test1", value, ev);
	if (BONOBO_EX (ev))
		return;

	printf("TEST3\n");

	doc = Bonobo_PropertyBag_getDocTitle (bag, "test1", ev);	

	if (BONOBO_EX (ev))
		return;

	printf("DocString: %s\n", doc);

	tc = Bonobo_PropertyBag_getType (bag, "test1", ev);
	if (BONOBO_EX (ev))
		return;

	printf("TEST4\n");

	if (!bonobo_arg_type_is_equal(tc, BONOBO_ARG_LONG, NULL)) {
		printf ("Wrong TypeCode %p\n", value->_type);
		return;
	}

	value = Bonobo_PropertyBag_getValue (bag, "test1", ev);
	if (BONOBO_EX (ev))
		return;

	if (bonobo_arg_type_is_equal(value->_type, BONOBO_ARG_LONG, NULL))
		printf ("Value: %d\n", BONOBO_ARG_GET_LONG(value));
	else
		printf ("Property is not long %p\n", value->_type);
	
}

gboolean
run_tests (gpointer data)
{
	CORBA_Environment ev;
	char *mname;

	CORBA_exception_init (&ev);

 	mname = g_strconcat ("xmldb:" , g_get_current_dir (), 
			     "/../tests/test-config.xmldb", 
			     "#config:/test1", NULL);

	test_moniker (mname, &ev);
	
	bonobo_main_quit ();

	return 0;
}

int
main (int argc, char **argv)
{

	if (bonobo_init (&argc, argv) == FALSE)
		g_error ("Cannot init bonobo");

	g_idle_add (run_tests, NULL);

	bonobo_main ();

	return 0;
}


