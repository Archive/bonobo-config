/**
 * bonobo-config-database.h: config database object implementation.
 *
 * Author:
 *   Dietmar Maurer  (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifndef __BONOBO_CONFIG_DATABASE_H__
#define __BONOBO_CONFIG_DATABASE_H__

#include <bonobo/bonobo-event-source.h>

G_BEGIN_DECLS

#define BONOBO_TYPE_CONFIG_DATABASE        (bonobo_config_database_get_type ())
#define BONOBO_CONFIG_DATABASE_TYPE        BONOBO_TYPE_CONFIG_DATABASE // deprecated, you should use BONOBO_TYPE_CONFIG_DATABASE
#define BONOBO_CONFIG_DATABASE(o)	   (G_TYPE_CHECK_INSTANCE_CAST ((o), BONOBO_TYPE_CONFIG_DATABASE, BonoboConfigDatabase))
#define BONOBO_CONFIG_DATABASE_CLASS(k)    (G_TYPE_CHECK_CLASS_CAST ((k), BONOBO_TYPE_CONFIG_DATABASE, BonoboConfigDatabaseClass))
#define BONOBO_IS_CONFIG_DATABASE(o)	   (G_TYPE_CHECK_INSTANCE_TYPE ((o), BONOBO_TYPE_CONFIG_DATABASE))
#define BONOBO_IS_CONFIG_DATABASE_CLASS(k) (G_TYPE_CHECK_CLASS_TYPE ((k), BONOBO_TYPE_CONFIG_DATABASE))

typedef struct _BonoboConfigDatabasePrivate BonoboConfigDatabasePrivate;

typedef struct BonoboPropertyBagServer {
	BonoboObject             parent;
} BonoboPropertyBagServer;

typedef struct {
	BonoboObjectClass        parent;

	POA_Bonobo_PropertyBag__epv epv;
} BonoboPropertyBagServerClass;

typedef struct {
	BonoboPropertyBagServer      base;

	gboolean                     writeable;

	BonoboConfigDatabasePrivate *priv;
} BonoboConfigDatabase;

typedef struct {
	BonoboPropertyBagServerClass  parent_class;

	POA_Bonobo_ConfigDatabase__epv epv;

        /*
         * virtual methods
         */

	CORBA_any      *(*get_value)    (BonoboConfigDatabase *db,
					 const CORBA_char     *key, 
					 CORBA_Environment    *ev);

	void            (*set_value)    (BonoboConfigDatabase *db,
					 const CORBA_char     *key, 
					 const CORBA_any      *value,
					 CORBA_Environment    *ev);

	Bonobo_KeyList *(*get_dirs)     (BonoboConfigDatabase *db,
					 const CORBA_char     *dir,
					 CORBA_Environment    *ev);

	Bonobo_KeyList *(*get_keys)     (BonoboConfigDatabase *db,
					 const CORBA_char     *dir,
					 CORBA_Environment    *ev);

	CORBA_boolean   (*has_dir)      (BonoboConfigDatabase *db,
					 const CORBA_char     *dir,
					 CORBA_Environment    *ev);

	void            (*remove_value) (BonoboConfigDatabase *db,
					 const CORBA_char     *key, 
					 CORBA_Environment    *ev);

	void            (*remove_dir)   (BonoboConfigDatabase *db,
					 const CORBA_char     *dir, 
					 CORBA_Environment    *ev);

	void            (*sync)         (BonoboConfigDatabase *db, 
					 CORBA_Environment    *ev);

} BonoboConfigDatabaseClass;


GType		      
bonobo_config_database_get_type  (void);

BonoboConfigDatabase *
bonobo_config_database_construct (BonoboConfigDatabase *cd,
				  BonoboEventSource    *opt_es);

char *
bonobo_config_dir_name           (const char *key);

char *
bonobo_config_leaf_name          (const char *key);

G_END_DECLS

#endif /* ! __BONOBO_CONFIG_DATABASE_H__ */
