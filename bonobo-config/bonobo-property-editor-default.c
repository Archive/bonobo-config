/*
 * bonobo-property-editor-default.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#include <bonobo.h>

#include "bonobo-property-editor.h"

static void
set_value_cb (BonoboPropertyEditor *editor,
	      BonoboArg            *value,
	      CORBA_Environment    *ev)
{
	GtkWidget *l;

	l = bonobo_control_get_widget (BONOBO_CONTROL (editor));

	gtk_label_set_text (GTK_LABEL (l), "value set");
}

BonoboObject *
bonobo_property_editor_default_new ()
{
	GtkWidget *l;
	BonoboPropertyEditor *ed;

	l = gtk_label_new ("no value set");
	gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
	gtk_widget_show (l);

	ed = bonobo_property_editor_new (l, set_value_cb, NULL);

	return BONOBO_OBJECT (ed);
}
