/**
 * bonobo-property-editor-list.h:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef _BONOBO_PROPERTY_EDITOR_LIST_H_
#define _BONOBO_PROPERTY_EDITOR_LIST_H_

#include "bonobo-property-editor.h"

G_BEGIN_DECLS

#define BONOBO_TYPE_PE_LIST        (bonobo_pe_list_get_type ())
#define BONOBO_PE_LIST_TYPE        BONOBO_TYPE_PE_LIST // deprecated, you should use BONOBO_TYPE_PE_LIST
#define BONOBO_PE_LIST(o)          (GTK_CHECK_CAST ((o), BONOBO_TYPE_PE_LIST, BonoboPEList))
#define BONOBO_PE_LIST_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_TYPE_PE_LIST, BonoboPEListClass))
#define BONOBO_IS_PE_LIST(o)       (GTK_CHECK_TYPE ((o), BONOBO_TYPE_PE_LIST))
#define BONOBO_IS_PE_LIST_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_TYPE_PE_LIST))

typedef struct _BonoboPEListPrivate BonoboPEListPrivate;

typedef struct {
	BonoboPropertyEditor   base;
	BonoboPEListPrivate *priv;
} BonoboPEList;


typedef struct {
	BonoboPropertyEditorClass parent_class;
} BonoboPEListClass;

GtkType            
bonobo_pe_list_get_type               (void);

BonoboObject *
bonobo_property_editor_sequence_new   ();

BonoboObject *
bonobo_property_editor_array_new      ();

G_END_DECLS

#endif /* _BONOBO_PROPERTY_EDITOR_LIST_H_ */
