/**
 * bonobo-config-bag.c: config bag object implementation.
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2000 Ximian, Inc.
 */
#include <config.h>
#include <bonobo/Bonobo.h>
#include <bonobo/bonobo-main.h>
#include <bonobo/bonobo-exception.h>
#include <string.h>

#include "bonobo-config-bag.h"

#define PARENT_TYPE (BONOBO_TYPE_OBJECT)

#define GET_BAG_FROM_SERVANT(servant) \
BONOBO_CONFIG_BAG (bonobo_object (servant))

static GObjectClass        *parent_class = NULL;

#define CLASS(o) BONOBO_CONFIG_BAG_CLASS (G_OBJECT_GET_CLASS (o))

static void
bonobo_config_bag_finalize (GObject *object)
{
	BonoboConfigBag *cb = BONOBO_CONFIG_BAG (object);
	
	if (cb->db) 
		bonobo_object_release_unref (cb->db, NULL);

	g_free (cb->path);

	parent_class->finalize (object);
}

static Bonobo_KeyList *
impl_Bonobo_PropertyBag_getKeys (PortableServer_Servant  servant,
				 const CORBA_char       *filter,
				 CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	Bonobo_KeyList  *retval;

	if (strchr (filter, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	path = g_strconcat (cb->path, "/", filter, NULL);

	retval = Bonobo_PropertyBag_getKeys (cb->db, path, ev);

	g_free (path);

	return retval;
}

static CORBA_TypeCode
impl_Bonobo_PropertyBag_getType (PortableServer_Servant  servant,
				 const CORBA_char       *key,
				 CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	CORBA_TypeCode   retval;

	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return CORBA_OBJECT_NIL;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	retval = Bonobo_PropertyBag_getType (cb->db, path, ev);

	g_free (path);

	return retval;
}

static CORBA_any *
impl_Bonobo_PropertyBag_getValue (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	CORBA_any       *retval;
 
	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	retval = Bonobo_PropertyBag_getValue (cb->db, path, ev);

	g_free (path);

	return retval;
}

static void 
impl_Bonobo_PropertyBag_setValue (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  const CORBA_any        *value,
				  CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	
	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	Bonobo_PropertyBag_setValue (cb->db, path, value, ev);

	g_free (path);
}

static Bonobo_PropertySet *
impl_Bonobo_PropertyBag_getValues (PortableServer_Servant servant,
				   const CORBA_char       *filter,
				   CORBA_Environment      *ev)
{
	BonoboConfigBag    *cb = GET_BAG_FROM_SERVANT (servant);
	char               *path;
	Bonobo_PropertySet *retval;

	if (strchr (filter, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	path = g_strconcat (cb->path, "/", filter, NULL);

	retval = Bonobo_PropertyBag_getValues (cb->db, path, ev);

	g_free (path);

	return retval;
}

static void                  
impl_Bonobo_PropertyBag_setValues (PortableServer_Servant servant,
				   const Bonobo_PropertySet *set,
				   CORBA_Environment *ev)
{
	int i;

	for (i = 0; i < set->_length; i++) {
		impl_Bonobo_PropertyBag_setValue (servant, 
						  set->_buffer [i].name,
						  &set->_buffer [i].value, 
						  ev);
		if (BONOBO_EX (ev))
			return;
	}
}

static CORBA_any *
impl_Bonobo_PropertyBag_getDefault (PortableServer_Servant  servant,
				    const CORBA_char       *key,
				    CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	CORBA_any       *retval;

	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	retval = Bonobo_PropertyBag_getDefault (cb->db, path, ev);

	g_free (path);

	return retval;
}

static CORBA_char *
impl_Bonobo_PropertyBag_getDocTitle (PortableServer_Servant  servant,
				     const CORBA_char       *key,
				     CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	CORBA_char      *retval;

	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	retval = Bonobo_PropertyBag_getDocTitle (cb->db, path, ev);

	g_free (path);

	return retval;
}

static CORBA_char *
impl_Bonobo_PropertyBag_getDoc (PortableServer_Servant  servant,
				const CORBA_char       *key,
				CORBA_Environment      *ev)
{
	BonoboConfigBag *cb = GET_BAG_FROM_SERVANT (servant);
	char            *path;
	CORBA_char      *retval;

	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return NULL;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	retval = Bonobo_PropertyBag_getDoc (cb->db, path, ev);

	g_free (path);

	return retval;
}

static Bonobo_PropertyFlags
impl_Bonobo_PropertyBag_getFlags (PortableServer_Servant  servant,
				  const CORBA_char       *key,
				  CORBA_Environment      *ev)
{
	BonoboConfigBag      *cb = GET_BAG_FROM_SERVANT (servant);
	char                 *path;
	Bonobo_PropertyFlags  retval;

	if (strchr (key, '/')) {
		bonobo_exception_set (ev, ex_Bonobo_PropertyBag_NotFound);
		return 0;
	}

	path = g_strconcat (cb->path, "/", key, NULL);

	retval = Bonobo_PropertyBag_getFlags (cb->db, path, ev);

	g_free (path);

	return retval;
}


void
notify_cb (BonoboListener    *listener,
	   const char        *event_name, 
	   const CORBA_any   *any,
	   CORBA_Environment *ev,
	   gpointer           user_data)
{
	BonoboConfigBag *cb = BONOBO_CONFIG_BAG (user_data);
	char *tmp, *ename;

	tmp = bonobo_event_subtype (event_name);
	ename = g_strconcat ("Bonobo/Property:change:", tmp, NULL); 
	g_free (tmp);

	bonobo_event_source_notify_listeners (cb->es, ename, any, NULL);

	g_free (ename);
}

BonoboConfigBag *
bonobo_config_bag_new (Bonobo_ConfigDatabase db,
		       const gchar *path)
{
	BonoboConfigBag *cb;
	char *m;
	int l;

	g_return_val_if_fail (db != NULL, NULL);
	g_return_val_if_fail (path != NULL, NULL);

	cb = g_object_new (BONOBO_TYPE_CONFIG_BAG, NULL);
	
	if (path[0] == '/')
		cb->path = g_strdup (path);
	else
		cb->path = g_strconcat ("/", path, NULL);

	cb->db = bonobo_object_dup_ref (db, NULL);

	while ((l = strlen (cb->path)) > 1 && path [l - 1] == '/') 
		cb->path [l] = '\0';
	
	cb->es = bonobo_event_source_new ();

	bonobo_object_add_interface (BONOBO_OBJECT (cb), 
				     BONOBO_OBJECT (cb->es));

	m = g_strconcat ("Bonobo/ConfigDatabase:change", cb->path, ":", NULL);

	bonobo_event_source_client_add_listener (db, notify_cb, m, NULL, cb);

	g_free (m);

	return cb;
}

static void
bonobo_config_bag_class_init (BonoboConfigBagClass *class)
{
	GObjectClass *object_class = (GObjectClass *) class;
	POA_Bonobo_PropertyBag__epv *epv= &class->epv;
	
	parent_class = g_type_class_peek_parent (class);

	object_class->finalize = bonobo_config_bag_finalize;

	epv->getKeys       = impl_Bonobo_PropertyBag_getKeys;
	epv->getType       = impl_Bonobo_PropertyBag_getType;
	epv->getValue      = impl_Bonobo_PropertyBag_getValue;
	epv->setValue      = impl_Bonobo_PropertyBag_setValue;
	epv->getValues     = impl_Bonobo_PropertyBag_getValues;
	epv->setValues     = impl_Bonobo_PropertyBag_setValues;
	epv->getDefault    = impl_Bonobo_PropertyBag_getDefault;
	epv->getDocTitle   = impl_Bonobo_PropertyBag_getDocTitle;
	epv->getDoc        = impl_Bonobo_PropertyBag_getDoc;
	epv->getFlags      = impl_Bonobo_PropertyBag_getFlags;
}

static void
bonobo_config_bag_init (BonoboConfigBag *cb)
{
	/* nothing to do */
}

BONOBO_TYPE_FUNC_FULL (BonoboConfigBag, 
		       Bonobo_PropertyBag,
		       PARENT_TYPE,
		       bonobo_config_bag);

