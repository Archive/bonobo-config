/**
 * bonobo-property-editor.h:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#ifndef _BONOBO_PROPERTY_EDITOR_H_
#define _BONOBO_PROPERTY_EDITOR_H_

#include <bonobo/bonobo-control.h>
#include <bonobo-conf/Bonobo_Config.h>

G_BEGIN_DECLS

#define BONOBO_PROPERTY_EDITOR_PREFIX "OAFIID:Bonobo_PropertyEditor_"
 
#define BONOBO_TYPE_PROPERTY_EDITOR        (bonobo_property_editor_get_type ())
#define BONOBO_PROPERTY_EDITOR_TYPE        BONOBO_TYPE_PROPERTY_EDITOR // deprecated, you should use BONOBO_TYPE_PROPERTY_EDITOR
#define BONOBO_PROPERTY_EDITOR(o)          (GTK_CHECK_CAST ((o), BONOBO_TYPE_PROPERTY_EDITOR, BonoboPropertyEditor))
#define BONOBO_PROPERTY_EDITOR_CLASS(k)    (GTK_CHECK_CLASS_CAST((k), BONOBO_TYPE_PROPERTY_EDITOR, BonoboPropertyEditorClass))
#define BONOBO_IS_PROPERTY_EDITOR(o)       (GTK_CHECK_TYPE ((o), BONOBO_TYPE_PROPERTY_EDITOR))
#define BONOBO_IS_PROPERTY_EDITOR_CLASS(k) (GTK_CHECK_CLASS_TYPE ((k), BONOBO_TYPE_PROPERTY_EDITOR))

typedef struct _BonoboPropertyEditorPrivate BonoboPropertyEditorPrivate;

typedef struct {
	BonoboControl base;

	CORBA_TypeCode tc; /* read only */

	BonoboPropertyEditorPrivate *priv;
} BonoboPropertyEditor;

typedef void (*BonoboPropertyEditorSetFn) (BonoboPropertyEditor *editor,
					   BonoboArg            *value,
					   CORBA_Environment    *ev);

typedef struct {
	BonoboControlClass parent_class;

	POA_Bonobo_PropertyEditor__epv epv;

	/* virtual methods */

	BonoboPropertyEditorSetFn set_value;

	Bonobo_PropertyEditor_PropertyEditorSet * 
	(*get_sub_editors)   (BonoboPropertyEditor  *editor, 
			      CORBA_Environment     *ev);

} BonoboPropertyEditorClass;

GtkType            
bonobo_property_editor_get_type   (void);

BonoboPropertyEditor *
bonobo_property_editor_new        (GtkWidget                 *widget,
				   BonoboPropertyEditorSetFn  set_cb,
				   CORBA_TypeCode             tc);

void                
bonobo_property_editor_set_value  (BonoboPropertyEditor *editor,
				   const BonoboArg      *value,
				   CORBA_Environment    *opt_ev);

BonoboObject *
bonobo_property_editor_basic_new   (CORBA_TypeCode tc);

BonoboObject *
bonobo_property_editor_boolean_new ();

BonoboObject *
bonobo_property_editor_enum_new    ();

BonoboObject *
bonobo_property_editor_default_new ();

BonoboObject *
bonobo_property_editor_filename_new ();

Bonobo_PropertyEditor
bonobo_property_editor_resolve     (CORBA_TypeCode tc,  
				    CORBA_Environment *ev);

GtkWidget *
bonobo_config_widget_new           (CORBA_TypeCode      tc, 
				    Bonobo_PropertyBag  pb,
				    const char         *name,
				    CORBA_any          *defval,
				    CORBA_Environment  *opt_ev);

G_END_DECLS

#endif /* _BONOBO_PROPERTY_EDITOR_H_ */
