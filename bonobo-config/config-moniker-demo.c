/*
 * config-moniker-demo.c: a small demo for the configuration moniker
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */
#include <libbonobo.h>

#include <bonobo-config/bonobo-config-database.h>
#include <bonobo-config/bonobo-preferences.h>
#include <bonobo-config/bonobo-config-control.h>
#include <bonobo-config/bonobo-property-editor.h>

/*
 * display a moniker as Bonobo/Control
 */
static void
display_as_control (const char *moniker, 
		    CORBA_Environment *ev)
{
	Bonobo_Control     control;
	GtkWidget         *widget;
	BonoboUIContainer *ui_container;
	GtkWidget         *window;

	control = bonobo_get_object (moniker, "IDL:Bonobo/Control:1.0", ev);
	if (BONOBO_EX (ev) || !control)
		g_error ("Couldn't get Bonobo/Control interface");

	window = bonobo_window_new ("config: moniker test", moniker);
	ui_container = bonobo_ui_container_new ();
	bonobo_ui_container_set_win (ui_container, BONOBO_WINDOW (window));

	gtk_window_set_default_size (GTK_WINDOW (window), 400, 350);

	widget = bonobo_widget_new_control_from_objref (control,
		BONOBO_OBJREF (ui_container));
	gtk_widget_show (widget);

	bonobo_object_unref (BONOBO_OBJECT (ui_container));

	bonobo_control_frame_control_activate (
		bonobo_widget_get_control_frame (BONOBO_WIDGET (widget)));

	bonobo_window_set_contents (BONOBO_WINDOW (window), widget);

	gtk_signal_connect (GTK_OBJECT (window), "destroy",
			    GTK_SIGNAL_FUNC (gtk_widget_destroy), NULL);

	gtk_widget_show_all (window);
}

/*
 * display a single value ("bonobo-conf-test/test-long")
 */
static void
demo1 ()
{
	CORBA_Environment ev;
	char *mname;

	CORBA_exception_init (&ev);

	mname = g_strconcat ("xmldb:" , g_get_current_dir (), 
			     "/../tests/test-config.xmldb", 
			     "#config:/bonobo-conf-test/test-long", NULL);
	
	display_as_control (mname, &ev); 
	g_assert (!BONOBO_EX (&ev));
}

/*
 * display a whole directory ("bonobo-conf-test")
 */
static void
demo2 ()
{
	CORBA_Environment ev;
	char *mname;

	CORBA_exception_init (&ev);

	mname = g_strconcat ("xmldb:" , g_get_current_dir (), 
			     "/../tests/test-config.xmldb", 
			     "#config:/bonobo-conf-test", NULL);
	
	display_as_control (mname, &ev); 
	g_assert (!BONOBO_EX (&ev));
}

			  
/*
 * manually create the layout of a property page
 */
static GtkWidget *
demo3_get_fn (BonoboConfigControl *control,
	      Bonobo_PropertyBag   pb,
	      gpointer             closure,
	      CORBA_Environment   *ev)
{
	GtkWidget *w, *table;

	table = gtk_table_new (3, 3, FALSE);

	w = gtk_label_new ("A normal integer value");
	gtk_table_attach_defaults (GTK_TABLE (table), w, 0, 1, 0, 1);

	w = bonobo_config_widget_new (TC_long, pb, "test-long2", NULL, ev);
	gtk_table_attach_defaults (GTK_TABLE (table), w, 1, 2, 0, 1);

	w = gtk_label_new ("Enumeration Types");
	gtk_table_attach_defaults (GTK_TABLE (table), w, 0, 1, 1, 2);

	w = bonobo_config_widget_new (TC_Bonobo_StorageType, pb, "test-enum", 
				      NULL, ev);
	gtk_table_attach_defaults (GTK_TABLE (table), w, 1, 2, 1, 2);

	w = gtk_label_new ("Filename");
	gtk_table_attach_defaults (GTK_TABLE (table), w, 0, 1, 2, 3);

	w = bonobo_config_widget_new (TC_Bonobo_FileName, pb, "test-filename",
				      NULL, ev);
	gtk_table_attach_defaults (GTK_TABLE (table), w, 1, 2, 2, 3);

	gtk_widget_show_all (table);

	return table;
}

/*
 * create a preferences dialog
 */
static void
demo3 ()
{
	CORBA_Environment ev;
	BonoboConfigControl *config_control;
	GtkWidget *pref;
	Bonobo_PropertyBag pb;
	char *mname;

	CORBA_exception_init (&ev);

	mname = g_strconcat ("xmldb:" , g_get_current_dir (), 
			     "/../tests/test-config.xmldb", 
			     "#config:/bonobo-conf-test", NULL);

	pb = bonobo_get_object (mname, "Bonobo/PropertyBag", &ev);
	g_assert (!BONOBO_EX (&ev));

	config_control = bonobo_config_control_new (NULL);
	g_assert (config_control != NULL);

	bonobo_config_control_add_page (config_control, 
					"Manually created Page",
					pb, demo3_get_fn, NULL);
	bonobo_config_control_add_page (config_control, "Auto generated page1",
					pb, NULL, NULL);
	bonobo_config_control_add_page (config_control, "Auto generated page2",
					pb, NULL, NULL);

	pref = bonobo_preferences_new (BONOBO_OBJREF (config_control));

	bonobo_object_unref (BONOBO_OBJECT (config_control));

	gtk_widget_show (pref);
}

/*
 * just some tests for localized values
 */
static void
demo4 ()
{
	Bonobo_ConfigDatabase db;
	CORBA_Environment ev;
	CORBA_any *value;
	char *mname;

	CORBA_exception_init (&ev);

	mname = g_strconcat ("xmldb:" , g_get_current_dir (), 
			     "/../tests/test-config.xmldb", NULL);

	db = bonobo_get_object (mname, "Bonobo/ConfigDatabase", &ev);
	g_assert (!BONOBO_EX (&ev));

	value = Bonobo_ConfigDatabase_getValue 
		(db, "doc/bonobo-conf-test/test-string", "", &ev);

	g_assert (!BONOBO_EX (&ev));
	printf ("FOUND VALUE: %s\n", BONOBO_ARG_GET_STRING (value));

	value = Bonobo_ConfigDatabase_getValue 
		(db, "doc/bonobo-conf-test/test-string", "de", &ev);

	g_assert (!BONOBO_EX (&ev));
	printf ("FOUND VALUE: %s\n", BONOBO_ARG_GET_STRING (value));

	value = Bonobo_ConfigDatabase_getValue 
		(db, "doc/bonobo-conf-test/test-string", "xyz", &ev);

	g_assert (!BONOBO_EX (&ev));
	printf ("FOUND VALUE: %s\n", BONOBO_ARG_GET_STRING (value));

	mname = g_strconcat ("xmldb:" , g_get_current_dir (), 
			     "/../tests/test-config.xmldb", 
			     "#xmldb:/tmp/test.xmldb", 
			     "#config:", "/bonobo-conf-test", NULL);

	display_as_control (mname, &ev);
	g_assert (!BONOBO_EX (&ev));
}

int
main (int argc, char **argv)
{
	CORBA_Environment ev;
	GtkWidget *win, *b, *vbox;

	CORBA_exception_init (&ev);

	gnome_init ("moniker-test", "0.0", argc, argv);

	if (bonobo_init (&argc, argv) == FALSE)
		g_error ("Cannot init bonobo");

	win = gnome_app_new ("config-moniker-demo", 
			    "Bonobo Configuration System Demo");

	gtk_signal_connect (GTK_OBJECT (win), "delete_event",
			    GTK_SIGNAL_FUNC (gtk_main_quit), NULL);
	
	gtk_widget_show (win);
	
	vbox = gtk_vbox_new (0, FALSE);

	b = gtk_button_new_with_label ("Demo 1");
	gtk_box_pack_start_defaults (GTK_BOX(vbox), b);
	gtk_signal_connect (GTK_OBJECT (b), "pressed",
			    GTK_SIGNAL_FUNC (demo1), NULL);

	b = gtk_button_new_with_label ("Demo 2");
	gtk_box_pack_start_defaults (GTK_BOX(vbox), b);
	gtk_signal_connect (GTK_OBJECT (b), "pressed",
			    GTK_SIGNAL_FUNC (demo2), NULL);

	b = gtk_button_new_with_label ("Demo 3");
	gtk_box_pack_start_defaults (GTK_BOX(vbox), b);
	gtk_signal_connect (GTK_OBJECT (b), "pressed",
			    GTK_SIGNAL_FUNC (demo3), NULL);

	b = gtk_button_new_with_label ("Demo 4");
	gtk_box_pack_start_defaults (GTK_BOX(vbox), b);
	gtk_signal_connect (GTK_OBJECT (b), "pressed",
			    GTK_SIGNAL_FUNC (demo4), NULL);

	gtk_widget_show_all (vbox);

	gnome_app_set_contents (GNOME_APP (win), vbox);

	bonobo_main ();

	return 0;
}


