/*
 * bonobo-config-utils.h: Utility functions
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */

#ifndef __BONOBO_CONFIG_UTILS_H__
#define __BONOBO_CONFIG_UTILS_H__

#include <glib.h>
#include <libxml/tree.h>
#include <libxml/parser.h>

G_BEGIN_DECLS

xmlNode *
bonobo_property_bag_xml_encode_any (xmlNode           *opt_parent,
				    const CORBA_any   *any,
				    CORBA_Environment *ev);
CORBA_any *
bonobo_property_bag_xml_decode_any (xmlNode           *node,
				    CORBA_Environment *ev);

char *
xml_node_to_string           (xmlNode  *node);

xmlNode *
bonobo_config_xml_encode_any (const CORBA_any   *any,
			      const char        *name,
			      CORBA_Environment *ev);

CORBA_any *
bonobo_config_xml_decode_any (xmlNode           *node,
			      const char        *locale, 
			      CORBA_Environment *ev);

char *
bonobo_config_any_to_string  (CORBA_any *any);

G_END_DECLS

#endif /* ! __BONOBO_CONFIG_UTILS_H__ */
