/**
 * bonobo-property-editor-struct.c:
 *
 * Author:
 *   Dietmar Maurer (dietmar@ximian.com)
 *
 * Copyright 2001 Ximian, Inc.
 */

#include <gtk/gtk.h>
#include <bonobo/bonobo-exception.h>
#include <bonobo/bonobo-main.h>

#include "bonobo-property-editor-struct.h"
#include "bonobo-config-utils.h"
#include "bonobo-subproperty.h"

#define PARENT_TYPE (bonobo_property_editor_get_type ())

/* Parent object class in GTK hierarchy */
static BonoboPropertyEditorClass *bonobo_pe_struct_parent_class;

struct _BonoboPEStructPrivate {
	CORBA_any *value;
	GtkWidget *widget;
	BonoboEventSource *es;
	GList *list;
};

static void
bonobo_pe_struct_destroy (GtkObject *object)
{
	BonoboPEStruct *editor = BONOBO_PE_STRUCT (object);

	if (editor->priv->es)
		bonobo_object_unref (BONOBO_OBJECT (editor->priv->es));

	if (editor->priv->value)
		CORBA_free (editor->priv->value);

	g_free (editor->priv);

	GTK_OBJECT_CLASS (bonobo_pe_struct_parent_class)->destroy (object);
}

static void 
real_pe_set_value (BonoboPropertyEditor *editor,
		   BonoboArg            *value,
		   CORBA_Environment    *ev)
{
	BonoboPEStructPrivate *priv = BONOBO_PE_STRUCT (editor)->priv;
	BonoboSubProperty *sub;
	CORBA_TypeCode tc;
	GList *list;
	int i, l, offset, pos;
	CORBA_any nv;
	char *str;

	if (value->_type->kind != CORBA_tk_struct) {
		bonobo_exception_set (ev,ex_Bonobo_PropertyEditor_InvalidType);
		return;
	}
	
	if (priv->value) {
		if (bonobo_arg_is_equal (priv->value, value, NULL))
			return;

		CORBA_free (priv->value);
	}

	priv->value = bonobo_arg_copy (value);

	l = priv->value->_type->sub_parts;
	list = priv->list;
	offset = 0;

	for (i = 0; i < l; i++) {
		if (!list)
			break;
		
		sub = BONOBO_SUB_PROPERTY (list->data);

		tc = priv->value->_type->subtypes [i];

		nv._type = tc;
		nv._value = priv->value->_value + offset;

		bonobo_sub_property_set_value (sub, &nv);

		/* fixme: serious data alignment problems ? */
		offset += ORBit_gather_alloc_info (tc);		

		list = list->next;
	}
		
	pos = 0;

	str = bonobo_config_any_to_string (value);
	gtk_editable_delete_text (GTK_EDITABLE (priv->widget), 0, -1);
	gtk_editable_insert_text (GTK_EDITABLE (priv->widget), str, 
				  strlen (str), &pos);
	g_free (str);

}

static void 
subproperty_change_cb (BonoboPropertyEditor *editor,
		       const CORBA_any      *value,
		       int                   index)
{
	BonoboPEStructPrivate *priv = BONOBO_PE_STRUCT (editor)->priv;
	DynamicAny_DynAny dyn;
	CORBA_sequence_NameValuePair *seq, *newseq;
	CORBA_NameValuePair pair;
	CORBA_any *newvalue;
	gpointer src, d;
	int i;

	dyn = CORBA_ORB_create_dyn_any (bonobo_orb (), priv->value, NULL);
	
	seq = DynamicAny_DynStruct_get_members (dyn, NULL);
	CORBA_Object_release ((CORBA_Object) dyn, NULL);


	newseq = CORBA_sequence_NameValuePair__alloc ();
	newseq->_length = seq->_length;
	newseq->_buffer = 
		CORBA_sequence_NameValuePair_allocbuf (newseq->_length);
	CORBA_sequence_set_release (newseq, TRUE);

	d = newseq->_buffer;
	pair.id = seq->_buffer [index].id;
	pair.value = *value;

	for (i = 0; i < newseq->_length; i++) {
		
		if (i == index) 
			src = &pair;
		else
			src = &seq->_buffer [i];

		_ORBit_copy_value (&src, &d, TC_CORBA_NameValuePair);
	}

	CORBA_free (seq);

	dyn = CORBA_ORB_create_dyn_struct (bonobo_orb (), priv->value->_type, 
					   NULL);

	DynamicAny_DynStruct_set_members (dyn, newseq, NULL);

	CORBA_free (newseq);
	
	newvalue = DynamicAny_DynAny_to_any (dyn, NULL);

	bonobo_property_editor_set_value (editor, newvalue, NULL);

	CORBA_free (newvalue);

	CORBA_Object_release ((CORBA_Object) dyn, NULL);
}

static Bonobo_PropertyEditor_PropertyEditorSet *
real_pe_get_sub_editors (BonoboPropertyEditor *editor,
			 CORBA_Environment    *ev)
{
	BonoboPEStructPrivate *priv = BONOBO_PE_STRUCT (editor)->priv;
	Bonobo_PropertyEditor_PropertyEditorSet *pl;
	Bonobo_PropertyEditor_Pair *buf;
	BonoboSubProperty *sub;
	CORBA_TypeCode tc;
	BonoboSubProperty *subprop;
	CORBA_any nv;
	int l, i, offset;
	GList *list;

	if (!priv->value)
		return NULL;
	
	list = priv->list;
	while (list) {
		sub = BONOBO_SUB_PROPERTY (list->data);
		bonobo_object_unref (BONOBO_OBJECT (sub));
		list = list->next;
	}
	g_list_free (priv->list);
	priv->list = NULL;

	pl = CORBA_sequence_Bonobo_PropertyEditor_Pair__alloc ();
	CORBA_sequence_set_release (pl, TRUE); 

	l = priv->value->_type->sub_parts;
	buf = CORBA_sequence_Bonobo_PropertyEditor_Pair_allocbuf (l);
	pl->_buffer = buf;
	offset = 0;

	for (i = 0; i < l; i++) {
		
		buf [i].name = 
			CORBA_string_dup (priv->value->_type->subnames [i]);

		tc = priv->value->_type->subtypes [i];

		buf [i].editor = bonobo_property_editor_resolve (tc, ev);
		if (BONOBO_EX (ev) || !buf [i].editor)
			continue;

		nv._type = tc;
		nv._value = priv->value->_value + offset;
		
		subprop = bonobo_sub_property_new (editor, buf [i].name, 
						   &nv, i, priv->es,
						   subproperty_change_cb);
		priv->list = g_list_append (priv->list, subprop);

		Bonobo_PropertyEditor_setProperty (buf [i].editor, 
						   BONOBO_OBJREF(subprop), ev);

		offset += ORBit_gather_alloc_info (tc);

		pl->_length = i + 1;		
	}

	return pl;
}

static void
bonobo_pe_struct_class_init (BonoboPEStructClass *klass)
{
	GtkObjectClass *object_class = (GtkObjectClass *)klass;
	BonoboPropertyEditorClass *pec = (BonoboPropertyEditorClass *)klass;

	bonobo_pe_struct_parent_class = gtk_type_class (PARENT_TYPE);

	pec->set_value = real_pe_set_value;
	pec->get_sub_editors = real_pe_get_sub_editors;

	object_class->destroy = bonobo_pe_struct_destroy;

}

static void
bonobo_pe_struct_init (BonoboPEStruct *editor)
{
	editor->priv = g_new0 (BonoboPEStructPrivate, 1);
}


BONOBO_X_TYPE_FUNC (BonoboPEStruct, PARENT_TYPE, bonobo_pe_struct);

BonoboObject *
bonobo_property_editor_struct_new ()
{
	BonoboPEStruct *editor;

	editor = gtk_type_new (bonobo_pe_struct_get_type ());

	editor->priv->widget = gtk_entry_new ();
	gtk_entry_set_editable (GTK_ENTRY (editor->priv->widget), FALSE);
	gtk_widget_show (editor->priv->widget);

	editor->priv->es = bonobo_event_source_new ();

	bonobo_control_construct (BONOBO_CONTROL (editor),
				  editor->priv->widget);
 
	return BONOBO_OBJECT (editor);
}
