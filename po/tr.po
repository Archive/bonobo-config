# Turkish translations for bonobo-config messages.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Nilgün Belma Bugüner <nilgun@fide.org>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: bonobo-config \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-12-28 21:34+0000\n"
"PO-Revision-Date: 2001-11-07 21:44+0200\n"
"Last-Translator: Nilgün Belma Bugüner <nilgun@fide.org>\n"
"Language-Team: Turkish <gnome-turk@gnome.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: tests/test-config.xml.in.h:1
msgid "a string"
msgstr "bir dizge"

#: tests/test-config.xml.in.h:2
msgid "just a test value"
msgstr "tam bir test değeri"
