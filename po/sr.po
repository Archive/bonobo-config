# Serbian translation of bonobo-config
# Courtesy of Prevod.org team (http://www.prevod.org/) -- 2003.
# 
# This file is distributed under the same license as the bonobo-config package.
# 
# Maintainer: Бранко Ивановић <popeye@one.ekof.bg.ac.yu>
#
msgid ""
msgstr ""
"Project-Id-Version: bonobo-config 2.2\n"
"POT-Creation-Date: 2003-04-28 19:40+0200\n"
"PO-Revision-Date: 2003-05-03 04:15+0100\n"
"Last-Translator: Бранко Ивановић <popeye@one.ekof.bg.ac.yu>\n"
"Language-Team: Serbian (sr) <serbiangnome-lista@nongnu.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3;    plural=n%10==1 && n%100!=11 ? "
"0 :    n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"

#: tests/test-config.xml.in.h:1
msgid "a string"
msgstr "реч"

#: tests/test-config.xml.in.h:2
msgid "just a test value"
msgstr "обичан тест унос"